Imports System.Threading
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.NetworkExplorer.Tools.Caching
Imports TKI.Tools
Imports TKI.Tools.Database

Friend Module RenameDucts
  Friend Sub RenameDuctsJob(status As IStatusDisplay, parameters() As Object)
    If parameters.Length <> 2 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
    If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))
    If TypeOf parameters(1) IsNot Document Then Throw New ArgumentException($"The parameter 2 has the wrong type. Expected: {NameOf(Document)}", NameOf(parameters))

    Dim vCluster = DirectCast(parameters(0), Cluster)
    Dim vDocument = DirectCast(parameters(1), Document)

    Task.WaitAll(RunRenameDuctsJob(status, vCluster, vDocument, CancellationToken.None))
  End Sub

  Private Async Function RunRenameDuctsJob(status As IStatusDisplay, vCluster As Cluster, document As Document, token As CancellationToken) As Task
    Using db As New DatabaseWorker(vCluster.Connection)
      Using burst As New RegenerateLabelDisableBurst(db.Connection)
        Using mapBurst = document.Map.InitiateDrawBurst()
          Using transaction = Await db.BeginTransaction(token)
            Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
            Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)

            Dim ductsByFid = Await Duct.GetCache(cache)
            Dim ductDuctsByOuterduct = Await cache.DuctDuctsByOuterDuctFid
            Dim processedDucts = New HashSet(Of Duct)

            For Each duct In ductsByFid.Values.Distinct
              If processedDucts.Contains(duct) Then Continue For
              If Not Await IsDuctBundleType(cache, duct) Then Continue For
              For Each ductduct In ductDuctsByOuterduct(duct.FID)
                If Not ductsByFid.ContainsKey(ductduct.DuctFID.GetValueOrDefault(-1)) Then Continue For
                Dim innerDuct = ductsByFid(ductduct.DuctFID.GetValueOrDefault(-1))
                Dim newName = $"{duct.Name}.{ductduct.PositionNumber}"
                If String.Equals(newName, innerDuct.Name, StringComparison.Ordinal) Then Continue For
                innerDuct.Name = newName
                processedDucts.Add(innerDuct)
              Next
            Next

            If processedDucts.Any Then
              processedDucts.First.GeomFeatureClass.UpdateFeatures(processedDucts.Select(Function(d) d.GeomFeature), False)
            End If

            Await transaction.Commit(token)
          End Using
        End Using
      End Using
    End Using

  End Function

  Private Async Function IsDuctBundleType(cache As ExportCache, duct As Duct) As Task(Of Boolean)
    If Not duct.ModelFID.HasValue Then Return False
    Dim ductTypeductTypeByFid = Await DuctTypeDuctType.GetCache(cache)

    For Each ductTypeDuctType In ductTypeductTypeByFid.Values
      If ductTypeDuctType.ParentDuctTypeFID = duct.ModelFID.Value Then Return True
    Next
    Return False
  End Function

End Module
