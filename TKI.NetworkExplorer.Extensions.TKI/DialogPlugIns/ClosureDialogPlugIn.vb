Imports System.Collections.Immutable
Imports System.Text.RegularExpressions
Imports System.Threading
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.Tools.MenuItemHelper
Imports TC.FO.API
Imports TKI.NetworkExplorer.Branding
Imports TKI.Tools
Imports TKI.UI
Imports TKI.Compat
Imports TKI.UI.Configuration
Imports System.IO

Namespace DialogPlugIns
  Public NotInheritable Class ClosureDialogPlugIn
    Inherits TC.Base.PlugIn.DialogPlugIn.TelcoItemDialogPlugIn

    Private WithEvents _mniSpliceClosure As New MenuItem()
    Private WithEvents _mniSpliceClosureBulk As New MenuItem()

    Private ReadOnly _switchesToFlip As ReentrantSwitch() = {Duct.SuspendDuctInsertionChecks, Duct.DisableRenamingOfInnerDucts, Duct.DisableRenamingOfConnectedDucts,
                                                           Duct.DisableCalculationOfDuctLength, Duct.DisableAutomaticDuctInsertionIntoManhole, DuctCable.SuspendAssignmentChecks,
                                                           DuctCable.SuspendAssignCableToConnectedDucts}

    Private ReadOnly _gfnwEngineeringCode As New DMCodeNumber(9117, 0, 19221)

    Friend Function IsGFNWEnabled(connection As TBConnection) As Boolean
      If connection Is Nothing Then Return False
      Return connection.DataModelCodes.OfType(Of DMCode).Where(Function(dmCode) dmCode.Code.Equals(_gfnwEngineeringCode)).Any()
    End Function

    Private Sub ClosureDialogPlugIn_InitMenu(sender As Object, e As MenuEventArgs) Handles Me.InitMenu
      If IsGFNWEnabled(Document.Connection) Then
        TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniSpliceClosure, "Telekom Doku: Muffe bestücken", TC.Common.Tools.PictureType.NO_Picture)
        TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniSpliceClosureBulk, "Telekom Doku: Muffe bestücken (Filter)", TC.Common.Tools.PictureType.NO_Picture)
      End If
    End Sub

    Private Sub _mniSpliceClosure_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniSpliceClosure.Click
      StartSpliceClosureJob({Dialog.Record.Fid})
    End Sub

    Private Sub _mniSpliceClosureBulk_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniSpliceClosureBulk.Click
      StartSpliceClosureJob(Dialog.Record.Fids)
    End Sub

    Public Sub StartSpliceClosureJob(fids As IList(Of Long))
      If Not Dialog.Record.Available Then
        Return
      End If
      Dim configEntries = New List(Of IConfigurationEntry)
      Dim feederFiberNumberEntry = New TextEntry("Startnummer HK-Fasern", "Startnummer HK-Fasern", "1", {New Validation.IsIntegerValidationRule()})
      configEntries.Add(feederFiberNumberEntry)
      Dim splitterCountEntry = New TextEntry("Anzahl 1:32 Splitter", "Anzahl 1:32 Splitter", "1", {New Validation.IsIntegerValidationRule()})
      configEntries.Add(splitterCountEntry)
      If Not ShowDialog("Muffen bestücken", Branding.EngineeringTkiWindowTheme, configEntries) Then
        Return  'Abbruch durch Benutzer
      End If

      Dim feederStartNumber = 1L
      Long.TryParse(feederFiberNumberEntry.Value, feederStartNumber)
      Dim splitterCount = 1L
      Long.TryParse(splitterCountEntry.Value, splitterCount)

      Dim statusForm = New Desktop.StatusForm(Of SplicingConfig)(AddressOf ExecuteSpliceClosureJob)
      statusForm.Parameters = {New SplicingConfig() With {.FIDs = fids.ToArray, .FeederStartNumber = feederStartNumber, .SplitterCount = splitterCount}}
      statusForm.AbortThreadOnCancel = True
      statusForm.CancelButtonVisible = True
      statusForm.CancelButtonEnabled = True
      statusForm.Status = "Muffen neu spleißen"
      statusForm.Type = Desktop.ProgressBarType.ProgressBar
      statusForm.Text = "Muffen neu spleißen"
      statusForm.Minimum = 0
      statusForm.Maximum = 100
      statusForm.ShowDialog()
    End Sub

    Private Sub ExecuteSpliceClosureJob(status As IStatusDisplay, parameters() As SplicingConfig)
      SpliceClosureJob(status, parameters.FirstOrDefault).Wait()
    End Sub

    Public Async Function SpliceClosureJob(status As IStatusDisplay,
                                           config As SplicingConfig) As Task
      If status Is Nothing Then Throw New ArgumentNullException(NameOf(status))

      status.Maximum = 100
      Dim startTime = Date.Now

      Dim progress = New ProgressMonitor()
      Dim exportTask = Task.Run(Function() SpliceClosure(config, progress), CancellationToken.None)
      While Not ReferenceEquals(Await Task.WhenAny(exportTask, Task.Delay(100)).ConfigureAwait(True), exportTask)
        status.Value = CInt(100 * progress.Progress)
      End While

      Await exportTask
    End Function

    Private Function SpliceClosure(config As SplicingConfig, progress As ProgressMonitor) As Boolean
      If Not Dialog.Record.Available Then
        Return False
      End If

      Dim trayModelEMK = FiberOpticTrayModel.Get(Document.Connection, "NAME", "EMK").Single.FID
      Dim trayModelMMK = FiberOpticTrayModel.Get(Document.Connection, "NAME", "MMK").Single.FID
      Dim splitterModel = FiberOpticSplitterModel.GetActiveModels(Document.Connection).Where(Function(sm) sm.Outs.HasValue AndAlso sm.Outs.Value = 32).FirstOrDefault


      Dim allCableModels = FiberOpticCableModel.Get(Document.Connection)
      progress.TotalSteps = config.FIDs.Count + 1

      Using tr = Document.Connection.BeginTransaction()
        Try
          Using ReentrantSwitches.Set(_switchesToFlip)
            Using db = Document.Map.InitiateDrawBurst()
              Using New RegenerateLabelDisableBurst(Document.Connection)
                Dim activeRuleBases As New List(Of RuleBase)

                For Each ruleBase In Document.Connection.RuleBases
                  If ruleBase.Active Then
                    activeRuleBases.Add(ruleBase)
                    ruleBase.Active = False
                  End If
                Next


                Dim splices As New List(Of FiberOpticSplice)
                Dim splitters = New List(Of FiberOpticSplitter)
                Dim splitterPaths = New List(Of FiberOpticSplitterPath)

                For Each fid In config.FIDs
                  progress.IncrementSteps()
                  Dim connectors As New List(Of FiberOpticConnector)

                  Dim closure = FiberOpticClosure.Get(Document.Connection, fid)
                  If Not closure.GetModelName().Contains("NVt10gr") Then Continue For

                  Dim hkCables = closure.GetIncomingCables()
                  If Not hkCables.Any Then Continue For

                  Dim hkFibers = Fiber.Get(Document.Connection, "FID_LINE", hkCables.Select(Function(c) c.FIDGeom.Value)).ToImmutableDictionary(Function(f) f.FIDAttr)
                  Dim hkFibersByCable = hkFibers.ToLookup(Function(f) f.Value.LineFID.Value, Function(f) f.Value)
                  Dim incomingFiberQueue = New Queue(Of Fiber)
                  For Each cable In hkCables.OrderBy(Function(c) c.Name)
                    For Each fiber In hkFibersByCable(cable.FIDGeom.GetValueOrDefault(-1))
                      incomingFiberQueue.Enqueue(fiber)
                    Next
                  Next
                  For i = 1 To config.FeederStartNumber - 1
                    If incomingFiberQueue.Any Then
                      incomingFiberQueue.Dequeue()
                    End If
                  Next

                  Dim existingSplices = closure.GetSplices().Cast(Of FiberOpticSplice)
                  Dim existingTrays = closure.GetTrays()
                  Dim existingSplitters = closure.GetSplitters()

                  If existingSplices.Any Then
                    FiberOpticSplice.RemoveTopologyBatch(existingSplices)
                    Document.Connection.FeatureClasses(FiberOpticSplice.GEOM_TABLE_NAME).DeleteFeatures(existingSplices.Select(Function(s) s.FIDGeom.Value).Distinct)
                    Document.Connection.FeatureClasses(FiberOpticSplice.ATTR_TABLE_NAME).DeleteFeatures(existingSplices.Select(Function(s) s.FIDAttr).Distinct)
                  End If
                  If existingSplitters.Any Then
                    Dim existingSplitterPaths = FiberOpticSplitterPath.Get(Document.Connection, FiberOpticSplitterPath.PointFIDField(Document.Connection).In(existingSplitters.Select(Function(s) s.FIDGeom.GetValueOrDefault(-1))), DAL.Queries.Condition.True, TC.ConditionOperator.ConditionAnd)
                    Document.Connection.FeatureClasses(FiberOpticSplitterPath.GEOM_TABLE_NAME).DeleteFeatures(existingSplitterPaths.Select(Function(s) s.FIDGeom.Value).Distinct)
                    Document.Connection.FeatureClasses(FiberOpticSplitterPath.ATTR_TABLE_NAME).DeleteFeatures(existingSplitterPaths.Select(Function(s) s.FIDAttr).Distinct)

                    Document.Connection.FeatureClasses(FiberOpticSplitter.GEOM_TABLE_NAME).DeleteFeatures(existingSplitters.Select(Function(s) s.FIDGeom.Value).Distinct)
                    Document.Connection.FeatureClasses(FiberOpticSplitter.ATTR_TABLE_NAME).DeleteFeatures(existingSplitters.Select(Function(s) s.FIDAttr).Distinct)
                  End If
                  If existingTrays.Any Then
                    Document.Connection.FeatureClasses(FiberOpticTray.TABLE_NAME).DeleteFeatures(existingTrays.Select(Function(s) s.FID).Distinct())
                  End If

                  'Muffenseite A:
                  Dim trayInfoA As New TrayInfo(72, FiberOpticClosureSide.SIDE_A)
                  For i = 1 To 72
                    CreateTray(closure.FIDGeom.Value, trayModelEMK, trayInfoA, $"EMK {i}")
                    trayInfoA.MoveNext()
                  Next

                  Dim trayInfoB As New TrayInfo(72, FiberOpticClosureSide.SIDE_B)
                  Dim splitterQueue = New Queue(Of FiberOpticSplitter)
                  For i = 1 To 18
                    Dim currentTray = CreateTray(closure.FIDGeom.Value, trayModelEMK, trayInfoB, $"EMK {i}")
                    trayInfoB.MoveNext()

                    For j = 1 To 2
                      If incomingFiberQueue.Any AndAlso splitterQueue.Count < config.SplitterCount Then
                        Dim nextFiber = incomingFiberQueue.Dequeue
                        Dim nextSplitter = CreateSplitter(splitterModel, closure)
                        splitterQueue.Enqueue(nextSplitter.Splitter)
                        splitters.Add(nextSplitter.Splitter)
                        splitterPaths.AddRange(nextSplitter.InPaths)
                        splitterPaths.AddRange(nextSplitter.OutPaths)

                        splices.Add(CreateSplitterSplice(closure.FIDGeom.Value, nextFiber, nextSplitter.InPaths.First, currentTray, j, Nothing))
                      ElseIf i < 4 OrElse i > 12 Then
                        If incomingFiberQueue.Any Then
                          Dim nextFiber = incomingFiberQueue.Dequeue
                          splices.Add(CreateSpliceFiberInTray(closure.FIDGeom.Value, nextFiber, currentTray))
                        End If
                      End If
                    Next
                  Next
                  For i = 19 To 48 Step 2
                    Dim currentTray = CreateTray(closure.FIDGeom.Value, trayModelMMK, trayInfoB, $"MMK {i}")
                    trayInfoB.MoveNext()
                    trayInfoB.MoveNext()

                    If splitterQueue.Any AndAlso i > 19 Then
                      Dim nextSplitter = splitterQueue.Dequeue
                      nextSplitter.TrayFID = currentTray.FID
                      nextSplitter.Name = $"{i}"
                    End If
                  Next
                  For i = 49 To 66
                    CreateTray(closure.FIDGeom.Value, trayModelEMK, trayInfoB, $"EMK {i}")
                    trayInfoB.MoveNext()
                  Next
                  For i = 67 To 72 Step 2
                    CreateTray(closure.FIDGeom.Value, trayModelMMK, trayInfoB, $"MMK {i}")
                    trayInfoB.MoveNext()
                    trayInfoB.MoveNext()
                  Next
                Next

                If splitters.Any Then
                  FiberOpticSplitter.InsertBatch(splitters)
                  FiberOpticSplitterPath.InsertBatch(splitterPaths)
                End If
                If splices.Any Then
                  TC.Common.API.WorkOrder.WorkOrderItemFeature.InsertBatch(splices)
                End If

                tr.Commit()

                For Each ruleBase In activeRuleBases
                  ruleBase.Active = True
                Next
              End Using
            End Using
          End Using
          progress.SetToFinished()
        Catch ex As Exception
          TC.HandleError(ex)
          tr.Rollback()
        End Try
      End Using
      Return True
    End Function



    Private Class TrayInfo
      Private _traysCreated As Long = 1
      Private _trayNumber As Long = 1

      Public ReadOnly Property ClosureSide As Long


      Public ReadOnly Property TraysCreated As Long
        Get
          Return _traysCreated
        End Get
      End Property

      Public ReadOnly Property TrayNumber As Long
        Get
          Return _trayNumber
        End Get
      End Property

      Public ReadOnly Property MaxTrayCount As Long

      Public Sub New(maxTrayCount As Long, closureSide As Long)
        Me.MaxTrayCount = maxTrayCount
        Me.ClosureSide = closureSide
      End Sub

      Public Function MoveNext() As Boolean
        If _traysCreated = MaxTrayCount Then Return False

        _trayNumber += 1
        _traysCreated += 1
        Return True
      End Function
    End Class

    Private Function CreateTray(pointFid As Long, modelFid As Long, trayInfo As TrayInfo) As FiberOpticTray
      Return CreateTray(pointFid, modelFid, trayInfo, trayInfo.TrayNumber.ToString)
    End Function
    Private Function CreateTray(pointFid As Long, modelFid As Long, trayInfo As TrayInfo, name As String) As FiberOpticTray
      Dim tray = FiberOpticTray.CreateNew(Document.Connection)
      tray.PointFID = pointFid
      tray.Name = name
      tray.Number = trayInfo.TrayNumber
      tray.IdClosureSide = trayInfo.ClosureSide
      tray.ModelFID = modelFid
      tray.Insert()
      Return tray
    End Function

    Private Function CreateSplitter(model As FiberOpticSplitterModel, closure As FiberOpticClosure) As (Splitter As FiberOpticSplitter, InPaths As IEnumerable(Of FiberOpticSplitterPath), OutPaths As IEnumerable(Of FiberOpticSplitterPath))
      Dim inPaths = New List(Of FiberOpticSplitterPath)
      Dim outPaths = New List(Of FiberOpticSplitterPath)

      Dim splitter = FiberOpticSplitter.CreateNew(Document.Connection)
      splitter.PointFID = closure.FIDGeom
      splitter.ModelFID = model.FID

      Dim inPath = FiberOpticSplitterPath.CreateNew(Document.Connection)
      inPath.Number = 1
      inPath.PointFID = splitter.FIDGeom
      inPath.ParentFID = closure.FIDGeom
      inPath.TypeID = FiberOpticSplitterPathType.IN
      inPaths.Add(inPath)

      For j = 1 To 32
        Dim splitterpath = FiberOpticSplitterPath.CreateNew(Document.Connection)
        splitterpath.Number = j
        splitterpath.PointFID = splitter.FIDGeom
        splitterpath.ParentFID = closure.FIDGeom
        splitterpath.TypeID = FiberOpticSplitterPathType.OUT
        outPaths.Add(splitterpath)
      Next

      Return (splitter, inPaths, outPaths)
    End Function


    Private Function CreateSplitterSplice(pointFid As Long, fiber1 As Fiber, splitterPath2 As FiberOpticSplitterPath, tray As FiberOpticTray, placeNumber? As Long, status As TC.Common.API.Status) As FiberOpticSplice
      Dim splice = FiberOpticSplice.CreateNew(Document.Connection)
      splice.Fiber1FID = fiber1?.FIDAttr
      splice.SplitterPath2FID = splitterPath2?.FIDAttr
      splice.SetTray(tray)
      splice.PlaceNumber = placeNumber
      splice.PointFID = pointFid
      splice.Active = True
      If status IsNot Nothing Then
        splice.StatusFID = status.FID
      End If
      Return splice
    End Function


    Private Function CreateSpliceFiberInTray(pointFid As Long, fiber1 As Fiber, tray As FiberOpticTray) As FiberOpticSplice
      Dim splice = FiberOpticSplice.CreateNew(Document.Connection)
      splice.Fiber1FID = fiber1.FIDAttr
      splice.SetTray(tray)
      splice.PointFID = pointFid
      splice.Active = True
      Return splice
    End Function

    Public Class SplicingConfig
      Public Property FIDs As IEnumerable(Of Long)
      Public Property FeederStartNumber As Long
      Public Property SplitterCount As Long
    End Class
  End Class
End Namespace
