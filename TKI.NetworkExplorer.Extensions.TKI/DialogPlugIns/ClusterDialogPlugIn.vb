Imports System.Threading
Imports Autodesk.AutoCAD.ApplicationServices
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports Autodesk.Map.IM.PlaneGeometry
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.NetworkExplorer.Tools.Caching
Imports TKI.Tools
Imports TKI.Tools.Database
Imports TKI.UI
Imports TKI.UI.Configuration

Namespace DialogPlugIns

  Public Class ClusterDialogPlugIn
    Inherits TC.Base.PlugIn.DialogPlugIn.TelcoItemDialogPlugIn

    Private WithEvents _mniMagicDtag As MenuItem
    Private WithEvents _mniCreateSplices As MenuItem
    Private WithEvents _mniCreatePipes As MenuItem
    Private WithEvents _mniMagicSegments As MenuItem
    Private WithEvents _mniDeleteDtag As MenuItem

    Private WithEvents _mniFitDucts As MenuItem
    Private WithEvents _mniUpdateStatus As MenuItem
    Private WithEvents _mniRenameDucts As MenuItem
    Private WithEvents _mniCreateInsertions As MenuItem

    Private WithEvents _mniExportNetBuildData As MenuItem

    Public Sub New()
      _mniMagicDtag = New MenuItem()
      _mniCreateSplices = New MenuItem()
      _mniCreatePipes = New MenuItem()
      _mniMagicSegments = New MenuItem()
      _mniDeleteDtag = New MenuItem()
      _mniFitDucts = New MenuItem()
      _mniUpdateStatus = New MenuItem()
      _mniRenameDucts = New MenuItem()
      _mniCreateInsertions = New MenuItem()
      _mniExportNetBuildData = New MenuItem()
    End Sub

    Public Overrides Function LicenseAvailable() As Boolean
      Return True
    End Function

    Private ReadOnly _gfnwEngineeringCode As New DMCodeNumber(9117, 0, 19221)

    Friend Function IsGFNWEnabled(connection As TBConnection) As Boolean
      If connection Is Nothing Then Return False
      Return connection.DataModelCodes.OfType(Of DMCode).Where(Function(dmCode) dmCode.Code.Equals(_gfnwEngineeringCode)).Any()
    End Function

    Private Sub TC_CLUSTER_InitMenu(sender As Object, e As MenuEventArgs) Handles Me.InitMenu
      If Document.Connection.FeatureClasses.Contains("TC_DC_SEGMENT") Then
        TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniMagicDtag, "Magie wirken", TC.Common.Tools.PictureType.NO_Picture)
        'TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniDeleteDtag, "Daten in Cluster löschen", TC.Common.Tools.PictureType.NO_Picture)
      End If
      TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniMagicSegments, "Trassen vereinfachen", TC.Common.Tools.PictureType.NO_Picture)
      TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniCreateSplices, "Schummeltool: Spleißen", TC.Common.Tools.PictureType.NO_Picture)
      TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniCreatePipes, "Schummeltool: Schutzrohre anlegen", TC.Common.Tools.PictureType.NO_Picture)

      If IsGFNWEnabled(Document.Connection) Then
        TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniUpdateStatus, "Telekom Doku: Status im Cluster updaten", TC.Common.Tools.PictureType.NO_Picture)
        TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniFitDucts, "Telekom Doku: Rohre fitten", TC.Common.Tools.PictureType.NO_Picture)
        TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniRenameDucts, "Telekom Doku: Mikroröhrchen benennen", TC.Common.Tools.PictureType.NO_Picture)
        TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniCreateInsertions, "Telekom Doku: Rohrabschlüsse anlegen", TC.Common.Tools.PictureType.NO_Picture)
      End If

#If DEBUG Then
      'TC.Common.Tools.MenuItemHelper.AddMenuItem(mnuEdit, _mniExportNetBuildData, "NET Build Daten exportieren", TC.Common.Tools.PictureType.NO_Picture)
#End If
    End Sub

    Private Sub MniFitDucts_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniFitDucts.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If

      Try
        Dim fileEntry = New FileEntry("CSV-Datei", "Der Optimierer hat immer 21!", Configuration.FileEntry.Mode.OpenFile, "CSV-Datei (*.csv)|*.csv", True, {New Validation.IsNotEmptyOrWhitespaceValidationRule})
        If ShowDialog({fileEntry}) Then
          Dim assistant = New DuctFitting.DuctFittingAssistent(Document)
          assistant.RunAssistant(Dialog.Record.Fid, fileEntry.Value)
        End If
      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniSimplifySegments_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniMagicSegments.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If
      Dim res = System.Windows.MessageBox.Show("Sicher?", "Trassen vereinfachen", MessageBoxButton.YesNo)
      If res <> MessageBoxResult.Yes Then Return
      Try
        Dim vCluster = Cluster.Get(Document.Connection, Dialog.Record.Fid)
        If vCluster Is Nothing Then Return
        For Each acadDialog In Document.Dialogs.OfType(Of Dialog)
          If Not acadDialog.Name.Equals("TC_CLUSTER") AndAlso acadDialog.Visible Then
            acadDialog.Close()
          End If
        Next

        Dim clusterName = vCluster.Name
        clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

        Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf SimplifySegmentsJob)
        statusForm.AbortThreadOnCancel = True
        statusForm.CancelButtonVisible = True
        statusForm.CancelButtonEnabled = True
        statusForm.Parameters = {vCluster, Document}
        statusForm.Status = ""
        statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
        statusForm.Text = "Vereinfache Trassen..."
        statusForm.ShowDialog()

      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniDeleteDtag_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniDeleteDtag.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If
      Dim res = System.Windows.MessageBox.Show("Sicher?", "Cluster leeren", MessageBoxButton.YesNo)
      If res <> MessageBoxResult.Yes Then Return
      Try
        Dim vCluster = Cluster.Get(Document.Connection, Dialog.Record.Fid)
        If vCluster Is Nothing Then Return
        For Each acadDialog In Document.Dialogs.OfType(Of Dialog)
          If Not acadDialog.Name.Equals("TC_CLUSTER") AndAlso acadDialog.Visible Then
            acadDialog.Close()
          End If
        Next

        Dim clusterName = vCluster.Name
        clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

        Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf DeleteDTAGJob)
        statusForm.AbortThreadOnCancel = True
        statusForm.CancelButtonVisible = True
        statusForm.CancelButtonEnabled = True
        statusForm.Parameters = {vCluster, Document}
        statusForm.Status = ""
        statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
        statusForm.Text = "Lösche NET-Daten..."
        statusForm.ShowDialog()

      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniUpdateStatus_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniUpdateStatus.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If

      Try
        Dim vCluster = Cluster.Get(Document.Connection, Dialog.Record.Fid)
        If vCluster Is Nothing Then Return
        For Each acadDialog In Document.Dialogs.OfType(Of Dialog)
          If Not acadDialog.Name.Equals("TC_CLUSTER") AndAlso acadDialog.Visible Then
            acadDialog.Close()
          End If
        Next

        Dim clusterName = vCluster.Name
        clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

        Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf UpdateStatusJob)
        statusForm.AbortThreadOnCancel = True
        statusForm.CancelButtonVisible = True
        statusForm.CancelButtonEnabled = True
        statusForm.Parameters = {vCluster, Document}
        statusForm.Status = ""
        statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
        statusForm.Text = "Update Status..."
        statusForm.ShowDialog()

      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniRenameDucts_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniRenameDucts.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If

      Try
        Dim vCluster = Cluster.Get(Document.Connection, Dialog.Record.Fid)
        If vCluster Is Nothing Then Return
        For Each acadDialog In Document.Dialogs.OfType(Of Dialog)
          If Not acadDialog.Name.Equals("TC_CLUSTER") AndAlso acadDialog.Visible Then
            acadDialog.Close()
          End If
        Next

        Dim clusterName = vCluster.Name
        clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

        Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf RenameDuctsJob)
        statusForm.AbortThreadOnCancel = True
        statusForm.CancelButtonVisible = True
        statusForm.CancelButtonEnabled = True
        statusForm.Parameters = {vCluster, Document}
        statusForm.Status = ""
        statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
        statusForm.Text = "Update Duct names..."
        statusForm.ShowDialog()

      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniCreateInsertions_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniCreateInsertions.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If

      Try
        Dim ductInsertionHelper = New CreateDuctInsertions(Document)
        ductInsertionHelper.RunCreateDuctInsertions(Dialog.Record.Fid)
      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniCreateSplices_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniCreateSplices.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If

      Try
        Dim assistant = New BMVI.SplicingAssistent(Document)
        assistant.RunAssistant(Dialog.Record.Fid)
      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniCreatePipes_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniCreatePipes.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If

      Try
        Dim creator = New BMVI.PipeCreator(Document)
        creator.Run(Dialog.Record.Fid)
      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniMagicDtag_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniMagicDtag.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If

      Try
        Dim assistant = New DTAG.DTAGAssistent(Document)
        assistant.RunAssistant(Dialog.Record.Fid)
      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("MagicDTAG", "TKI"), ex)
      End Try
    End Sub

    Private Sub MniExportNetBuildData_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniExportNetBuildData.Click
      If Not Dialog.Record.Available OrElse Dialog.Mode <> DialogMode.View Then
        Return
      End If

      Try
        Dim vCluster = Cluster.Get(Document.Connection, Dialog.Record.Fid)
        If vCluster Is Nothing Then Return
        For Each acadDialog In Document.Dialogs.OfType(Of Dialog)
          If Not acadDialog.Name.Equals("TC_CLUSTER") AndAlso acadDialog.Visible Then
            acadDialog.Close()
          End If
        Next

        Dim clusterName = vCluster.Name
        clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

        Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf ExportNetBuildDataJob)
        statusForm.AbortThreadOnCancel = True
        statusForm.CancelButtonVisible = True
        statusForm.CancelButtonEnabled = True
        statusForm.Parameters = {vCluster, Document}
        statusForm.Status = ""
        statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
        statusForm.Text = "Exporting NET Build data..."
        statusForm.ShowDialog()

      Catch ex As System.Exception
        ErrorReport.ShowError(New ErrorReport.BrandingConfiguration("NET Build export", "TKI"), ex)
      End Try
    End Sub
  End Class

End Namespace
