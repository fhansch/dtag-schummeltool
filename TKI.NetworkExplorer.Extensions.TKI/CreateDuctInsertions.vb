Imports System.Threading
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Doc.Topologies
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.NetworkExplorer.Tools.Caching
Imports TKI.Tools
Imports TKI.Tools.Database

Friend Class CreateDuctInsertions
  Private _errors As IList(Of DTAG.DTAGError)
  Private Property Document As Autodesk.Map.IM.Forms.Document
  Friend Sub New(doc As Autodesk.Map.IM.Forms.Document)
    _errors = New List(Of DTAG.DTAGError)
    _Document = doc
  End Sub


  Friend Sub RunCreateDuctInsertions(fid As Long)
    Dim vCluster = Cluster.Get(Document.Connection, fid)
    If vCluster Is Nothing Then Return

    _errors.Clear()
    For Each acadDialog In Document.Dialogs.OfType(Of Dialog)
        If Not acadDialog.Name.Equals("TC_CLUSTER") AndAlso acadDialog.Visible Then
          acadDialog.Close()
        End If
      Next

      Dim clusterName = vCluster.Name
      clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

      Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf CreateDuctInsertionsJob)
      statusForm.AbortThreadOnCancel = True
      statusForm.CancelButtonVisible = True
      statusForm.CancelButtonEnabled = True
      statusForm.Parameters = {vCluster, Document}
      statusForm.Status = ""
      statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
      statusForm.Text = "Creating duct insertions..."
      statusForm.ShowDialog()

    If _errors.Any Then
      Dim errorLog = New TC.frmErrorLog(Document)
      errorLog.Visible = False
      errorLog.ClearError()
      For Each dtagError In _errors
        If dtagError.FID.HasValue Then
          errorLog.AddNewError(dtagError.Text, dtagError.TableName, dtagError.FID.Value, False)
        Else
          errorLog.AddNewError(dtagError.Text, dtagError.TableName, 0, False)
        End If
      Next
      errorLog.Visible = True
      errorLog.ShowError()
    End If
  End Sub


  Private Sub CreateDuctInsertionsJob(status As IStatusDisplay, parameters() As Object)
    If parameters.Length <> 2 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
    If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))
    If TypeOf parameters(1) IsNot Document Then Throw New ArgumentException($"The parameter 2 has the wrong type. Expected: {NameOf(Document)}", NameOf(parameters))

    Dim vCluster = DirectCast(parameters(0), Cluster)
    Dim vDocument = DirectCast(parameters(1), Document)
    _errors = RunCreateDuctInsertionsJob(status, vCluster, vDocument, CancellationToken.None).Result
  End Sub

  Private ReadOnly _switchesToFlip As ReentrantSwitch() = {
    TC.Common.API.DuctInfrastructure.Duct.SuspendDuctInsertionChecks,
    TC.Common.API.DuctInfrastructure.Duct.DisableRenamingOfInnerDucts,
    TC.Common.API.DuctInfrastructure.Duct.DisableRenamingOfConnectedDucts,
    TC.Common.API.DuctInfrastructure.Duct.DisableCalculationOfDuctLength,
    TC.Common.API.DuctInfrastructure.Duct.DisableAutomaticDuctInsertionIntoManhole,
    TC.Common.API.DuctInfrastructure.DuctCable.SuspendAssignmentChecks,
    TC.Common.API.DuctInfrastructure.DuctCable.SuspendAssignCableToConnectedDucts
  }

  Private Async Function RunCreateDuctInsertionsJob(status As IStatusDisplay, vCluster As Cluster, document As Document, token As CancellationToken) As Task(Of IList(Of DTAG.DTAGError))
    Dim savedDuctConnectionInsertionState = TC.Common.API.DuctInfrastructure.Duct.AllowInsertingOfDuctConnections
    TC.Common.API.DuctInfrastructure.Duct.AllowInsertingOfDuctConnections = True
    Dim errors = New List(Of DTAG.DTAGError)
    Try
      Using db As New DatabaseWorker(vCluster.Connection)
        Dim lineFc = db.Connection.FeatureClasses(Duct.GEOM_TABLE_NAME)
        Dim pointFc = db.Connection.FeatureClasses(DuctInsertion.GEOM_TABLE_NAME)
        Using ReentrantSwitches.Set(_switchesToFlip)
          Using New RegenerateLabelDisableBurst(db.Connection)
            Using mapBurst = document.Map.InitiateDrawBurst()
              Using Await db.StartTopologyBurst({lineFc, pointFc}, token).ConfigureAwait(False)

                Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
                Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)
                Dim newInsertions = New List(Of DuctInsertion)
                Dim newTopologies = New List(Of (Feature, Feature))
                Dim newModels = New List(Of DuctInsertionModel)

                Dim ductsByFid = Await Duct.GetCache(cache)
                Dim ductModelsByFid = Await DuctType.GetCache(cache)
                Dim ductInsertionModelsByFid = Await DuctInsertionModel.GetCache(cache)
                Dim ductInsertionModelsByName = New Dictionary(Of String, DuctInsertionModel)
                For Each ductInsertionModel In ductInsertionModelsByFid.Values
                  If String.IsNullOrWhiteSpace(ductInsertionModel.Name) Then Continue For
                  ductInsertionModelsByName(ductInsertionModel.Name) = ductInsertionModel 'Lieber so, wer weiß ob mal was doppelt angelegt wurde
                Next
                For Each ductModel In ductModelsByFid.Values
                  If String.IsNullOrWhiteSpace(ductModel.Name) Then Continue For
                  If ductInsertionModelsByName.ContainsKey(ductModel.Name) Then Continue For
                  Dim newDuctInsertionModel = Await db.ExecuteOnDatabase(Function(c) DuctInsertionModel.CreateNew(db.Connection), token)
                  newDuctInsertionModel.Name = ductModel.Name
                  ductInsertionModelsByName(ductModel.Name) = newDuctInsertionModel
                  newModels.Add(newDuctInsertionModel)
                Next

                Dim existingDuctInsertionsbyDuctFid = Await cache.DuctInsertionsByDuctFid()
                For Each duct In ductsByFid.Values.Distinct
                  Dim insertions = existingDuctInsertionsbyDuctFid(duct.FID)
                  Dim fittings = Await cache.GetFittingsByDuctFid(duct.FID)
                  If insertions.Count + fittings.Count > 1 Then Continue For 'Fall 1: Es gibt 2 Abschlüsse / Fittinge - easy!
                  Dim geom = Await cache.GetFeatureGeometry(duct)
                  Dim ductModel As DuctType = If(ductModelsByFid.ContainsKey(duct.ModelFID.GetValueOrDefault(-1)), ductModelsByFid(duct.ModelFID.Value), Nothing)
                  Dim insertionModel As DuctInsertionModel = If(Not String.IsNullOrEmpty(ductModel?.Name) AndAlso ductInsertionModelsByName.ContainsKey(ductModel?.Name), ductInsertionModelsByName(ductModel.Name), Nothing)

                  If geom Is Nothing Then
                    errors.Add(New DTAG.DTAGError($"Rohr ""{duct.Name}"" hat keine gültige Geometrie und wurde übersprungen.", duct.FeatureClass.Name, duct.FID))
                    Continue For
                  End If

                  If insertions.Count + fittings.Count = 1 Then
                    'Fall 2: Ein Ende ist offen, jetzt kommt der Best Guess:
                    Dim connectedEndPoint = TryCast(If(insertions.Any, insertions.First.Geometry, fittings.First.Geometry), Point)
                    If connectedEndPoint Is Nothing Then
                      Debug.Fail("Abbruch! Keine Geometrie an vorhandenem Endpunkt!")
                      Continue For 'Irgendwas ist hier falsch!
                    End If
                    Dim startDistance = geom.StartPoint.Distance2DFunction(connectedEndPoint)
                    Dim endDistance = geom.EndPoint.Distance2DFunction(connectedEndPoint)
                    Dim point = If(startDistance > endDistance, geom.StartPoint, geom.EndPoint)
                    If Not CheckPointGeom(point, vCluster, cache.Tolerance) Then Continue For
                    Dim newInsertion As DuctInsertion = Await CreateDuctInsertionForPoint(point, duct, insertionModel, db, token)
                    newInsertions.Add(newInsertion)
                    newTopologies.Add((newInsertion.GeomFeature, duct.GeomFeature))
                  Else
                    'Fall 3: Beide Enden sind offen, das ist einfach!
                    For Each point In {geom.StartPoint, geom.EndPoint}
                      If Not CheckPointGeom(point, vCluster, cache.Tolerance) Then Continue For
                      Dim newInsertion As DuctInsertion = Await CreateDuctInsertionForPoint(point, duct, insertionModel, db, token)
                      newInsertions.Add(newInsertion)
                      newTopologies.Add((newInsertion.GeomFeature, duct.GeomFeature))
                    Next
                  End If
                Next

                Dim topologies = db.Connection.Topologies
                If Not topologies.AreEntriesLoaded Then
                  Await db.ExecuteOnDatabase(Sub(c) topologies.LoadLazyValues(), token).ConfigureAwait(False)
                End If
                Dim topology = TryCast(topologies.Get("TC"), LogicalTopology)
                If topology Is Nothing Then Return errors

                Using tContainer = Await db.GetTransactionContainer(topology, token).ConfigureAwait(False)
                  Dim topoTransaction = tContainer.GetOrCreateTransaction()

                  For Each topoEntry In newTopologies
                    Dim fromTopologyFeature = Await db.ExecuteOnDatabase(Function() topoTransaction.ConvertToTopologyFeature(topoEntry.Item1), token).ConfigureAwait(False)
                    Dim toTopologyFeature = Await db.ExecuteOnDatabase(Function() topoTransaction.ConvertToTopologyFeature(topoEntry.Item2), token).ConfigureAwait(False)

                    topoTransaction.AddLink(fromTopologyFeature, toTopologyFeature, LogicalTopology.Flow.Both)
                  Next
                End Using

                If newModels.Any Then
                  newModels.First.FeatureClass.InsertFeatures(newModels.Select(Function(m) m.Feature))
                End If
                If newInsertions.Any Then
                  DuctInsertion.InsertBatch(newInsertions)
                End If
              End Using
            End Using
          End Using
        End Using
      End Using

    Catch ex As Exception
      Throw New Exception("Error creating DuctInsertions", ex)
    Finally
      TC.Common.API.DuctInfrastructure.Duct.AllowInsertingOfDuctConnections = savedDuctConnectionInsertionState
    End Try
    Return errors
  End Function

  Private Async Function CreateDuctInsertionForPoint(point As LinePoint, duct As Duct, model As DuctInsertionModel, db As DatabaseWorker, token As CancellationToken) As Task(Of DuctInsertion)
    Dim newInsertion = Await db.ExecuteOnDatabase(Function(c) DuctInsertion.CreateNew(db.Connection), token)
    newInsertion.Geometry = New Point(point.X, point.Y)
    newInsertion.DuctFID = duct.FID
    newInsertion.ModelFID = model?.FID
    newInsertion.StatusFID = duct.StatusFID
    Return newInsertion
  End Function

  Private Function CheckPointGeom(point As LinePoint, vCluster As Cluster, tolerance As Double) As Boolean
    Dim geom = TryCast(vCluster.Geometry, Polygon)
    If geom Is Nothing Then Return False
    Return geom.AnyInteract2DPredicate(tolerance)(point)
  End Function
End Class
