Imports System.Threading
Imports Autodesk.AutoCAD.ApplicationServices
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Doc.Topologies
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports Autodesk.Map.IM.PlaneGeometry
Imports TC
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.Common.API.Topography
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.Tools
Imports TKI.Tools.Database
Imports TKI.UI
Imports TKI.UI.Configuration

Namespace DuctFitting
  Friend Class DuctFittingAssistent
    Private ReadOnly _switchesToFlip As ReentrantSwitch() = {Duct.SuspendDuctInsertionChecks, Duct.DisableRenamingOfInnerDucts, Duct.DisableRenamingOfConnectedDucts,
                                                           Duct.DisableCalculationOfDuctLength, Duct.DisableAutomaticDuctInsertionIntoManhole, DuctCable.SuspendAssignmentChecks,
                                                           DuctCable.SuspendAssignCableToConnectedDucts}

    Private _errors As IList(Of DTAG.DTAGError)
    Private Property Document As Autodesk.Map.IM.Forms.Document
    Friend Sub New(doc As Autodesk.Map.IM.Forms.Document)
      _errors = New List(Of DTAG.DTAGError)
      _Document = doc
    End Sub

    Friend Sub RunAssistant(clusterFid As Long, filepath As String)
      Dim vCluster = Cluster.Get(Document.Connection, clusterFid)
      If vCluster Is Nothing Then Return

      _errors.Clear()
      Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf CreateDuctFittingJob)
      statusForm.AbortThreadOnCancel = True
      statusForm.CancelButtonVisible = True
      statusForm.CancelButtonEnabled = True
      statusForm.Parameters = {vCluster, filepath}
      statusForm.Status = ""
      statusForm.Maximum = 100
      statusForm.Type = Desktop.ProgressBarType.ProgressBar
      statusForm.Text = "Rohre werden gefittet"
      statusForm.ShowDialog()
      '  End If
      If _errors.Any Then
        Dim errorLog = New TC.frmErrorLog(Document)
        errorLog.Visible = False
        errorLog.ClearError()
        For Each dtagError In _errors
          If dtagError.FID.HasValue Then
            errorLog.AddNewError(dtagError.Text, dtagError.TableName, dtagError.FID.Value, False)
          Else
            errorLog.AddNewError(dtagError.Text, dtagError.TableName, 0, False)
          End If
        Next
        errorLog.Visible = True
        errorLog.ShowError()
      End If
    End Sub



    Private Sub CreateDuctFittingJob(status As IStatusDisplay, parameters() As Object)
      If parameters.Length <> 2 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
      If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))

      Dim vCluster = DirectCast(parameters(0), Cluster)
      Dim filePath = CStr(parameters(1))

      _errors = RunDuctFittingJob(status, vCluster, filePath, CancellationToken.None).Result
    End Sub

    Private Async Function RunDuctFittingJob(status As IStatusDisplay, vCluster As Cluster, filepath As String, token As CancellationToken) As Task(Of IList(Of DTAG.DTAGError))
      Using db As New DatabaseWorker(vCluster.Connection, 0)
        'Using ReentrantSwitches.Set(_switchesToFlip)
        Using burst As New RegenerateLabelDisableBurst(db.Connection)
          Using mapBurst = Document.Map.InitiateDrawBurst()
            'Using transaction = Await db.BeginTransaction(token)
            Try
              'FeatureRuleHelper.SuspendDeleteSegmentDuct()
              'FeatureRuleHelper.SuspendDeleteDuctDuct()
              'FeatureRuleHelper.SuspendDeleteDuctCable()

              Dim errors = Await ExecuteInternal(status, db, vCluster, filepath, token)

              ' Await transaction.Commit(token)
              If errors.Any Then
                Return errors
              End If
            Catch ex As Exception
              HandleError(ex)
            Finally
              FeatureRuleHelper.ResumeDeleteDuctDuct()
                FeatureRuleHelper.ResumeDeleteSegmentDuct()
                FeatureRuleHelper.ResumeDeleteDuctCable()
              End Try
            ' End Using
          End Using
        End Using
        'End Using
      End Using
      Return New List(Of DTAG.DTAGError)
    End Function

    Private Async Function ExecuteInternal(status As IStatusDisplay, db As DatabaseWorker, vCluster As Cluster, filepath As String, token As CancellationToken) As Task(Of IList(Of DTAG.DTAGError))
      Dim errors = New List(Of DTAG.DTAGError)
      Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
      Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)

      'STEP 1: TODO: Excel laden
      status.Status = "Lade Quelldaten..."
      Dim data = New List(Of (BuildingName As String, DuctBundleName As String, PositionNumber As Integer, IsContinousMicroDuct As Boolean))
      Using reader = New Tools.Data.CsvReader(filepath)
        While Await reader.ReadNextLineAsync()
          Try
            Dim buildingName = reader.ReadString(2)
            If String.IsNullOrEmpty(buildingName) Then Continue While
            Dim ductBundleName = reader.ReadString(0)
            If String.IsNullOrEmpty(ductBundleName) Then Continue While
            Dim positionNumber = reader.ReadInteger(1)
            Dim isContinous = reader.ReadInteger(3)
            data.Add((buildingName, ductBundleName, positionNumber, isContinous = 1))
          Catch ex As Exception
            'ignore
          End Try
        End While
      End Using
      status.Value = 10
      status.Maximum = 10 + data.Count


      'STEP 2: Auflösen des Knotens:
      For Each buildingInfo In data
        'Gebäude suchen:
        Dim splitDucts = New List(Of Duct)
        Dim intersectionPoint As LinePoint = Nothing
        Dim ductLeadIn As Duct = Nothing
        Dim ductInsertion As DuctInsertion = Nothing
        Dim building As Building = Nothing

        Using transaction = Await db.BeginTransaction(token)

          status.Value += 1

          If String.IsNullOrWhiteSpace(buildingInfo.BuildingName) Then Continue For
          Dim possibleMatches As IEnumerable(Of Building) = Await TryMatchBuildingsByName(cache, buildingInfo.BuildingName)
          If Not possibleMatches.Any Then
            errors.Add(New DTAG.DTAGError($"Kein Gebäude gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", Nothing, Nothing))
            Continue For
          ElseIf possibleMatches.Count > 1 Then
            errors.Add(New DTAG.DTAGError($"Mehr als ein Gebäude gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", Nothing, Nothing))
            Continue For
          End If
          Building = possibleMatches.FirstOrDefault
          Try
            Dim ductInsertions = building.GetDuctInsertions()
            If Not ductInsertions.Any Then
              errors.Add(New DTAG.DTAGError($"Kein Rohrabschluss gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", building.FeatureClass.Name, building.FID))
              Continue For
            ElseIf ductInsertions.Count > 1 Then
              Dim firstMicroDuctInsertions = New List(Of DuctInsertion)
              For Each ductInsert In ductInsertions
                Dim connectedDuct = ductInsert.GetDuct
                If connectedDuct Is Nothing Then Continue For
                If connectedDuct.HasDirectInnerDucts Then Continue For
                Dim position = connectedDuct.GetOuterDuctDucts?.FirstOrDefault?.PositionNumber
                If position.GetValueOrDefault(-1) <> 1 Then Continue For
                firstMicroDuctInsertions.Add(ductInsert)
              Next
              ductInsertions = firstMicroDuctInsertions
              If ductInsertions.Count > 1 Then
                errors.Add(New DTAG.DTAGError($"Mehr als ein Rohrabschluss gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", building.FeatureClass.Name, building.FID))
                Continue For
              End If
            End If
            DuctInsertion = ductInsertions.FirstOrDefault
            ductLeadIn = ductInsertion.GetDuct()
            If ductLeadIn Is Nothing Then
              errors.Add(New DTAG.DTAGError($"Kein HP+ Rohr gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", ductInsertion.FeatureClass.Name, ductInsertion.FID))
              Continue For
            End If
            If ductLeadIn.GetFittings().Any Then Continue For
            Dim buildingDuctInsertions = ductLeadIn.GetDuctInsertions
            If buildingDuctInsertions.Count > 1 Then
              If Not GetNvtInsertions(buildingDuctInsertions).Any Then
                errors.Add(New DTAG.DTAGError($"HP+ Rohr hat 2 Rohrabschlüsse aber keine Verbindung zum NVt an Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", ductLeadIn.FeatureClass.Name, ductLeadIn.FID))
              End If
              Continue For 'Direkte Verbindung zu NVT (MR durchgehend)
            End If
            Dim ductGeom = ductLeadIn.GetAssignedGeometry
            If ductGeom Is Nothing Then
              errors.Add(New DTAG.DTAGError($"HP+ Rohr hat keine gültige Geometrie für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", ductLeadIn.FeatureClass.Name, ductLeadIn.FID))
              Continue For
            End If
            intersectionPoint = If(ductGeom.StartPoint.Distance2D(ductInsertion.Point) < cache.Tolerance, ductGeom.EndPoint, ductGeom.StartPoint)
            Dim segments = Await TryGetSegmentsAtPoint(cache, intersectionPoint)
            Dim ductBundles As IEnumerable(Of Duct) = GetMatchingDuctBundlesFromSegments(segments, buildingInfo.DuctBundleName)
            If Not ductBundles.Any Then
              errors.Add(New DTAG.DTAGError($"Keine Verbindung von HP+ zu Rohrverband gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", ductLeadIn.FeatureClass.Name, ductLeadIn.FID))
              Continue For
            End If
            'Dim ductBundle = ductBundles.FirstOrDefault 'ist egal welches wir nehmen weil unser Ziel-MR ist noch durchgehend und damit im Zweifel sowieso in beiden Verbänden
            Dim innerducts = New HashSet(Of Duct)
            For Each ductBundle In ductBundles
              Dim innerD = ductBundle.GetInnerDuctDucts.Where(Function(dd) dd.PositionNumber = buildingInfo.PositionNumber).FirstOrDefault?.GetInnerDuct
              If innerD IsNot Nothing Then
                innerducts.Add(innerD)
              End If
            Next
            Dim innerduct = GetNvtCandidate(innerducts)
            If Not innerducts.Any Then
              errors.Add(New DTAG.DTAGError($"Kein inneres Rohr an dieser Position im Verband gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", ductBundles.First.FeatureClass.Name, ductBundles.First.FID))
              Continue For
            ElseIf innerduct Is Nothing Then
              errors.Add(New DTAG.DTAGError($"Keine Verbindung von HP+ zum NVt gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", ductLeadIn.FeatureClass.Name, ductLeadIn.FID))
              Continue For
            End If
            Dim otherSegment As SurveyPlan.Segment = Nothing
            Dim otherDuct As Duct = Nothing

            Dim segmentsWithDuct = segments.Where(Function(s) s.GetDucts().Contains(innerduct))
            Dim segment = segmentsWithDuct.First
            If segmentsWithDuct.Count > 1 OrElse (Not segment.Start.Distance2D(intersectionPoint) < cache.Tolerance AndAlso Not segment.End.Distance2D(intersectionPoint) < cache.Tolerance) Then
              SplitDuct(innerduct, segmentsWithDuct.First, intersectionPoint, otherDuct)
            End If
            If otherDuct IsNot Nothing Then
              splitDucts.Add(otherDuct)
            End If
            splitDucts.Add(innerduct)
            innerduct = GetNvtCandidate(splitDucts)

            If innerduct Is Nothing Then
              errors.Add(New DTAG.DTAGError($"Keine Verbindung von HP+ zum NVt gefunden für Adresse: ""{buildingInfo.BuildingName}"" in Rohrverband ""{buildingInfo.DuctBundleName}"".", ductLeadIn.FeatureClass.Name, ductLeadIn.FID))
              Continue For
            End If

            If splitDucts.Count > 1 Then
              For Each fitting In splitDucts(0).GetFittings
                If fitting.GetConnectedDucts.Contains(splitDucts(1)) Then
                  fitting.Delete()
                  Exit For
                End If
              Next
            Else
              innerduct.GetFittings.Where(Function(f) f.Geometry.Distance2DFunction(intersectionPoint) < db.Connection.DefaultSpatialTolerance).FirstOrDefault?.Delete()
              innerduct.GetDuctInsertions.Where(Function(f) f.Geometry.Distance2DFunction(intersectionPoint) < db.Connection.DefaultSpatialTolerance).FirstOrDefault?.Delete()
            End If
            If buildingInfo.IsContinousMicroDuct Then
              For Each segment In ductLeadIn.GetAllDirectSegments
                segment.AddDuct(innerduct)
              Next
              Dim newInsertion = innerduct.AddDuctInsertion(TryCast(ductInsertion.Geometry, Point), building, ductInsertion.ModelFID)
              'newInsertion.Insert()
              ductLeadIn.Delete()
            Else
              innerduct.AddFitting(ductLeadIn, intersectionPoint)
            End If

          Catch ex As Exception
            'Ignore!
            errors.Add(New DTAG.DTAGError(ex.ToString, Building.TABLE_NAME, Building.FID))
          End Try

          Await transaction.Commit(token)
        End Using
      Next

      Return errors
    End Function

    Private Function GetNvtCandidate(ducts As IEnumerable(Of Duct)) As Duct
      For Each duct In ducts
        Dim insertions = New List(Of DuctInsertion)
        Dim checkedDucts = New HashSet(Of Long)(ducts.Except({duct}).Select(Function(d) d.FID))
        Dim ductsToCheck = New Queue(Of Duct)({duct})
        While Not insertions.Any AndAlso ductsToCheck.Any
          Dim nextDuct = ductsToCheck.Dequeue
          If Not checkedDucts.Add(nextDuct.FID) Then Continue While
          insertions.AddRange(nextDuct.GetDuctInsertions)
          For Each fitDuct In nextDuct.GetConnectedDuctsByFitting()
            ductsToCheck.Enqueue(fitDuct)
          Next
        End While
        If GetNvtInsertions(insertions).Any Then
          Return duct
        End If
      Next
      Return Nothing
    End Function

    Private Function GetNvtInsertions(insertions As IEnumerable(Of DuctInsertion)) As IEnumerable(Of DuctInsertion)
      Return insertions.Where(Function(d) d.PointFID.HasValue).ToArray
    End Function

    Private Sub SplitDuct(vDuct As Duct, vSegment As Segment, splitPoint As LinePoint, ByRef otherduct As Duct)
      If vDuct Is Nothing OrElse vSegment Is Nothing Then Return

      'Wenn Rohr eine Geometrie besitzt, dann Trennung der Trassen über SplitDuctWithGeometry
      If vDuct.Geometry IsNot Nothing Then
        Me.SplitDuctWithGeometry(vDuct, splitPoint, AddFittingMode.All)
        Return
      End If

      If splitPoint Is Nothing Then Return

      'Berechnung des nächstgelegenen Punktes
      Dim closestPoint = Me.GetClosestPoint(DirectCast(vSegment.Geometry, LineString), splitPoint)

      If Me.ValidateDistanceBetweenTwoPointsForSegment(vDuct.Connection, closestPoint, splitPoint.ToPoint2D()) Then
        'Trennung durchführen
        If vDuct.SplitAsInnerDuctInSegment(vSegment, New Point(closestPoint.X, closestPoint.Y), AddFittingMode.All, Nothing, otherduct, False) Then

        End If
      End If
    End Sub

    Private Function ValidateDistanceBetweenTwoPointsForSegment(connection As TBConnection, closestPoint As Point2D, splitPoint As Point2D) As Boolean
      Return Me.ValidateDistanceBetweenTwoPoints(connection, closestPoint, splitPoint)
    End Function

    Private Sub SplitDuctWithGeometry(vDuct As Duct, Optional splitPoint As Point = Nothing, Optional addFittingAfterSplit As AddFittingMode = AddFittingMode.None)
      'Abfrage, ob das Rohr vor der Trennung zu Trassen zugeordnet ist
      Dim hasSegmentAssignment As Boolean = False
      If vDuct.HasSegmentAssignment Then
        hasSegmentAssignment = True
      End If

      If splitPoint Is Nothing Then Return

      'Berechnung des nächstgelegenen Punktes
      Dim closestPoint = Me.GetClosestPoint(DirectCast(vDuct.Geometry, LineString), splitPoint)

      If Me.ValidateDistanceBetweenTwoPointsForDuct(vDuct.Connection, closestPoint, splitPoint.ToPoint2D()) Then
        vDuct.SplitAsDuctWithGeometry(New Point(closestPoint.X, closestPoint.Y), addFittingAfterSplit)
      End If
    End Sub

    Private Function ValidateDistanceBetweenTwoPointsForDuct(connection As TBConnection, closestPoint As Point2D, splitPoint As Point2D) As Boolean
      Return Me.ValidateDistanceBetweenTwoPoints(connection, closestPoint, splitPoint)
    End Function
    Private Function ValidateDistanceBetweenTwoPoints(connection As TBConnection, closestPoint As Point2D, splitPoint As Point2D) As Boolean
      'Bestätigungsbox, wenn der gewählte Punkt mehr als 10 Meter von der Trasse entfernt
      Dim factor As Double = TC.SP.Tools.Map.GetMeasuringUnitFactor(connection)
      If closestPoint.DistanceTo(splitPoint) * factor > 10 Then
        Return False
      End If
      Return True
    End Function

    Private Function GetClosestPoint(line As LineString, point As Point) As Point2D
      Return line.ToLine2D().CalculateClosestPoint(point.ToPoint2D())
    End Function

    Private Function GetMatchingDuctBundlesFromSegments(segments As IEnumerable(Of SurveyPlan.Segment), ductBundleName As String) As IEnumerable(Of Duct)
      Dim result = New List(Of Duct)
      If String.IsNullOrEmpty(ductBundleName) Then Return result

      For Each segment In segments
        For Each duct In segment.GetDirectDucts
          If Not String.IsNullOrWhiteSpace(duct.Name) AndAlso duct.Name.Contains(ductBundleName) Then
            result.Add(duct)
          End If
        Next
      Next

      Return result
    End Function

    Private Async Function TryGetSegmentsAtPoint(cache As ExportCache, intersectionPoint As LinePoint) As Task(Of IEnumerable(Of SurveyPlan.Segment))
      Dim result = New List(Of SurveyPlan.Segment)
      If intersectionPoint Is Nothing Then Return result
      'Immer wieder neu holen weil wir zwischendurch ja neue Segments beim Teilen erstellen
      Return Await cache.Db.ExecuteOnDatabase(Function() SurveyPlan.Segment.Get(cache.Db.Connection, New Spatial_Mask.Struct With {.AnyInteract = True}, intersectionPoint), CancellationToken.None)
    End Function

    Private Async Function TryMatchBuildingsByName(cache As ExportCache, buildingName As String) As Task(Of IEnumerable(Of Building))
      If String.IsNullOrWhiteSpace(buildingName) Then Return Enumerable.Empty(Of Building)
      Dim result = New List(Of Building)
      Dim buildingsByFid = Await Building.GetCache(cache)
      Dim streetsByFid = Await Street.GetCache(cache)
      Dim postcodesByFid = Await Postcode.GetCache(cache)
      Dim citiesByFid = Await City.GetCache(cache)
      Dim cleanedBuildingName As String = buildingName.ToUpperInvariant.Replace(" "c, "")

      For Each building In buildingsByFid.Values
        If Not building.IsConnected Then Continue For
        Dim postCodeString = If(postcodesByFid.ContainsKey(building.PostcodeFID.GetValueOrDefault(-1)), postcodesByFid(building.PostcodeFID.Value).Postcode, String.Empty)
        Dim streetString = If(streetsByFid.ContainsKey(building.StreetFID.GetValueOrDefault(-1)), streetsByFid(building.StreetFID.Value).Name, String.Empty)
        Dim cityString As String = String.Empty
        'If Not String.IsNullOrEmpty(postCodeString) AndAlso citiesByFid.ContainsKey(postcodesByFid(building.PostcodeFID.Value).CityFID.GetValueOrDefault(-1)) Then
        '  cityString = citiesByFid(postcodesByFid(building.PostcodeFID.Value).CityFID.GetValueOrDefault(-1)).Name
        'End If
        Dim matchingString = ($"{streetString} {building.Housenumber} {building.Suffix}, {postCodeString} {cityString}").ToUpperInvariant.Replace(" "c, "")
        If String.Equals(cleanedBuildingName, matchingString) Then
          result.Add(building)
          Continue For
        End If

        If Not String.IsNullOrEmpty(streetString) AndAlso citiesByFid.ContainsKey(streetsByFid(building.StreetFID.Value).CityFID.GetValueOrDefault(-1)) Then
          cityString = citiesByFid(streetsByFid(building.StreetFID.Value).CityFID.GetValueOrDefault(-1)).Name
        End If
        matchingString = ($"{streetString} {building.Housenumber} {building.Suffix}, {postCodeString} {cityString}").ToUpperInvariant.Replace(" "c, "")
        If String.Equals(cleanedBuildingName, matchingString) Then
          result.Add(building)
        End If
      Next
      Return result
    End Function

  End Class

End Namespace
