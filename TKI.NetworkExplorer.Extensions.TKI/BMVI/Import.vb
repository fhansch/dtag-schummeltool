Imports System.Threading
Imports Autodesk.Map.IM.CoordinateSystem.API
Imports Autodesk.Map.IM.CoordinateSystem.Factory
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Graphic
Imports TKI.Tools
Imports TKI.Tools.Database

Namespace BMVI
  Public Class Import
    Friend Property Transformation As Autodesk.Map.IM.CoordinateSystem.API.ITransformation
    Friend Property Db As DatabaseWorker
    Friend Property Token As CancellationToken

    Private _segmentsByStart As New Dictionary(Of Point, IList(Of TC.Common.API.SurveyPlan.Segment))
    Private _segmentsByEnd As New Dictionary(Of Point, IList(Of TC.Common.API.SurveyPlan.Segment))
    Private _pipesBySegment As New Dictionary(Of TC.Common.API.SurveyPlan.Segment, IList(Of TC.Common.API.DuctInfrastructure.Duct))
    Private _segmentsByPipe As New Dictionary(Of TC.Common.API.DuctInfrastructure.Duct, IList(Of TC.Common.API.SurveyPlan.Segment))

    Public Sub New(db As DatabaseWorker, token As CancellationToken)
      Transformation = CreateTransformation(db, token)
      _Db = db
      _Token = token

      Dim segments = db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Segment.Get(c), token).Result
      For Each segment In segments
        Dim geom = TryCast(segment.Geometry, LineString)
        If segment.Start Is Nothing OrElse segment.End Is Nothing OrElse geom Is Nothing Then Continue For
        If Not _segmentsByStart.ContainsKey(geom.StartPoint) Then _segmentsByStart(geom.StartPoint) = New List(Of TC.Common.API.SurveyPlan.Segment)
        _segmentsByStart(geom.StartPoint).Add(segment)
        If Not _segmentsByEnd.ContainsKey(geom.EndPoint) Then _segmentsByEnd(geom.EndPoint) = New List(Of TC.Common.API.SurveyPlan.Segment)
        _segmentsByEnd(geom.EndPoint).Add(segment)
      Next
    End Sub
    Private Function CreateTransformation(db As DatabaseWorker, token As CancellationToken) As ITransformation
      Return db.ExecuteOnDatabase(
            Function()
              Try
                Dim csFactory = New CoordinateSystemFactory()
                Dim sourceCoordinateSystem As ICoordinateSystem
                Using coordinateSystems = csFactory.CoordinateSystems
                  sourceCoordinateSystem = coordinateSystems.CreateFromWKT("GEOGCS[""ETRS89"",DATUM[""D_ETRS_1989"",SPHEROID[""GRS_1980"",6378137,298.257222101]],PRIMEM[""Greenwich"",0],UNIT[""Degree"",0.017453292519943295]]")
                  Dim targetCoordinateSystem = coordinateSystems.CreateFromWKT(db.Connection.DefaultCoordinateSystem.Wkt)
                  If Not sourceCoordinateSystem.Equals(targetCoordinateSystem) Then
                    Using transformations = csFactory.Transformations
                      Return transformations.Create(sourceCoordinateSystem, targetCoordinateSystem)
                    End Using
                  End If
                End Using
              Catch ignore As Exception
              End Try
              Return Nothing
            End Function, token).Result
      Return Nothing
    End Function
    Public Async Function PerformImport(directory As String) As Task
      Dim persons = (Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.Person.Get(c), Token)).ToList
      Dim createdTrenches = Await CreateSegments(directory, persons)
      Dim createdDucts = Await CreateDucts(directory, persons, createdTrenches)
      Dim createdStructures = Await CreateStructures(directory, persons)
      Dim createdBuildings = Await CreateBuildings(directory)
      Dim createdDevices = Await CreateDevices(directory, persons, createdStructures)
      Dim createdCables = Await CreateCables(directory, persons, createdTrenches, createdDucts, createdDevices)
    End Function

    Private Async Function CreateBuildings(directory As String) As Task(Of Dictionary(Of Data.Consumer, TC.Common.API.Topography.Building))
      Dim createdBuildings = New Dictionary(Of BMVI.Data.Consumer, TC.Common.API.Topography.Building)

      Dim fileName = IO.Directory.GetFiles(directory, "*Endverbraucher*.shp").FirstOrDefault
      Dim consumers = TKI.Tools.Data.Shapefile.DeserializeShapefile(Of BMVI.Data.Consumer)(fileName)
      For Each consumer In consumers
        Dim existingBuildings = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.Topography.Building.Get(c, "INFO", {consumer.MpDbId & "." & consumer.MpId}), Token)
        Dim building = existingBuildings.FirstOrDefault
        If building Is Nothing Then
          building = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.Topography.Building.CreateNew(c), Token)
        End If
        createdBuildings(consumer) = building
        building.Info = consumer.MpDbId & "." & consumer.MpId
        SetNullableLong(consumer.ID, building.Feature, "EXPORT_ID")
        Select Case consumer.Endkunde.GetValueOrDefault(1)
          Case 1
            building.NumberOfHouseholds = 1
          Case 2
            building.NumberOfBusinesses = 1
          Case 3
            SetNullableLong(1, building.Feature, "NUMBER_HOSPITALS")
          Case 4
            SetNullableLong(1, building.Feature, "NUMBER_SCHOOLS")
          Case 5
            SetNullableLong(1, building.Feature, "NUMBER_PUBLICS")
        End Select
        SetNullableDouble(consumer.Daempfung, building.Feature, "BMVI_LOSS")


        If existingBuildings.Any Then
          Await Db.ExecuteOnDatabase(Function(c) building.Update(), Token)
        Else
          Try
            building.Geometry = Transformation.Transform(consumer.Geometry.FirstOrDefault)
          Catch ex As Exception
          End Try
          Await Db.ExecuteOnDatabase(Function(c) building.Insert(), Token)
        End If
      Next

      Return createdBuildings
    End Function

    Private Async Function CreateCables(directory As String, persons As List(Of TC.Common.API.Person), createdTrenches As Dictionary(Of Data.Trench, TC.Common.API.SurveyPlan.Segment), createdDucts As Dictionary(Of Data.Duct, TC.Common.API.DuctInfrastructure.Duct), createdDevices As Dictionary(Of Data.NetworkTechnology, TC.FO.API.FiberOpticDevice)) As Task(Of Dictionary(Of Data.Connection, TC.FO.API.FiberOpticCable))
      Dim createdCables = New Dictionary(Of BMVI.Data.Connection, TC.FO.API.FiberOpticCable)
      Dim planeTolerance = Autodesk.Map.IM.PlaneGeometry.Tolerance.CreateFromSDOTolerance(Db.Connection.DefaultSpatialTolerance)

      Dim trenchesById = createdTrenches.Keys.ToLookup(Function(t) t.ID)
      Dim ductsById = createdDucts.Keys.ToLookup(Function(t) t.ID)
      Dim devicesById = createdDevices.Keys.ToLookup(Function(t) t.ID)
      Dim fileName = IO.Directory.GetFiles(directory, "*Verbindungen*.shp").FirstOrDefault
      Dim connections = TKI.Tools.Data.Shapefile.DeserializeShapefile(Of BMVI.Data.Connection)(fileName)
      Dim personsByCompany = persons.ToLookup(Function(c) c.Company)
      Dim personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
      Dim cableModels = Await Db.ExecuteOnDatabase(Function(c) TC.FO.API.FiberOpticCableModel.GetActiveModels(c), Token)
      Dim cableModelsByTypeName = New Dictionary(Of String, TC.FO.API.FiberOpticCableModel)
      For Each cableModel In cableModels
        If Not String.IsNullOrWhiteSpace(cableModel.Name) Then cableModelsByTypeName(cableModel.Name) = cableModel
      Next
      Dim microductsByBundle = New Dictionary(Of TC.Common.API.DuctInfrastructure.Duct, Queue(Of TC.Common.API.DuctInfrastructure.Duct))
      For Each bundle In createdDucts.Values.Distinct
        If Not Await Db.ExecuteOnDatabase(Function(c) bundle.HasInnerDucts, Token) Then Continue For
        Dim innerDucts = Await Db.ExecuteOnDatabase(Function(c) bundle.GetDirectInnerDucts, Token)
        microductsByBundle(bundle) = New Queue(Of TC.Common.API.DuctInfrastructure.Duct)(innerDucts.Where(Function(d) d.GetDuctCableCount = 0))
      Next

      For Each connectionGrouping In connections.GroupBy(Function(d) d.MpId.GetValueOrDefault(d.GetHashCode))
        Dim referenceConnection = connectionGrouping.First
        If (referenceConnection.VerbArt = 1 OrElse referenceConnection.VerbArt = 3) AndAlso referenceConnection.Zustand.GetValueOrDefault(0) = 1 Then
          Continue For 'Vorhandene Kupferkabel - werden ignoriert!
        End If
        Dim existingCables = Await Db.ExecuteOnDatabase(Function(c) TC.FO.API.FiberOpticCable.Get(c, "INFO", {referenceConnection.MpDbId & "." & referenceConnection.MpId}), Token)
        Dim cable = existingCables.FirstOrDefault
        If cable Is Nothing Then
          cable = Await Db.ExecuteOnDatabase(Function(c) TC.FO.API.FiberOpticCable.CreateNew(c), Token)
        End If

        cable.Info = referenceConnection.MpDbId & "." & referenceConnection.MpId
        If Not existingCables.Any OrElse referenceConnection.Zustand.GetValueOrDefault(0) <> 1 Then
          SetNullableLong(referenceConnection.Zustand, cable.AttrFeature, "ID_STATE")
        End If
        Dim typeName = referenceConnection.VaSonst '.Split("/"c).FirstOrDefault
        If String.IsNullOrWhiteSpace(typeName) Then
          Select Case referenceConnection.VerbArt
            Case 1
              typeName = $"Kupferkabel {referenceConnection.AnzahlFA.GetValueOrDefault(1)} Adern"
            Case 3
              typeName = $"Koaxialkabel {referenceConnection.AnzahlFA.GetValueOrDefault(1)} Adern"
            Case Else
              typeName = $"Glasfaserkabel {referenceConnection.AnzahlFA.GetValueOrDefault(1)} Fasern"
          End Select
        End If
        If Not String.IsNullOrWhiteSpace(typeName) Then
          Dim cableModel As TC.FO.API.FiberOpticCableModel = Nothing
          If Not cableModelsByTypeName.TryGetValue(typeName, cableModel) Then
            cableModel = Await Db.ExecuteOnDatabase(Function(c) TC.FO.API.FiberOpticCableModel.CreateNew(c), Token)
            cableModel.Name = typeName
            cableModel.FiberCount = referenceConnection.AnzahlFA.GetValueOrDefault(1)
            cableModelsByTypeName(typeName) = cableModel
            Await Db.ExecuteOnDatabase(Function(c) cableModel.Insert(), Token)
          End If
          cable.ModelFID = cableModel.FID
        End If
        If Not existingCables.Any Then
          If referenceConnection.IdStart.HasValue AndAlso devicesById(referenceConnection.IdStart.Value).Any Then
            Dim networkTech = devicesById(referenceConnection.IdStart.Value).First
            Dim device = createdDevices(networkTech)
            device.AddOutgoingCable(cable)
          End If
          If referenceConnection.IdEnde.HasValue AndAlso devicesById(referenceConnection.IdEnde.Value).Any Then
            Dim networkTech = devicesById(referenceConnection.IdEnde.Value).First
            Dim device = createdDevices(networkTech)
            device.AddIncomingCable(cable)
          End If
        End If

        If Not String.IsNullOrWhiteSpace(referenceConnection.EFName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByCompany(referenceConnection.EFName).Any Then
            person = personsByCompany(referenceConnection.EFName).First
          Else
            person = Await CreateNewPerson(referenceConnection.EFName, String.Empty, String.Empty, referenceConnection.ETel, referenceConnection.EEMail, referenceConnection.EOrt)
            persons.Add(person)
            personsByCompany = persons.ToLookup(Function(c) c.Company)
          End If
          SetNullableLong(person.FID, cable.AttrFeature, "FID_OWNER")
        End If
        If Not String.IsNullOrWhiteSpace(referenceConnection.AVName & referenceConnection.AZName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByFirstLastName(referenceConnection.AVName & referenceConnection.AZName).Any Then
            person = personsByFirstLastName(referenceConnection.AVName & referenceConnection.AZName).First
          Else
            person = Await CreateNewPerson(String.Empty, referenceConnection.AVName, referenceConnection.AZName, referenceConnection.ATel, referenceConnection.AEMail, String.Empty)
            persons.Add(person)
            personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
          End If
          SetNullableLong(person.FID, cable.AttrFeature, "FID_CONTACT")
        End If

        If existingCables.Any Then
          For Each connectionSection In connectionGrouping
            createdCables(connectionSection) = cable
          Next
          Await Db.ExecuteOnDatabase(Function(c) cable.Update(), Token)
        Else
          Await Db.ExecuteOnDatabase(Function(c) cable.Insert(), Token)

          Dim cableSegments = New HashSet(Of TC.Common.API.SurveyPlan.Segment)
          Dim cableDucts = New HashSet(Of TC.Common.API.DuctInfrastructure.Duct)
          For Each cableSection In connectionGrouping
            If cableSection.IdRohr.GetValueOrDefault(0) <> 0 Then
              Dim duct As TC.Common.API.DuctInfrastructure.Duct = Nothing
              If ductsById(cableSection.IdRohr.GetValueOrDefault(-1)).Any AndAlso createdDucts.ContainsKey(ductsById(cableSection.IdRohr.GetValueOrDefault(-1)).FirstOrDefault) Then
                duct = createdDucts(ductsById(cableSection.IdRohr.GetValueOrDefault(-1)).FirstOrDefault)
              End If
              cableDucts.Add(duct)
            Else
              Dim segment As TC.Common.API.SurveyPlan.Segment = Nothing
              Dim geom As LineString = Nothing
              Try
                geom = Transformation.Transform(cableSection.Geometry).ToLine2D.ToLineString(planeTolerance)
              Catch ex As Exception
              End Try
              If trenchesById(cableSection.IdTrassen.GetValueOrDefault(-1)).Any AndAlso createdTrenches.ContainsKey(trenchesById(cableSection.IdTrassen.GetValueOrDefault(-1)).FirstOrDefault) Then
                segment = createdTrenches(trenchesById(cableSection.IdTrassen.GetValueOrDefault(-1)).FirstOrDefault)
              ElseIf geom IsNot Nothing AndAlso _segmentsByStart.ContainsKey(geom.StartPoint) AndAlso _segmentsByEnd.ContainsKey(geom.EndPoint) AndAlso _segmentsByStart(geom.StartPoint).Intersect(_segmentsByEnd(geom.EndPoint)).Any Then
                'Zu bestehendem Segment hinzufügen
                segment = _segmentsByStart(geom.StartPoint).Intersect(_segmentsByEnd(geom.EndPoint)).First
              Else
                segment = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Segment.CreateNew(c), Token)
                segment.Geometry = geom
                Await Db.ExecuteOnDatabase(Function(c) segment.Insert(), Token)
                If segment.Start IsNot Nothing AndAlso segment.End IsNot Nothing Then
                  If Not _segmentsByStart.ContainsKey(geom.StartPoint) Then _segmentsByStart(geom.StartPoint) = New List(Of TC.Common.API.SurveyPlan.Segment)
                  _segmentsByStart(geom.StartPoint).Add(segment)
                  If Not _segmentsByEnd.ContainsKey(geom.EndPoint) Then _segmentsByEnd(geom.EndPoint) = New List(Of TC.Common.API.SurveyPlan.Segment)
                  _segmentsByEnd(geom.EndPoint).Add(segment)
                End If
              End If
              cableSegments.Add(segment)
            End If
            createdCables(cableSection) = cable
          Next

          Dim segmentsAndPipes = GetPotentialPipes(cableSegments.ToList, _pipesBySegment, _segmentsByPipe)
          Dim newSegmentCables = segmentsAndPipes.ResultSegments
          Dim newDuctCables = segmentsAndPipes.ResultDucts.Concat(cableDucts).Distinct.ToList
          For Each pipe In newDuctCables.ToList
            If microductsByBundle.ContainsKey(pipe) AndAlso microductsByBundle(pipe).Any Then
              newDuctCables.Remove(pipe)
              newDuctCables.Add(microductsByBundle(pipe).Dequeue)
            End If
          Next
          newDuctCables = newDuctCables.Distinct.ToList
          For Each pipe In newDuctCables
            Dim ductCable = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.DuctInfrastructure.DuctCable.CreateNew(c), Token)
            ductCable.SetDuct(pipe)
            ductCable.SetCable(cable)
            'SetNullableLong(ductsection.ID, segmentDuct.Feature, "EXPORT_ID")
            Await Db.ExecuteOnDatabase(Function(c) ductCable.Insert(), Token)
          Next
          For Each segment In newSegmentCables
            Dim segmentCable = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.SegmentCable.CreateNew(c), Token)
            segmentCable.SetSegment(segment)
            segmentCable.SetCable(cable)
            'SetNullableLong(ductsection.ID, segmentDuct.Feature, "EXPORT_ID")
            Await Db.ExecuteOnDatabase(Function(c) segmentCable.Insert(), Token)
          Next
        End If
      Next

      Return createdCables
    End Function

    Private Async Function CreateDevices(directory As String, persons As List(Of TC.Common.API.Person), createdStructures As Dictionary(Of Data.Structure, TC.Common.API.SurveyPlan.StructuralNode)) As Task(Of Dictionary(Of Data.NetworkTechnology, TC.FO.API.FiberOpticDevice))
      Dim createdDevices = New Dictionary(Of BMVI.Data.NetworkTechnology, TC.FO.API.FiberOpticDevice)

      Dim fileName = IO.Directory.GetFiles(directory, "*Netztechnik*.shp").FirstOrDefault
      Dim networkTechnology = TKI.Tools.Data.Shapefile.DeserializeShapefile(Of BMVI.Data.NetworkTechnology)(fileName)
      Dim personsByCompany = persons.ToLookup(Function(c) c.Company)
      Dim personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
      Dim structuresById = createdStructures.Keys.ToLookup(Function(s) s.ID)
      For Each networkTech In networkTechnology
        Dim existingClosures = Await Db.ExecuteOnDatabase(Function(c) TC.FO.API.FiberOpticClosure.Get(c, "INFO", {networkTech.MpDbId & "." & networkTech.MpId}), Token)
        Dim existingTerminators = Await Db.ExecuteOnDatabase(Function(c) TC.FO.API.FiberOpticTerminator.Get(c, "INFO", {networkTech.MpDbId & "." & networkTech.MpId}), Token)
        Dim existingDevices = Enumerable.Empty(Of TC.FO.API.FiberOpticDevice).Concat(existingClosures).Concat(existingTerminators)
        Dim device = existingDevices.FirstOrDefault
        If device Is Nothing Then
          Select Case networkTech.Art_NT
            Case 12, 13, 1, 9, 4, 3
              device = Await Db.ExecuteOnDatabase(Function(c) TC.FO.API.FiberOpticTerminator.CreateNew(c), Token)
            Case Else
              device = Await Db.ExecuteOnDatabase(Function(c) TC.FO.API.FiberOpticClosure.CreateNew(c), Token)
          End Select
        End If
        createdDevices(networkTech) = device
        device.Info = networkTech.MpDbId & "." & networkTech.MpId
        SetNullableLong(networkTech.ID, device.AttrFeature, "EXPORT_ID")
        SetNullableLong(networkTech.Art_NT, device.AttrFeature, "ID_NETWORK_TECH")
        SetString(networkTech.NT_sonst, device.AttrFeature, "ALTERNATE_NETWORK_TECH")
        SetNullableLong(networkTech.TeArtVor, device.AttrFeature, "ID_EXISTING_TECH_TYPE")
        SetString(networkTech.TeVSonst, device.AttrFeature, "ALTERNATE_EXISTING_TECH_TYPE")
        SetNullableLong(networkTech.TeArtGep, device.AttrFeature, "ID_FUTURE_TECH_TYPE")
        SetString(networkTech.TeGSonst, device.AttrFeature, "ALTERNATE_FUTURE_TECH_TYPE")
        If networkTech.IdBau.HasValue AndAlso structuresById(networkTech.IdBau.Value).Any Then
          Dim struct = structuresById(networkTech.IdBau.Value).First
          Dim [structure] = createdStructures(struct)
          device.SetStructuralPoint([structure])
        End If
        If Not existingDevices.Any OrElse networkTech.Zustand.GetValueOrDefault(0) <> 1 Then
          SetNullableLong(networkTech.Zustand, device.AttrFeature, "ID_STATE")
        End If
        device.Name = networkTech.Bezeichner
        SetNullableLong(networkTech.MontZ, device.AttrFeature, "MONTAGE_HEIGHT")

        If Not String.IsNullOrWhiteSpace(networkTech.EFName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByCompany(networkTech.EFName).Any Then
            person = personsByCompany(networkTech.EFName).First
          Else
            person = Await CreateNewPerson(networkTech.EFName, String.Empty, String.Empty, networkTech.ETel, networkTech.EEMail, networkTech.EOrt)
            persons.Add(person)
            personsByCompany = persons.ToLookup(Function(c) c.Company)
          End If
          SetNullableLong(person.FID, device.AttrFeature, "FID_OWNER")
        End If
        If Not String.IsNullOrWhiteSpace(networkTech.AVName & networkTech.AZName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByFirstLastName(networkTech.AVName & networkTech.AZName).Any Then
            person = personsByFirstLastName(networkTech.AVName & networkTech.AZName).First
          Else
            person = Await CreateNewPerson(String.Empty, networkTech.AVName, networkTech.AZName, networkTech.ATel, networkTech.AEMail, String.Empty)
            persons.Add(person)
            personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
          End If
          SetNullableLong(person.FID, device.AttrFeature, "FID_CONTACT")
        End If

        If existingDevices.Any Then
          Await Db.ExecuteOnDatabase(Function(c) device.Update(), Token)
        Else
          Try
            device.Geometry = Transformation.Transform(networkTech.Geometry.FirstOrDefault)
          Catch ex As Exception
          End Try
          Await Db.ExecuteOnDatabase(Function(c) device.Insert(), Token)
        End If
      Next

      Return createdDevices
    End Function

    Private Async Function CreateStructures(directory As String, persons As IList(Of TC.Common.API.Person)) As Task(Of Dictionary(Of Data.Structure, TC.Common.API.SurveyPlan.StructuralNode))
      Dim createdStructures = New Dictionary(Of BMVI.Data.Structure, TC.Common.API.SurveyPlan.StructuralNode)

      Dim fileName = IO.Directory.GetFiles(directory, "*Bauten*.shp").FirstOrDefault
      Dim structures = TKI.Tools.Data.Shapefile.DeserializeShapefile(Of BMVI.Data.Structure)(fileName)
      Dim personsByCompany = persons.ToLookup(Function(c) c.Company)
      Dim personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
      For Each struct In structures
        Dim existingManholes = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Manhole.Get(c, "INFO", {struct.MpDbId & "." & struct.MpId}), Token)
        Dim existingCabinets = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Cabinet.Get(c, "INFO", {struct.MpDbId & "." & struct.MpId}), Token)
        Dim existingPoles = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Pole.Get(c, "INFO", {struct.MpDbId & "." & struct.MpId}), Token)
        Dim existingStructures = Enumerable.Empty(Of TC.Common.API.SurveyPlan.StructuralNode).Concat(existingCabinets).Concat(existingManholes).Concat(existingPoles)
        Dim [structure] = existingStructures.FirstOrDefault
        If [structure] Is Nothing Then
          Select Case struct.Art_Bau
            Case 5, 6, 7, 8, 9
              [structure] = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Pole.CreateNew(c), Token)
            Case 4
              [structure] = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Manhole.CreateNew(c), Token)
            Case Else
              [structure] = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Cabinet.CreateNew(c), Token)
          End Select
        End If
        createdStructures(struct) = [structure]
        [structure].Info = struct.MpDbId & "." & struct.MpId
        SetNullableLong(struct.ID, [structure].AttrFeature, "EXPORT_ID")
        SetNullableLong(struct.Art_Bau, [structure].AttrFeature, "ID_STRUCTURE_TYPE")
        SetString(struct.Bau_sonst, [structure].AttrFeature, "ALTERNATE_STRUCTURE_TYPE")
        [structure].Name = struct.Bezeichner
        If Not existingStructures.Any OrElse struct.Zustand.GetValueOrDefault(0) <> 1 Then
          SetNullableLong(struct.Zustand, [structure].AttrFeature, "ID_STATE")
        End If

        If Not String.IsNullOrWhiteSpace(struct.EFName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByCompany(struct.EFName).Any Then
            person = personsByCompany(struct.EFName).First
          Else
            person = Await CreateNewPerson(struct.EFName, String.Empty, String.Empty, struct.ETel, struct.EEMail, struct.EOrt)
            persons.Add(person)
            personsByCompany = persons.ToLookup(Function(c) c.Company)
          End If
          SetNullableLong(person.FID, [structure].AttrFeature, "FID_OWNER")
        End If
        If Not String.IsNullOrWhiteSpace(struct.AVName & struct.AZName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByFirstLastName(struct.AVName & struct.AZName).Any Then
            person = personsByFirstLastName(struct.AVName & struct.AZName).First
          Else
            person = Await CreateNewPerson(String.Empty, struct.AVName, struct.AZName, struct.ATel, struct.AEMail, String.Empty)
            persons.Add(person)
            personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
          End If
          SetNullableLong(person.FID, [structure].AttrFeature, "FID_CONTACT")
        End If

        If existingStructures.Any Then
          Await Db.ExecuteOnDatabase(Function(c) [structure].Update(), Token)
        Else
          Try
            [structure].Geometry = Transformation.Transform(struct.Geometry.FirstOrDefault)
          Catch ex As Exception
          End Try
          Await Db.ExecuteOnDatabase(Function(c) [structure].Insert(), Token)
        End If
      Next

      Return createdStructures
    End Function

    Private Async Function CreateDucts(directory As String, persons As IList(Of TC.Common.API.Person), createdTrenches As Dictionary(Of Data.Trench, TC.Common.API.SurveyPlan.Segment)) As Task(Of Dictionary(Of Data.Duct, TC.Common.API.DuctInfrastructure.Duct))
      Dim createdDucts = New Dictionary(Of BMVI.Data.Duct, TC.Common.API.DuctInfrastructure.Duct)
      Dim planeTolerance = Autodesk.Map.IM.PlaneGeometry.Tolerance.CreateFromSDOTolerance(Db.Connection.DefaultSpatialTolerance)

      Dim trenchesById = createdTrenches.Keys.ToLookup(Function(t) t.ID)
      Dim fileName = IO.Directory.GetFiles(directory, "*Leerrohre*.shp").FirstOrDefault
      Dim ducts = TKI.Tools.Data.Shapefile.DeserializeShapefile(Of BMVI.Data.Duct)(fileName)
      Dim personsByCompany = persons.ToLookup(Function(c) c.Company)
      Dim personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
      Dim ductModels = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.DuctInfrastructure.DuctType.GetActiveModels(c), Token)
      Dim ductModelsByTypeName = New Dictionary(Of String, TC.Common.API.DuctInfrastructure.DuctType)
      For Each ductModel In ductModels
        If Not String.IsNullOrWhiteSpace(ductModel.Name) Then ductModelsByTypeName(ductModel.Name) = ductModel
      Next
      For Each segment In createdTrenches.Values.Distinct
        Dim geom = TryCast(segment.Geometry, LineString)
        If segment.Start Is Nothing OrElse segment.End Is Nothing OrElse geom Is Nothing Then Continue For
        If Not _segmentsByStart.ContainsKey(geom.StartPoint) Then _segmentsByStart(geom.StartPoint) = New List(Of TC.Common.API.SurveyPlan.Segment)
        _segmentsByStart(geom.StartPoint).Add(segment)
        If Not _segmentsByEnd.ContainsKey(geom.EndPoint) Then _segmentsByEnd(geom.EndPoint) = New List(Of TC.Common.API.SurveyPlan.Segment)
        _segmentsByEnd(geom.EndPoint).Add(segment)
      Next

      For Each ductGrouping In ducts.OrderBy(Function(d) d.LRArt).GroupBy(Function(d) d.MpId.GetValueOrDefault(d.GetHashCode))
        Dim referenceDuct = ductGrouping.First
        Dim existingDucts = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.DuctInfrastructure.Duct.Get(c, "INFO", {referenceDuct.MpDbId & "." & referenceDuct.MpId}), Token)
        Dim duct = existingDucts.FirstOrDefault
        If duct Is Nothing Then
          duct = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.DuctInfrastructure.Duct.CreateNew(c), Token)
        End If

        duct.Info = referenceDuct.MpDbId & "." & referenceDuct.MpId
        If Not existingDucts.Any OrElse referenceDuct.Zustand.GetValueOrDefault(0) <> 1 Then
          SetNullableLong(referenceDuct.Zustand, duct.AttrFeature, "ID_STATE")
        End If
        Dim typeName = referenceDuct.LRSonst '.Split("/"c).FirstOrDefault
        If Not String.IsNullOrWhiteSpace(typeName) Then
          Dim ductModel As TC.Common.API.DuctInfrastructure.DuctType = Nothing
          If Not ductModelsByTypeName.TryGetValue(typeName, ductModel) Then
            ductModel = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.DuctInfrastructure.DuctType.CreateNew(c), Token)
            ductModel.Name = typeName
            Dim lrSonst = referenceDuct.LRSonst
            If lrSonst?.Length > 50 Then
              lrSonst = lrSonst.Substring(0, 50)
            End If
            SetString(lrSonst, ductModel.Feature, "BMVI_DUCT_TYPE_OTHER")
            Dim internalDuctType = 99
              Select Case referenceDuct.LRArt.GetValueOrDefault(0)
                Case 2
                  internalDuctType = 11
                Case 3
                  internalDuctType = 12
                Case 4
                  internalDuctType = 13
                Case 5
                  internalDuctType = 15
                Case 6
                  internalDuctType = 14
              End Select
              SetNullableInteger(internalDuctType, ductModel.Feature, "ID_BMVI_DUCT_TYPE")
              ductModelsByTypeName(typeName) = ductModel
              Await Db.ExecuteOnDatabase(Function(c) ductModel.Insert(), Token)
            End If
            duct.ModelFID = ductModel.FID
        End If
        If Not String.IsNullOrWhiteSpace(referenceDuct.EFName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByCompany(referenceDuct.EFName).Any Then
            person = personsByCompany(referenceDuct.EFName).First
          Else
            person = Await CreateNewPerson(referenceDuct.EFName, String.Empty, String.Empty, referenceDuct.ETel, referenceDuct.EEMail, referenceDuct.EOrt)
            persons.Add(person)
            personsByCompany = persons.ToLookup(Function(c) c.Company)
          End If
          SetNullableLong(person.FID, duct.AttrFeature, "FID_OWNER")
        End If
        If Not String.IsNullOrWhiteSpace(referenceDuct.AVName & referenceDuct.AZName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByFirstLastName(referenceDuct.AVName & referenceDuct.AZName).Any Then
            person = personsByFirstLastName(referenceDuct.AVName & referenceDuct.AZName).First
          Else
            person = Await CreateNewPerson(String.Empty, referenceDuct.AVName, referenceDuct.AZName, referenceDuct.ATel, referenceDuct.AEMail, String.Empty)
            persons.Add(person)
            personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
          End If
          SetNullableLong(person.FID, duct.AttrFeature, "FID_CONTACT")
        End If

        If existingDucts.Any Then
          For Each ductSection In ductGrouping
            createdDucts(ductSection) = duct
          Next
          Await Db.ExecuteOnDatabase(Function(c) duct.Update(), Token)
          If referenceDuct.LRArt = 1 Then
            Dim ductSegments = New List(Of TC.Common.API.SurveyPlan.Segment)(Await Db.ExecuteOnDatabase(Function(c) duct.GetAllDirectSegments, Token))
            For Each segment In ductSegments
              If Not _pipesBySegment.ContainsKey(segment) Then _pipesBySegment(segment) = New List(Of TC.Common.API.DuctInfrastructure.Duct)
              _pipesBySegment(segment).Add(duct)
              If Not _segmentsByPipe.ContainsKey(duct) Then _segmentsByPipe(duct) = New List(Of TC.Common.API.SurveyPlan.Segment)
              _segmentsByPipe(duct).Add(segment)
            Next
          End If
          If referenceDuct.Zustand <> 1 Then
            Dim segmentDucts = Await Db.ExecuteOnDatabase(Function(c) duct.GetSegmentDucts, Token)
            Dim ductSegments = New List(Of TC.Common.API.SurveyPlan.Segment)(Await Db.ExecuteOnDatabase(Function(c) duct.GetAllDirectSegments, Token))
            Await AssignDuctsToSegmentsOrPipes(referenceDuct, duct, ductSegments)
            For Each ductSegment In segmentDucts
              Await Db.ExecuteOnDatabase(Function(c) ductSegment.Delete(), Token)
            Next
          End If
        Else
          Await Db.ExecuteOnDatabase(Function(c) duct.Insert(), Token)
          Dim ductSegments = New List(Of TC.Common.API.SurveyPlan.Segment)
          For Each ductsection In ductGrouping
            Dim segment As TC.Common.API.SurveyPlan.Segment = Nothing
            Dim geom As LineString = Nothing
            Try
              geom = Transformation.Transform(ductsection.Geometry).ToLine2D.ToLineString(planeTolerance)
            Catch ex As Exception
            End Try
            If trenchesById(ductsection.IdTrassen.GetValueOrDefault(-1)).Any AndAlso createdTrenches.ContainsKey(trenchesById(ductsection.IdTrassen.GetValueOrDefault(-1)).FirstOrDefault) Then
              segment = createdTrenches(trenchesById(ductsection.IdTrassen.GetValueOrDefault(-1)).FirstOrDefault)
            ElseIf geom IsNot Nothing AndAlso _segmentsByStart.ContainsKey(geom.StartPoint) AndAlso _segmentsByEnd.ContainsKey(geom.EndPoint) AndAlso _segmentsByStart(geom.StartPoint).Intersect(_segmentsByEnd(geom.EndPoint)).Any Then
              'Zu bestehendem Segment hinzufügen
              segment = _segmentsByStart(geom.StartPoint).Intersect(_segmentsByEnd(geom.EndPoint)).First
            Else
              segment = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Segment.CreateNew(c), Token)
              segment.Geometry = geom
              SetNullableLong(ductsection.VerlegeTiefe, segment.AttrFeature, "INSTALLATION_DEPTH")
              Await Db.ExecuteOnDatabase(Function(c) segment.Insert(), Token)

              If segment.Start IsNot Nothing AndAlso segment.End IsNot Nothing Then
                If Not _segmentsByStart.ContainsKey(geom.StartPoint) Then _segmentsByStart(geom.StartPoint) = New List(Of TC.Common.API.SurveyPlan.Segment)
                _segmentsByStart(geom.StartPoint).Add(segment)
                If Not _segmentsByEnd.ContainsKey(geom.EndPoint) Then _segmentsByEnd(geom.EndPoint) = New List(Of TC.Common.API.SurveyPlan.Segment)
                _segmentsByEnd(geom.EndPoint).Add(segment)
              End If
            End If
            createdDucts(ductsection) = duct
            ductSegments.Add(segment)
          Next

          Await AssignDuctsToSegmentsOrPipes(referenceDuct, duct, ductSegments)
        End If
      Next
      Return createdDucts
    End Function

    Private Async Function AssignDuctsToSegmentsOrPipes(referenceDuct As Data.Duct, duct As TC.Common.API.DuctInfrastructure.Duct, ductSegments As List(Of TC.Common.API.SurveyPlan.Segment)) As Task
      Dim segmentsAndPipes = GetPotentialPipes(ductSegments, _pipesBySegment, _segmentsByPipe)
      Dim newSegmentDucts = If(referenceDuct.LRArt <> 1 AndAlso referenceDuct.Zustand <> 1, segmentsAndPipes.ResultSegments, ductSegments)
      Dim newDuctDucts = If(referenceDuct.LRArt <> 1 AndAlso referenceDuct.Zustand <> 1, segmentsAndPipes.ResultDucts, New List(Of TC.Common.API.DuctInfrastructure.Duct))
      For Each pipe In newDuctDucts
        Dim ductDuct = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.DuctInfrastructure.DuctDuct.CreateNew(c), Token)
        ductDuct.SetOuterDuct(pipe)
        ductDuct.SetInnerDuct(duct)
        'SetNullableLong(ductsection.ID, segmentDuct.Feature, "EXPORT_ID")
        Await Db.ExecuteOnDatabase(Function(c) ductDuct.Insert(), Token)
      Next
      For Each segment In newSegmentDucts
        Dim segmentDuct = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.SegmentDuct.CreateNew(c), Token)
        segmentDuct.SetSegment(segment)
        segmentDuct.SetDuct(duct)
        'SetNullableLong(ductsection.ID, segmentDuct.Feature, "EXPORT_ID")
        Await Db.ExecuteOnDatabase(Function(c) segmentDuct.Insert(), Token)
        If referenceDuct.LRArt = 1 Then
          If Not _pipesBySegment.ContainsKey(segment) Then _pipesBySegment(segment) = New List(Of TC.Common.API.DuctInfrastructure.Duct)
          _pipesBySegment(segment).Add(duct)
          If Not _segmentsByPipe.ContainsKey(duct) Then _segmentsByPipe(duct) = New List(Of TC.Common.API.SurveyPlan.Segment)
          _segmentsByPipe(duct).Add(segment)
        End If
      Next
    End Function

    Private Async Function CreateNewPerson(company As String, vname As String, name As String, eTel As String, eeMail As String, eOrt As String) As Task(Of TC.Common.API.Person)
      Dim person = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.Person.CreateNew(c), Token)
      person.Company = company
      person.FirstName = vname
      person.Name = name
      person.PhoneOffice = eTel
      SetString(eeMail, person.Feature, "E_MAIL")
      If Not String.IsNullOrWhiteSpace(eOrt) Then
        Dim city = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.Topography.City.Get(c, TC.Common.API.Topography.City.ATTR_NAME, {eOrt}).FirstOrDefault, Token)
        If city Is Nothing Then
          city = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.Topography.City.CreateNew(c), Token)
          city.Name = eOrt
          Await Db.ExecuteOnDatabase(Function(c) city.Insert(), Token)
        End If
        Dim street = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.Topography.Street.CreateNew(c), Token)
        street.Name = "Dummy"
        street.SetCity(city)
        Await Db.ExecuteOnDatabase(Function(c) street.Insert(), Token)
        Dim building = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.Topography.Building.CreateNew(c), Token)
        building.SetStreet(street)
        Await Db.ExecuteOnDatabase(Function(c) building.Insert(), Token)
        person.SetBuilding(building)
      End If
      Await Db.ExecuteOnDatabase(Function(c) person.Insert(), Token)
      Return person
    End Function

    Private Function GetPotentialPipes(ductSegments As List(Of TC.Common.API.SurveyPlan.Segment),
                                       pipesBySegment As Dictionary(Of TC.Common.API.SurveyPlan.Segment, IList(Of TC.Common.API.DuctInfrastructure.Duct)),
                                       segmentsByPipe As Dictionary(Of TC.Common.API.DuctInfrastructure.Duct, IList(Of TC.Common.API.SurveyPlan.Segment))) As (ResultSegments As IList(Of TC.Common.API.SurveyPlan.Segment), ResultDucts As IList(Of TC.Common.API.DuctInfrastructure.Duct))
      Dim resultSegments = New List(Of TC.Common.API.SurveyPlan.Segment)(ductSegments)
      Dim resultPipes = New List(Of TC.Common.API.DuctInfrastructure.Duct)

      For Each segment In ductSegments
        If Not resultSegments.Contains(segment) Then Continue For
        If Not pipesBySegment.ContainsKey(segment) Then Continue For
        For Each pipe In pipesBySegment(segment).OrderByDescending(Function(p) p.Length.GetValueOrDefault(0))
          If segmentsByPipe(pipe).Intersect(resultSegments).Count = segmentsByPipe(pipe).Count Then
            resultPipes.Add(pipe)
            resultSegments = resultSegments.Except(segmentsByPipe(pipe)).ToList
            Exit For
          End If
        Next
      Next

      Return (resultSegments, resultPipes)
    End Function

    Private Async Function CreateSegments(directory As String, persons As IList(Of TC.Common.API.Person)) As Task(Of Dictionary(Of Data.Trench, TC.Common.API.SurveyPlan.Segment))
      Dim createdTrenches = New Dictionary(Of BMVI.Data.Trench, TC.Common.API.SurveyPlan.Segment)
      Dim planeTolerance = Autodesk.Map.IM.PlaneGeometry.Tolerance.CreateFromSDOTolerance(Db.Connection.DefaultSpatialTolerance)

      Dim fileName = IO.Directory.GetFiles(directory, "*Trassenbau*.shp").FirstOrDefault
      Dim trenches = TKI.Tools.Data.Shapefile.DeserializeShapefile(Of BMVI.Data.Trench)(fileName)
      Dim personsByCompany = persons.ToLookup(Function(c) c.Company)
      Dim personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
      For Each trench In trenches
        Dim geom As LineString = Nothing
        Try
          geom = Transformation.Transform(trench.Geometry).ToLine2D.ToLineString(planeTolerance)
        Catch ex As Exception
        End Try
        Dim existingSegments = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Segment.Get(c, "INFO", {trench.MpDbId & "." & trench.MpId}), Token)
        If Not existingSegments.Any AndAlso geom IsNot Nothing AndAlso _segmentsByStart.ContainsKey(geom.StartPoint) AndAlso _segmentsByEnd.ContainsKey(geom.EndPoint) AndAlso _segmentsByStart(geom.StartPoint).Intersect(_segmentsByEnd(geom.EndPoint)).Any Then
          'Zu bestehendem Segment hinzufügen
          existingSegments = _segmentsByStart(geom.StartPoint).Intersect(_segmentsByEnd(geom.EndPoint)).ToList
        End If
        Dim segment = existingSegments.FirstOrDefault

        If segment Is Nothing Then
          segment = Await Db.ExecuteOnDatabase(Function(c) TC.Common.API.SurveyPlan.Segment.CreateNew(c), Token)
        End If
        createdTrenches(trench) = segment
        segment.Info = trench.MpDbId & "." & trench.MpId
        SetNullableLong(trench.ID, segment.AttrFeature, "EXPORT_ID")
        SetBoolean(trench.Trassenb.GetValueOrDefault(0) <> 12, segment.AttrFeature, "IS_SEALED")
        SetNullableLong(trench.Verfahren, segment.AttrFeature, "ID_TRENCHING_METHOD")
        SetString(trench.VerfSonst, segment.AttrFeature, "ALTERNATE_TRENCHING_METHOD")
        SetBoolean(trench.Mitverleg.GetValueOrDefault(0) = 1, segment.AttrFeature, "IS_SYNERGY")
        SetNullableLong(trench.VerlegeTiefe, segment.AttrFeature, "INSTALLATION_DEPTH")
        If Not String.IsNullOrWhiteSpace(trench.EFName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByCompany(trench.EFName).Any Then
            person = personsByCompany(trench.EFName).First
          Else
            person = Await CreateNewPerson(trench.EFName, String.Empty, String.Empty, trench.ETel, trench.EEMail, trench.EOrt)
            persons.Add(person)
            personsByCompany = persons.ToLookup(Function(c) c.Company)
          End If
          SetNullableLong(person.FID, segment.AttrFeature, "FID_OWNER")
        End If
        If Not String.IsNullOrWhiteSpace(trench.AVName & trench.AZName) Then
          Dim person As TC.Common.API.Person = Nothing
          If personsByFirstLastName(trench.AVName & trench.AZName).Any Then
            person = personsByFirstLastName(trench.AVName & trench.AZName).First
          Else
            person = Await CreateNewPerson(String.Empty, trench.AVName, trench.AZName, trench.ATel, trench.AEMail, String.Empty)
            persons.Add(person)
            personsByFirstLastName = persons.ToLookup(Function(c) c.FirstName & c.LastName)
          End If
          SetNullableLong(person.FID, segment.AttrFeature, "FID_CONTACT")
        End If

        If existingSegments.Any Then
          Await Db.ExecuteOnDatabase(Function(c) segment.Update(), Token)
        Else
          segment.Geometry = geom
          Await Db.ExecuteOnDatabase(Function(c) segment.Insert(), Token)
        End If
      Next

      Return createdTrenches
    End Function
  End Class

End Namespace
