Imports Autodesk.Map.IM.Graphic
Imports TKI.Tools.Data

Namespace BMVI.Data
  '<Shapefile("Verbindungen.shp")>
  <GeoJsonFile("Verbindungen.geojson")>
  Public NotInheritable Class Connection
    <ShapefileColumn("ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ID As Integer

    <ShapefileColumn("VERB_ART", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("verb_art")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property VerbArt As Integer? = Nothing

    <ShapefileColumn("V_A_SONST")>
    <GeoJsonFileColumn("v_a_sonst")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property VaSonst As String = Nothing

    <ShapefileColumn("ANZAHL_VER", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anzahl_ver")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnzahlVer As Integer? = Nothing

    <ShapefileColumn("LAE_KABEL", FieldLength:=10, DecimalPlaces:=8, [Optional]:=True)>
    <GeoJsonFileColumn("lae_kabel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property LaeKabel As Double? = Nothing

    <ShapefileColumn("ANZAHL_F_A", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anzahl_f_a")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnzahlFA As Integer? = Nothing

    <ShapefileColumn("LFD_M_F_A", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("lfd_m_f_a")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property LaufendeMeterFA As Integer?

    <ShapefileColumn("F_A_RESERV", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("f_a_reserv")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property FAReserve As Integer? = Nothing

    <ShapefileColumn("ZUSTAND", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("zustand")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Zustand As Integer? = Nothing

    <ShapefileColumn("ID_TRASSEN", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_trassen")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property IdTrassen As Integer? = Nothing

    <ShapefileColumn("ID_LR", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_lr")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property IdRohr As Integer? = Nothing

    <ShapefileColumn("ID_START", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_start")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property IdStart As Integer? = 0

    <ShapefileColumn("ID_ENDE", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_ende")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property IdEnde As Integer? = 0

    <ShapefileColumn("E_FNAME")>
    <GeoJsonFileColumn("e_fname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EFName As String = Nothing

    <ShapefileColumn("E_ORT")>
    <GeoJsonFileColumn("e_ort")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EOrt As String = Nothing

    <ShapefileColumn("E_TEL")>
    <GeoJsonFileColumn("e_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ETel As String

    <ShapefileColumn("E_E_MAIL")>
    <GeoJsonFileColumn("e_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EEMail As String = Nothing

    <ShapefileColumn("A_VNAME")>
    <GeoJsonFileColumn("a_vname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AVName As String = Nothing

    <ShapefileColumn("A_ZNAME")>
    <GeoJsonFileColumn("a_zname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AZName As String = Nothing

    <ShapefileColumn("A_TEL")>
    <GeoJsonFileColumn("a_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ATel As String = Nothing

    <ShapefileColumn("A_E_MAIL")>
    <GeoJsonFileColumn("a_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AEMail As String = Nothing

    <ShapefileColumn("MP_DB_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_db_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpDbId As Integer? = Nothing

    <ShapefileColumn("MP_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpId As Integer? = Nothing

    <ShapefileColumn("MIT_CAB_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mit_cab_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MitCabId As Integer? = Nothing

    ''' <summary>
    ''' The geometry of the line.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Code is called by the serializer.")>
    <Map3d.ShapefileMap3dGeometry>
    <GeoJsonFileGeometry>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification:="The geometry has to be set.")>
    Public Property Geometry As LineString
  End Class

End Namespace
