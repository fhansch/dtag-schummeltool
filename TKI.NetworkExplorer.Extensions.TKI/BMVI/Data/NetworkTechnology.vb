Imports Autodesk.Map.IM.Graphic
Imports TKI.Tools.Data

Namespace BMVI.Data
  '<Shapefile("Netztechnik.shp")>
  <GeoJsonFile("Netztechnik.geojson")>
  Public NotInheritable Class NetworkTechnology
    <ShapefileColumn("ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ID As Integer

    <ShapefileColumn("ART_NT", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("art_nt")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Art_NT As Integer? = Nothing

    <ShapefileColumn("NT_SONST")>
    <GeoJsonFileColumn("nt_sonst")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property NT_sonst As String = Nothing

    <ShapefileColumn("TE_ART_VOR", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("te_art_vor")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property TeArtVor As Integer

    <ShapefileColumn("TE_V_SONST")>
    <GeoJsonFileColumn("te_v_sonst")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property TeVSonst As String = Nothing

    <ShapefileColumn("TE_ART_GEP", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("te_art_gep")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property TeArtGep As Integer

    <ShapefileColumn("TE_G_SONST")>
    <GeoJsonFileColumn("te_g_sonst")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property TeGSonst As String = Nothing

    <ShapefileColumn("BEZEICHNER")>
    <GeoJsonFileColumn("bezeichner")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Bezeichner As String = Nothing

    <ShapefileColumn("ID_BAU", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_bau")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property IdBau As Integer? = Nothing

    <ShapefileColumn("ID_TECH", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_tech")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property IdTech As Integer? = Nothing

    <ShapefileColumn("ZUSTAND", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("zustand")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Zustand As Integer? = Nothing

    <ShapefileColumn("MONT_Z", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mont_z")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MontZ As Integer? = Nothing

    <ShapefileColumn("KD_ANB_V", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("kd_anb_v")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property KdAnbV As Integer? = Nothing

    <ShapefileColumn("ANZ_ANS_V", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anz_ans_v")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnzAnsV As Integer? = Nothing

    <ShapefileColumn("KD_ANB_G", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("kd_anb_g")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property KdAnbG As Integer? = Nothing

    <ShapefileColumn("ANZ_ANS_G", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anz_ans_g")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnzAnsG As Integer? = Nothing

    <ShapefileColumn("SENDE_GEOM", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("sende_geom")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property SendeGeom As Integer? = Nothing

    <ShapefileColumn("SENDE_BB", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("sende_bb")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property SendeBB As Integer? = Nothing

    <ShapefileColumn("SI_ABST_XY", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("si_abst_xy")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property SiAbstXY As Integer? = Nothing

    <ShapefileColumn("SI_ABST_Z", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("si_abst_z")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property SiAbstZ As Integer? = Nothing

    <ShapefileColumn("E_FNAME")>
    <GeoJsonFileColumn("e_fname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EFName As String = Nothing

    <ShapefileColumn("E_ORT")>
    <GeoJsonFileColumn("e_ort")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EOrt As String = Nothing

    <ShapefileColumn("E_TEL")>
    <GeoJsonFileColumn("e_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ETel As String = Nothing

    <ShapefileColumn("E_E_MAIL")>
    <GeoJsonFileColumn("e_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EEMail As String = Nothing

    <ShapefileColumn("A_VNAME")>
    <GeoJsonFileColumn("a_vname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AVName As String = Nothing

    <ShapefileColumn("A_ZNAME")>
    <GeoJsonFileColumn("a_zname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AZName As String = Nothing

    <ShapefileColumn("A_TEL")>
    <GeoJsonFileColumn("a_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ATel As String = Nothing

    <ShapefileColumn("A_E_MAIL")>
    <GeoJsonFileColumn("a_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AEMail As String = Nothing

    <ShapefileColumn("MP_DB_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_db_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpDbId As Integer? = Nothing

    <ShapefileColumn("MP_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpId As Integer? = Nothing

    ''' <summary>
    ''' The geometry of the point.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Code is called by the serializer.")>
    <Map3d.ShapefileMap3dGeometry>
    <GeoJsonFileGeometry>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification:="The geometry has to be set.")>
    Public Property Geometry As MultiPoint
  End Class
End Namespace
