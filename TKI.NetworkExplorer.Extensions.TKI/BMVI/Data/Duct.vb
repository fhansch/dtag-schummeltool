Imports Autodesk.Map.IM.Graphic
Imports TKI.Tools.Data

Namespace BMVI.Data
  '<Shapefile("Leerrohre.shp")>
  <GeoJsonFile("Leerrohre.geojson")>
  Public NotInheritable Class Duct
    Implements IEquatable(Of Duct)
    <ShapefileColumn("ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ID As Integer

    <ShapefileColumn("LR_ART", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("lr_art")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property LRArt As Integer? = Nothing

    <ShapefileColumn("LR_SONST")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property LRSonst As String = Nothing

    <ShapefileColumn("ANZAHL", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anzahl")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Anzahl As Integer? = Nothing

    <ShapefileColumn("LR_RESERV", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("lr_reserv")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property LRReserve As Integer? = Nothing

    <ShapefileColumn("LAE_LR", FieldLength:=10, DecimalPlaces:=8, [Optional]:=True)>
    <GeoJsonFileColumn("lae_lr")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property LaeLr As Double? = Nothing

    <ShapefileColumn("ZUSTAND", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("zustand")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Zustand As Integer? = Nothing

    <ShapefileColumn("VERL_TIEFE", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("verl_tiefe")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property VerlegeTiefe As Integer? = Nothing

    <ShapefileColumn("ID_TRASSEN", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_trassen")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property IdTrassen As Integer? = Nothing

    <ShapefileColumn("E_FNAME")>
    <GeoJsonFileColumn("e_fname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EFName As String = Nothing

    <ShapefileColumn("E_ORT")>
    <GeoJsonFileColumn("e_ort")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EOrt As String = Nothing

    <ShapefileColumn("E_TEL")>
    <GeoJsonFileColumn("e_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ETel As String = Nothing

    <ShapefileColumn("E_E_MAIL")>
    <GeoJsonFileColumn("e_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EEMail As String = Nothing

    <ShapefileColumn("A_VNAME")>
    <GeoJsonFileColumn("a_vname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AVName As String = Nothing

    <ShapefileColumn("A_ZNAME")>
    <GeoJsonFileColumn("a_zname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AZName As String = Nothing

    <ShapefileColumn("A_TEL")>
    <GeoJsonFileColumn("a_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ATel As String = Nothing

    <ShapefileColumn("A_E_MAIL")>
    <GeoJsonFileColumn("a_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AEMail As String = Nothing

    <ShapefileColumn("MP_DB_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_db_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpDbId As Integer? = Nothing

    <ShapefileColumn("MP_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpId As Integer? = Nothing

    <ShapefileColumn("SECTION_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("section_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property SectionId As Integer? = Nothing

    ''' <summary>
    ''' The geometry of the line.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Code is called by the serializer.")>
    <Map3d.ShapefileMap3dGeometry>
    <GeoJsonFileGeometry>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification:="The geometry has to be set.")>
    Public Property Geometry As LineString


    Public Overloads Function Equals(other As Duct) As Boolean Implements IEquatable(Of Duct).Equals
      If other Is Nothing Then Return False

      If Me.IdTrassen Is Nothing Then
        If other.IdTrassen IsNot Nothing Then Return False
      Else
        If Not Me.IdTrassen.Equals(other.IdTrassen) Then Return False
      End If
      If Me.LRArt Is Nothing Then
        If other.LRArt IsNot Nothing Then Return False
      Else
        If Not Me.LRArt.Equals(other.LRArt) Then Return False
      End If
      If Me.LRSonst Is Nothing Then
        If other.LRSonst IsNot Nothing Then Return False
      Else
        If Not Me.LRSonst.Equals(other.LRSonst) Then Return False
      End If
      If Me.Zustand Is Nothing Then
        If other.Zustand IsNot Nothing Then Return False
      Else
        If Not Me.Zustand.Equals(other.Zustand) Then Return False
      End If
      If Me.Geometry Is Nothing Then
        If other.Geometry IsNot Nothing Then Return False
      Else
        If Not Me.Geometry.Equals(other.Geometry) Then Return False
      End If

      Return True
    End Function

    Public Overrides Function Equals(obj As Object) As Boolean
      Return Me.Equals(TryCast(obj, Duct))
    End Function

    Public Overrides Function GetHashCode() As Integer
      Return MyBase.GetHashCode()
    End Function
  End Class
End Namespace
