Imports Autodesk.Map.IM.Graphic
Imports TKI.Tools.Data

Namespace BMVI.Data
  '<Shapefile("Trassenbau.shp")>
  <GeoJsonFile("Trassenbau.geojson")>
  Public NotInheritable Class Trench
    <ShapefileColumn("ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ID As Integer

    <ShapefileColumn("ID_SUMME", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_summe")>
    Public Property IDSumme As Integer

    <ShapefileColumn("TRASSENB", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("trassenb")>
    Public Property Trassenb As Integer? = Nothing

    <ShapefileColumn("VERFAHREN", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("verfahren")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Verfahren As Integer? = Nothing

    <ShapefileColumn("VERF_SONST")>
    <GeoJsonFileColumn("verf_sonst")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property VerfSonst As String = Nothing

    <ShapefileColumn("MITVERLEG", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mitverleg")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Mitverleg As Integer? = Nothing

    <ShapefileColumn("MITV_EIGEN")>
    <GeoJsonFileColumn("mitv_eigen")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MitverlegtEigen As String = Nothing

    <ShapefileColumn("VERL_TIEFE", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("verl_tiefe")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property VerlegeTiefe As Integer? = Nothing

    <ShapefileColumn("E_FNAME")>
    <GeoJsonFileColumn("e_fname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EFName As String = Nothing

    <ShapefileColumn("E_ORT")>
    <GeoJsonFileColumn("e_ort")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EOrt As String = Nothing

    <ShapefileColumn("E_TEL")>
    <GeoJsonFileColumn("e_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ETel As String = Nothing

    <ShapefileColumn("E_E_MAIL")>
    <GeoJsonFileColumn("e_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EEMail As String = Nothing

    <ShapefileColumn("A_VNAME")>
    <GeoJsonFileColumn("a_vname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AVName As String = Nothing

    <ShapefileColumn("A_ZNAME")>
    <GeoJsonFileColumn("a_zname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AZName As String = Nothing

    <ShapefileColumn("A_TEL")>
    <GeoJsonFileColumn("a_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ATel As String = Nothing

    <ShapefileColumn("A_E_MAIL")>
    <GeoJsonFileColumn("a_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AEMail As String = Nothing

    <ShapefileColumn("MP_DB_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_db_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpDbId As Integer? = Nothing

    <ShapefileColumn("MP_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpId As Integer? = Nothing

    ''' <summary>
    ''' The geometry of the line.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Code is called by the serializer.")>
    <Map3d.ShapefileMap3dGeometry>
    <GeoJsonFileGeometry>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification:="The geometry has to be set.")>
    Public Property Geometry As LineString
  End Class
End Namespace
