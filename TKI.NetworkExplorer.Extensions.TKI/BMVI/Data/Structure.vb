Imports Autodesk.Map.IM.Graphic
Imports TKI.Tools.Data

Namespace BMVI.Data
  '<Shapefile("Bauten.shp")>
  <GeoJsonFile("Bauten.geojson")>
  Public NotInheritable Class [Structure]
    <ShapefileColumn("ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ID As Integer

    <ShapefileColumn("ART_BAU", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("art_bau")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Art_Bau As Integer? = Nothing

    <ShapefileColumn("BAU_SONST")>
    <GeoJsonFileColumn("bau_sonst")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Bau_sonst As String = Nothing

    <ShapefileColumn("BEZEICHNER")>
    <GeoJsonFileColumn("bezeichner")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Bezeichner As String = Nothing

    <ShapefileColumn("ZUSTAND", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("zustand")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Zustand As Integer? = Nothing


    <ShapefileColumn("DIMENSION")>
    <GeoJsonFileColumn("dimension")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Dimension As String = Nothing

    <ShapefileColumn("LAGE", FieldLength:=10)>
    <GeoJsonFileColumn("lage")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Lage As Integer? = Nothing

    <ShapefileColumn("E_FNAME")>
    <GeoJsonFileColumn("e_fname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EFName As String = Nothing

    <ShapefileColumn("E_ORT")>
    <GeoJsonFileColumn("e_ort")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EOrt As String = Nothing

    <ShapefileColumn("E_TEL")>
    <GeoJsonFileColumn("e_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ETel As String = Nothing

    <ShapefileColumn("E_E_MAIL")>
    <GeoJsonFileColumn("e_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property EEMail As String = Nothing

    <ShapefileColumn("A_VNAME")>
    <GeoJsonFileColumn("a_vname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AVName As String = Nothing

    <ShapefileColumn("A_ZNAME")>
    <GeoJsonFileColumn("a_zname")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AZName As String = Nothing

    <ShapefileColumn("A_TEL")>
    <GeoJsonFileColumn("a_tel")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ATel As String = Nothing

    <ShapefileColumn("A_E_MAIL")>
    <GeoJsonFileColumn("a_e_mail")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AEMail As String = Nothing

    <ShapefileColumn("MP_DB_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_db_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpDbId As Integer? = Nothing

    <ShapefileColumn("MP_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpId As Integer? = Nothing

    ''' <summary>
    ''' The geometry of the point.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Code is called by the serializer.")>
    <Map3d.ShapefileMap3dGeometry>
    <GeoJsonFileGeometry>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification:="The geometry has to be set.")>
    Public Property Geometry As MultiPoint
  End Class
End Namespace
