Imports Autodesk.Map.IM.Graphic
Imports TKI.Tools.Data

Namespace BMVI.Data
  '<Shapefile("Endverbraucher.shp")>
  <GeoJsonFile("Endverbraucher.geojson")>
  Public NotInheritable Class Consumer
    <ShapefileColumn("ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property ID As Integer

    <ShapefileColumn("AN_HAU_AUS", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("an_hau_aus")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnHauAus As Integer? = Nothing

    <ShapefileColumn("ENDKUNDE", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("endkunde")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Endkunde As Integer? = Nothing

    <ShapefileColumn("ID_NETZE", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("id_netze")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property IDNetzTe As Integer? = Nothing

    <ShapefileColumn("ANB_MIN_DO", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anb_min_do")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnbMinDo As Integer? = Nothing

    <ShapefileColumn("ANB_MIN_UP", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anb_min_up")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnbMinUp As Integer? = Nothing

    <ShapefileColumn("ANB_MAX_DO", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anb_max_do")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnbMaxDo As Integer? = Nothing

    <ShapefileColumn("ANB_MAX_UP", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("anb_max_up")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property AnbMaxUp As Integer? = Nothing

    <ShapefileColumn("DAEMPFUNG", FieldLength:=10, DecimalPlaces:=8, [Optional]:=True)>
    <GeoJsonFileColumn("daempfung")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property Daempfung As Double? = Nothing

    <ShapefileColumn("MP_DB_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_db_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpDbId As Integer? = Nothing

    <ShapefileColumn("MP_ID", FieldLength:=10, [Optional]:=True)>
    <GeoJsonFileColumn("mp_id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Is present to reflect the structure of the BMVI output.")>
    Public Property MpId As Integer? = Nothing


    ''' <summary>
    ''' The geometry of the point.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="Code is called by the serializer.")>
    <Map3d.ShapefileMap3dGeometry>
    <GeoJsonFileGeometry>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification:="The geometry has to be set.")>
    Public Property Geometry As MultiPoint
  End Class
End Namespace
