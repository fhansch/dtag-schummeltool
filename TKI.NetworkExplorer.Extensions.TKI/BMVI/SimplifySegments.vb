Imports System.Threading
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.FO.API
Imports TC.SP.FeatureRulePlugIns
Imports TKI.Geometries.Tools
Imports TKI.NetworkExplorer.Caching
Imports TKI.Tools
Imports TKI.Tools.Database

Friend Module SimplifySegments
  Friend Sub SimplifySegmentsJob(status As IStatusDisplay, parameters() As Object)
    If parameters.Length <> 2 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
    If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))
    If TypeOf parameters(1) IsNot Document Then Throw New ArgumentException($"The parameter 2 has the wrong type. Expected: {NameOf(Document)}", NameOf(parameters))

    Dim vCluster = DirectCast(parameters(0), Cluster)
    Dim vDocument = DirectCast(parameters(1), Document)

    Task.WaitAll(RunSimplifySegmentsJob(status, vCluster, vDocument, CancellationToken.None))
  End Sub

  Private Async Function RunSimplifySegmentsJob(status As IStatusDisplay, vCluster As Cluster, document As Document, token As CancellationToken) As Task
    Using db As New DatabaseWorker(vCluster.Connection)
      Using burst As New RegenerateLabelDisableBurst(db.Connection)
        Using mapBurst = document.Map.InitiateDrawBurst()
          Using transaction = Await db.BeginTransaction(token)
            Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
            Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)

            status.Status = "Daten ermitteln..."
            Dim segments = Await Segment.GetCache(cache)
            Dim segmentDucts = Await cache.SegmentDuctsBySegmentFid
            Dim segmentCables = Await cache.SegmentCablesBySegmentFid
            Dim segmentsToDelete = New HashSet(Of Segment)

            For Each groupedBySealedAttribute In segments.Values.Distinct.GroupBy(Function(s) GetNullableBoolean(s.AttrFeature, "IS_SEALED"))
              For Each groupedByTrenchingAttribute In groupedBySealedAttribute.GroupBy(Function(s) GetNullableLong(s.AttrFeature, "ID_TRENCHING_METHOD"))
                For Each groupedBySynergyAttribute In groupedByTrenchingAttribute.GroupBy(Function(s) GetNullableBoolean(s.AttrFeature, "IS_SYNERGY"))
                  For Each groupedByDepthAttribute In groupedBySynergyAttribute.GroupBy(Function(s) GetNullableLong(s.AttrFeature, "INSTALLATION_DEPTH"))
                    For Each groupedByCableDuct In groupedByDepthAttribute.GroupBy(Function(s) New TrenchAssignments(segmentDucts(s.FID).Select(Function(sd) sd.DuctFID.GetValueOrDefault(-1)).ToArray, segmentCables(s.FID).Select(Function(sc) sc.CableFID.GetValueOrDefault(-1)).ToArray))
                      'Belegung ist gleich oder Segment ist leer!
                      Dim groupedByGeometry = GroupByMap3dGeometry(Of Segment)(groupedByCableDuct, Function(s) TryCast(s.Geometry, LineString), db.Connection.DefaultSpatialTolerance)
                      For Each segmentGroup In groupedByGeometry
                        If segmentGroup.Count = 1 Then Continue For
                        Dim newGeometry = CreateMap3dGeometry(segmentGroup.Select(Function(s) TryCast(s.Geometry, LineString)), db.Connection.DefaultSpatialTolerance)
                        segmentGroup(0).Geometry = newGeometry
                        segmentGroup(0).Update()
                        For i = 1 To segmentGroup.Count - 1
                          segmentsToDelete.Add(segmentGroup(i))
                        Next
                      Next
                    Next
                  Next
                Next
              Next
            Next

            Using ReentrantSwitches.Set(SegmentFeatureRulePlugin.SuspendDeleteMessage)
              Await DeleteUtilityFeatures(segmentsToDelete, cache.Db, token)
            End Using

            Await transaction.Commit(token)
          End Using
        End Using
      End Using
    End Using

  End Function

  Private NotInheritable Class TrenchAssignments
    Implements IEquatable(Of TrenchAssignments)

    Private ReadOnly _ducts As IReadOnlyCollection(Of Long)
    Private ReadOnly _cables As IReadOnlyCollection(Of Long)

    Public Sub New(ducts As IReadOnlyCollection(Of Long), cables As IReadOnlyCollection(Of Long))
      If ducts Is Nothing Then Throw New ArgumentNullException(NameOf(ducts))
      If cables Is Nothing Then Throw New ArgumentNullException(NameOf(cables))

      _ducts = ducts.OrderBy(Function(d) d).ToArray
      _cables = cables.OrderBy(Function(c) c).ToArray
    End Sub

    Public Overrides Function Equals(obj As Object) As Boolean
      Return Equals(TryCast(obj, TrenchAssignments))
    End Function

    Public Overloads Function Equals(other As TrenchAssignments) As Boolean Implements IEquatable(Of TrenchAssignments).Equals
      If other Is Nothing Then Return False
      If _ducts.Count <> other._ducts.Count OrElse _cables.Count <> other._cables.Count Then Return False

      Return _ducts.SequenceEqual(other._ducts) AndAlso
          _cables.SequenceEqual(other._cables)
    End Function

    Public Overrides Function GetHashCode() As Integer
      Return 1
    End Function
  End Class

  Private Async Function DeleteUtilityFeatures(features As IEnumerable(Of UtilityFeatureItem), db As DatabaseWorker, token As CancellationToken) As Task
    If Not features.Any Then Return
    Dim fc = features.First.FeatureClass
    Dim unprocessedFeatures = New HashSet(Of UtilityFeatureItem)(features)
    While unprocessedFeatures.Any
      Dim nextBatch = unprocessedFeatures.Take(If(unprocessedFeatures.Count > 500, 500, unprocessedFeatures.Count)).ToList
      For Each item In nextBatch
        unprocessedFeatures.Remove(item)
      Next
      Await db.ExecuteOnDatabase(Function() fc.DeleteFeatures(nextBatch.Select(Function(f) f.FID)), token)
    End While
  End Function
End Module
