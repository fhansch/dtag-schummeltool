Imports System.Threading
Imports Autodesk.AutoCAD.ApplicationServices
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports Autodesk.Map.IM.PlaneGeometry
Imports TC.Common.API
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.Tools
Imports TKI.Tools.Database
Imports TKI.UI
Imports TKI.UI.Configuration

Namespace BMVI
  Friend Class SplicingAssistent

    Private Property Document As Autodesk.Map.IM.Forms.Document
    Friend Sub New(doc As Autodesk.Map.IM.Forms.Document)
      _Document = doc
    End Sub

    Friend Sub RunAssistant(clusterFid As Long)
      Dim vCluster = Cluster.Get(Document.Connection, clusterFid)
      If vCluster Is Nothing Then Return

      Dim clusterName = vCluster.Name
      clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

      Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf CreateSplicingJob)
      statusForm.AbortThreadOnCancel = True
      statusForm.CancelButtonVisible = True
      statusForm.CancelButtonEnabled = True
      statusForm.Parameters = {vCluster}
      statusForm.Status = ""
      statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
      statusForm.Text = "Magie wird gewirkt"
      statusForm.ShowDialog()
      '  End If
    End Sub

    Private Sub CreateSplicingJob(status As IStatusDisplay, parameters() As Object)
      If parameters.Length <> 1 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
      If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))

      Dim vCluster = DirectCast(parameters(0), Cluster)

      RunSplicingJob(status, vCluster, CancellationToken.None).Wait()
    End Sub

    Private Async Function RunSplicingJob(status As IStatusDisplay, vCluster As Cluster, token As CancellationToken) As Task
      Using db As New DatabaseWorker(vCluster.Connection)
        Using burst As New RegenerateLabelDisableBurst(db.Connection)
          Using mapBurst = Document.Map.InitiateDrawBurst()
            Using transaction = Await db.BeginTransaction(token)
              Await ExecuteInternal(status, db, vCluster, token)
              Await transaction.Commit(token)
            End Using
          End Using
        End Using
      End Using
    End Function

    Private Async Function ExecuteInternal(status As IStatusDisplay, db As DatabaseWorker, vCluster As Cluster, token As CancellationToken) As Task
      Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
      Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)

      Dim terminators = Await FiberOpticTerminator.GetCache(cache)
      Dim closures = Await FiberOpticClosure.GetCache(cache)
      Dim cables = Await FiberOpticCable.GetCache(cache)
      Dim splitterModels = Await FiberOpticSplitterModel.GetCache(cache)
      Dim fibersByCableFid = Await cache.FibersByCableFid
      Dim allDevices = New HashSet(Of FiberOpticDevice)(Enumerable.Empty(Of FiberOpticDevice).Concat(terminators.Values).Concat(closures.Values))
      Dim outgoingCablesByDevice = allDevices.ToDictionary(Function(d) d, Function(d) d.GetOutgoingCables)
      Dim incomingCablesByDevice = allDevices.ToDictionary(Function(d) d, Function(d) d.GetIncomingCables)
      Dim terminatorsByCableFid = Await cache.FiberOpticTerminatorsByCableFid()
      Dim closuresByCableFid = Await cache.FiberOpticClosuresByCableFid()

      Dim splitters = Await FiberOpticSplitter.GetCache(cache)
      Dim splices = Await FiberOpticSplice.GetCache(cache)
      Dim connectors = Await FiberOpticConnector.GetCache(cache)
      Dim trays = Await FiberOpticTray.GetCache(cache)

      status.Status = "Daten ermitteln..."
      Dim deletefeatureQueue = New Queue(Of IEnumerable(Of FeatureItem))
      deletefeatureQueue.Enqueue((Await FiberOpticSplice.GetCache(cache)).Values)
      deletefeatureQueue.Enqueue((Await FiberOpticSplitter.GetCache(cache)).Values)
      deletefeatureQueue.Enqueue((Await FiberOpticSplitterPath.GetCache(cache)).Values)
      deletefeatureQueue.Enqueue((Await FiberOpticTray.GetCache(cache)).Values)
      deletefeatureQueue.Enqueue((Await FiberOpticConnector.GetCache(cache)).Values)

      While deletefeatureQueue.Any
        Dim nextFeatures = deletefeatureQueue.Dequeue
        If Not nextFeatures.Any Then Continue While
        status.Status = $"Lösche {nextFeatures.First.FeatureClass.Caption}..."
        If TypeOf nextFeatures.First Is UtilityFeatureItem Then
          Await DeleteUtilityFeatures(nextFeatures.OfType(Of UtilityFeatureItem), cache.Db, token)
        Else
          Await DeleteFeatures(nextFeatures, cache.Db, token)
        End If
      End While

      Dim sortedDevices = New List(Of FiberOpticDevice)
      Dim remainingDevices = New HashSet(Of FiberOpticDevice)(allDevices)
      Dim devicesToProcess = New Stack(Of FiberOpticDevice)(allDevices.Where(Function(d) Not outgoingCablesByDevice(d).Any))
      Dim countNotChanged = 0
      While devicesToProcess.Any
        Dim currentDevice = devicesToProcess.Pop
        If Not remainingDevices.Remove(currentDevice) Then
          If countNotChanged > 100 Then
            Continue While
          End If
          sortedDevices.Remove(currentDevice)
          countNotChanged += 1
        Else
          countNotChanged = 0
        End If
        sortedDevices.Add(currentDevice)
        For Each outCable In incomingCablesByDevice(currentDevice)
          For Each dev In terminatorsByCableFid(outCable.FID)
            If dev.Equals(currentDevice) Then Continue For
            devicesToProcess.Push(dev)
          Next
          For Each dev In closuresByCableFid(outCable.FID)
            If dev.Equals(currentDevice) Then Continue For
            devicesToProcess.Push(dev)
          Next
        Next
      End While

      Dim usedFibersByCable As New Dictionary(Of Long, IList(Of (Fiber As Fiber, IsP2P As Boolean)))
      Dim createdTrays = New List(Of FiberOpticTray)
      Dim createdSplices = New ArrayList
      Dim createdConnectors = New List(Of FiberOpticConnector)
      Dim createdSplitters = New List(Of FiberOpticSplitter)
      Dim createdSplitterPaths = New List(Of FiberOpticSplitterPath)
      Dim connectorFiberConnections As New List(Of FiberOpticConnector.FiberToConnectorSpliceStructure)
      For Each device In sortedDevices
        Dim splitterInPaths = New Queue(Of FiberOpticSplitterPath)
        Dim splitterOutPaths = New Queue(Of FiberOpticSplitterPath)
        Dim trayNumber = 1
        Dim connectorCount = 1
        Dim splitterModelFid = GetNullableLong(device.AttrFeature, "FID_SPLITTER_MODEL")
        Dim splitterModel = If(splitterModels.ContainsKey(splitterModelFid.GetValueOrDefault(0)), splitterModels(splitterModelFid.Value), Nothing)
        Dim ponFibers = GetNullableLong(device.AttrFeature, "PON_FIBERS").GetValueOrDefault(0)
        Dim p2pFibers = GetNullableLong(device.AttrFeature, "P2P_FIBERS").GetValueOrDefault(0)
        Dim incomingFibers As New Queue(Of Fiber)
        For Each inCable In incomingCablesByDevice(device)
          For Each fiber In fibersByCableFid(inCable.FID).OrderBy(Function(f) f.Number.GetValueOrDefault(0))
            incomingFibers.Enqueue(fiber)
          Next
        Next

        For Each outCable In outgoingCablesByDevice(device)
          If Not usedFibersByCable.ContainsKey(outCable.FIDGeom.Value) Then Continue For
          Dim currentTray = Await CreateTray(db, device, trayNumber, token)
          trayNumber += 1
          createdTrays.Add(currentTray)
          Dim remainingTraySpace = 12

          For Each usedFiber In usedFibersByCable(outCable.FIDGeom.Value)
            If remainingTraySpace = 0 Then
              currentTray = Await CreateTray(db, device, trayNumber, token)
              trayNumber += 1
              createdTrays.Add(currentTray)
              remainingTraySpace = 12
            End If
            remainingTraySpace -= 1

            If Not incomingFibers.Any Then
              If TypeOf device Is FiberOpticTerminator Then
                Dim connector As FiberOpticConnector = If(TypeOf device Is FiberOpticTerminator, Await CreateConnector(db, device, connectorCount, token), Nothing)
                connectorCount += 1
                createdConnectors.Add(connector)
                Dim connFiber As FiberOpticConnector.FiberToConnectorSpliceStructure
                connFiber.Connector = connector
                connFiber.ConnectedFeatureFIDAttr = usedFiber.Fiber.FIDAttr
                connFiber.PlaceNumber = 12 - remainingTraySpace
                connFiber.TrayFid = currentTray.FID
                connectorFiberConnections.Add(connFiber)
              Else
                Dim splice As FiberOpticSplice = Await CreateFiberToTraySplice(db, device, usedFiber.Fiber, currentTray, 12 - remainingTraySpace, Nothing, token)
                createdSplices.Add(splice)
              End If
              Continue For
            End If

            If usedFiber.IsP2P OrElse splitterModel Is Nothing Then
              Dim inFiber = incomingFibers.Dequeue()
              Dim splice As FiberOpticSplice = Await CreateFiberToFiberSplice(db, device, inFiber, usedFiber.Fiber, currentTray, 12 - remainingTraySpace, token)
              createdSplices.Add(splice)
              If Not usedFibersByCable.ContainsKey(inFiber.LineFID.Value) Then usedFibersByCable(inFiber.LineFID.Value) = New List(Of (Fiber, Boolean))
              usedFibersByCable(inFiber.LineFID.Value).Add((inFiber, usedFiber.IsP2P))
            Else
              If Not splitterOutPaths.Any Then
                Dim splitter = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplitter.CreateNew(c), token)
                splitter.PointFID = device.FIDGeom
                splitter.ModelFID = splitterModel.FID
                'splitter.Name = i.ToString
                createdSplitters.Add(splitter)

                Dim inPath = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplitterPath.CreateNew(c), token)
                inPath.Number = 1
                inPath.PointFID = splitter.FIDGeom
                inPath.ParentFID = device.FIDGeom
                inPath.TypeID = FiberOpticSplitterPathType.IN
                createdSplitterPaths.Add(inPath)
                splitterInPaths.Enqueue(inPath)

                For j = 1 To 32
                  Dim splitterpath = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplitterPath.CreateNew(c), token)
                  splitterpath.Number = j
                  splitterpath.PointFID = splitter.FIDGeom
                  splitterpath.ParentFID = device.FIDGeom
                  splitterpath.TypeID = FiberOpticSplitterPathType.OUT
                  createdSplitterPaths.Add(splitterpath)
                  splitterOutPaths.Enqueue(splitterpath)
                Next

                If remainingTraySpace = 0 Then
                  currentTray = Await CreateTray(db, device, trayNumber, token)
                  trayNumber += 1
                  createdTrays.Add(currentTray)
                  remainingTraySpace = 12
                End If
                remainingTraySpace -= 1

                Dim inFiber = incomingFibers.Dequeue()
                Dim inSplice As FiberOpticSplice = Await CreateFiberToSplitterPathSplice(db, device, inFiber, inPath, currentTray, 12 - remainingTraySpace, token)
                createdSplices.Add(inSplice)
                If Not usedFibersByCable.ContainsKey(inFiber.LineFID.Value) Then usedFibersByCable(inFiber.LineFID.Value) = New List(Of (Fiber, Boolean))
                usedFibersByCable(inFiber.LineFID.Value).Add((inFiber, True))
              End If

              Dim outSplice As FiberOpticSplice = Await CreateSplitterPathToFiberSplice(db, device, splitterOutPaths.Dequeue, usedFiber.Fiber, currentTray, 12 - remainingTraySpace, token)
              createdSplices.Add(outSplice)
            End If
          Next
        Next

        If p2pFibers > 0 Then
          Dim currentTray = Await CreateTray(db, device, trayNumber, token)
          trayNumber += 1
          createdTrays.Add(currentTray)
          Dim remainingTraySpace = 12

          For i = 1 To p2pFibers
            If remainingTraySpace = 0 Then
              currentTray = Await CreateTray(db, device, trayNumber, token)
              trayNumber += 1
              createdTrays.Add(currentTray)
              remainingTraySpace = 12
            End If
            remainingTraySpace -= 1

            If Not incomingFibers.Any Then Continue For
            Dim inFiber = incomingFibers.Dequeue()
            Dim connector As FiberOpticConnector = If(TypeOf device Is FiberOpticTerminator, Await CreateConnector(db, device, connectorCount, token), Nothing)
            If connector IsNot Nothing Then
              connectorCount += 1
              createdConnectors.Add(connector)
              Dim connFiber As FiberOpticConnector.FiberToConnectorSpliceStructure
              connFiber.Connector = connector
              connFiber.ConnectedFeatureFIDAttr = inFiber.FIDAttr
              connFiber.PlaceNumber = 12 - remainingTraySpace
              connFiber.TrayFid = currentTray.FID
              connectorFiberConnections.Add(connFiber)
            Else
              Dim splice As FiberOpticSplice = Await CreateFiberToTraySplice(db, device, inFiber, currentTray, 12 - remainingTraySpace, connector, token)
              createdSplices.Add(splice)
            End If
            If Not usedFibersByCable.ContainsKey(inFiber.LineFID.Value) Then usedFibersByCable(inFiber.LineFID.Value) = New List(Of (Fiber, Boolean))
            usedFibersByCable(inFiber.LineFID.Value).Add((inFiber, True))
          Next
        End If

        If ponFibers > 0 Then
          Dim currentTray = Await CreateTray(db, device, trayNumber, token)
          trayNumber += 1
          createdTrays.Add(currentTray)
          Dim remainingTraySpace = 12

          For i = 1 To ponFibers
            If splitterModel Is Nothing Then
              If remainingTraySpace = 0 Then
                currentTray = Await CreateTray(db, device, trayNumber, token)
                trayNumber += 1
                createdTrays.Add(currentTray)
                remainingTraySpace = 12
              End If
              remainingTraySpace -= 1

              If Not incomingFibers.Any Then Continue For
              Dim inFiber = incomingFibers.Dequeue()
              Dim connector As FiberOpticConnector = If(TypeOf device Is FiberOpticTerminator, Await CreateConnector(db, device, connectorCount, token), Nothing)
              If connector IsNot Nothing Then
                connectorCount += 1
                createdConnectors.Add(connector)
                Dim connFiber As FiberOpticConnector.FiberToConnectorSpliceStructure
                connFiber.Connector = connector
                connFiber.ConnectedFeatureFIDAttr = inFiber.FIDAttr
                connFiber.PlaceNumber = 12 - remainingTraySpace
                connFiber.TrayFid = currentTray.FID
                connectorFiberConnections.Add(connFiber)
              Else
                Dim splice As FiberOpticSplice = Await CreateFiberToTraySplice(db, device, inFiber, currentTray, 12 - remainingTraySpace, connector, token)
                createdSplices.Add(splice)
              End If
              If Not usedFibersByCable.ContainsKey(inFiber.LineFID.Value) Then usedFibersByCable(inFiber.LineFID.Value) = New List(Of (Fiber, Boolean))
              usedFibersByCable(inFiber.LineFID.Value).Add((inFiber, False))

            Else
              Dim splitter = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplitter.CreateNew(c), token)
              splitter.PointFID = device.FIDGeom
              splitter.ModelFID = splitterModel.FID
              'splitter.Name = i.ToString
              createdSplitters.Add(splitter)

              Dim inPath = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplitterPath.CreateNew(c), token)
              inPath.Number = 1
              inPath.PointFID = splitter.FIDGeom
              inPath.ParentFID = device.FIDGeom
              inPath.TypeID = FiberOpticSplitterPathType.IN
              createdSplitterPaths.Add(inPath)
              splitterInPaths.Enqueue(inPath)

              For j = 1 To 32
                Dim splitterpath = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplitterPath.CreateNew(c), token)
                splitterpath.Number = j
                splitterpath.PointFID = splitter.FIDGeom
                splitterpath.ParentFID = device.FIDGeom
                splitterpath.TypeID = FiberOpticSplitterPathType.OUT
                createdSplitterPaths.Add(splitterpath)
                'splitterOutPaths.Enqueue(splitterpath)
              Next

              If remainingTraySpace = 0 Then
                currentTray = Await CreateTray(db, device, trayNumber, token)
                trayNumber += 1
                createdTrays.Add(currentTray)
                remainingTraySpace = 12
              End If
              remainingTraySpace -= 1

              If Not incomingFibers.Any Then Continue For
              Dim inFiber = incomingFibers.Dequeue()
              Dim splice As FiberOpticSplice = Await CreateFiberToSplitterPathSplice(db, device, inFiber, inPath, currentTray, 12 - remainingTraySpace, token)
              createdSplices.Add(splice)
              If Not usedFibersByCable.ContainsKey(inFiber.LineFID.Value) Then usedFibersByCable(inFiber.LineFID.Value) = New List(Of (Fiber, Boolean))
              usedFibersByCable(inFiber.LineFID.Value).Add((inFiber, True))
            End If
          Next
        End If
      Next

      status.Status = $"{FiberOpticTray.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() db.Connection.FeatureClasses(FiberOpticTray.TABLE_NAME).InsertFeatures(createdTrays.Select(Function(f) f.Feature)), token)
      status.Status = $"{FiberOpticSplitter.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticSplitter.InsertBatch(createdSplitters), token)
      status.Status = $"{FiberOpticSplitterPath.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticSplitterPath.InsertBatch(createdSplitterPaths), token)
      status.Status = $"{FiberOpticConnector.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticConnector.InsertBatch(createdConnectors), token)
      status.Status = $"{FiberOpticSplice.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticSplice.InsertFeatures(createdSplices), token)
      status.Status = $"Fasern und Spleiße topologisch verknüpfen..."
      Await db.ExecuteOnDatabase(Sub() FiberOpticConnector.ConnectToFiberBatch(connectorFiberConnections), token)
      status.Status = $"Transaktion abschließen..."
    End Function

    Private Shared Async Function CreateFiberToTraySplice(db As DatabaseWorker, device As FiberOpticDevice, inFiber As Fiber, currentTray As FiberOpticTray, positionNumber As Integer, connector As FiberOpticConnector, token As CancellationToken) As Task(Of FiberOpticSplice)
      Dim splice = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplice.CreateNew(c), token)
      splice.SetTray(currentTray)
      splice.SetFiber1(inFiber)
      If connector IsNot Nothing Then
        splice.PlaceNumber = positionNumber
        splice.SetConnector(connector)
      End If
      splice.PointFID = device.FIDGeom
      splice.Active = True
      Return splice
    End Function

    Private Shared Async Function CreateFiberToFiberSplice(db As DatabaseWorker, device As FiberOpticDevice, inFiber As Fiber, outFiber As Fiber, currentTray As FiberOpticTray, positionNumber As Integer, token As CancellationToken) As Task(Of FiberOpticSplice)
      Dim splice = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplice.CreateNew(c), token)
      splice.SetTray(currentTray)
      splice.SetFiber1(inFiber)
      splice.SetFiber2(outFiber)
      splice.PlaceNumber = positionNumber
      splice.PointFID = device.FIDGeom
      splice.Active = True
      Return splice
    End Function

    Private Shared Async Function CreateSplitterPathToFiberSplice(db As DatabaseWorker, device As FiberOpticDevice, splitterPath As FiberOpticSplitterPath, outFiber As Fiber, currentTray As FiberOpticTray, positionNumber As Integer, token As CancellationToken) As Task(Of FiberOpticSplice)
      Dim splice = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplice.CreateNew(c), token)
      splice.SetTray(currentTray)
      splice.SetFiber2(outFiber)
      splice.SetSplitterPath1(splitterPath)
      splice.PlaceNumber = positionNumber
      splice.PointFID = device.FIDGeom
      splice.Active = True
      Return splice
    End Function

    Private Shared Async Function CreateFiberToSplitterPathSplice(db As DatabaseWorker, device As FiberOpticDevice, inFiber As Fiber, splitterPath As FiberOpticSplitterPath, currentTray As FiberOpticTray, positionNumber As Integer, token As CancellationToken) As Task(Of FiberOpticSplice)
      Dim splice = Await db.ExecuteOnDatabase(Function(c) FiberOpticSplice.CreateNew(c), token)
      splice.SetTray(currentTray)
      splice.SetFiber1(inFiber)
      splice.SetSplitterPath2(splitterPath)
      splice.PlaceNumber = positionNumber
      splice.PointFID = device.FIDGeom
      splice.Active = True
      Return splice
    End Function

    Private Shared Async Function CreateTray(db As DatabaseWorker, device As FiberOpticDevice, trayNumber As Integer, token As CancellationToken) As Task(Of FiberOpticTray)
      Dim newTray = Await db.ExecuteOnDatabase(Function() FiberOpticTray.CreateNew(db.Connection), token)
      newTray.IdClosureSide = FiberOpticClosureSide.UNKNOWN
      newTray.PointFID = device.FIDGeom
      newTray.Number = trayNumber
      Return newTray
    End Function

    Private Shared Async Function CreateConnector(db As DatabaseWorker, device As FiberOpticDevice, number As Long, token As CancellationToken) As Task(Of FiberOpticConnector)
      Dim connector = Await db.ExecuteOnDatabase(Function(c) FiberOpticConnector.CreateNew(c), token)
      connector.Number = number
      connector.PointFID = device.FIDGeom
      Return connector
    End Function

  End Class

End Namespace
