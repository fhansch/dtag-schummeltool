Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports TC.Common.Tools
Imports TC.Common.API.SurveyPlan
Imports TC.FO.API

Namespace BMVI
  Public Class CabinetDialogPlugIn
    Inherits TC.Base.PlugIn.DialogPlugIn.TelcoItemDialogPlugIn

    Private WithEvents _mniMergeClosures As New MenuItem()

    Private Sub DeviceDialogPlugIn_InitMenu(sender As Object, e As MenuEventArgs) Handles Me.InitMenu
      If Me.Dialog.FeatureClass.Name = Cabinet.TABLE_NAME Then
        AddMenuItem(mnuEdit, _mniMergeClosures, "Schummeltool: Muffen und Abschlüsse zusammenfassen")
      End If
    End Sub

    Private Sub _mniMergeClosures_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniMergeClosures.Click
      Try
        If Me.Dialog.Record.Available Then
          Dim currentCabinet = Cabinet.Get(Me.Document.Connection, Me.Dialog.Record.Fid)
          Dim devices = currentCabinet.GetDevices()
          Dim terminators = devices.OfType(Of FiberOpticTerminator).ToList
          If terminators.Count <> 1 Then Return
          Dim closures = devices.OfType(Of FiberOpticClosure).ToList
          If Not closures.Any Then Return

          Dim terminator = terminators.First
          Dim currentTerminatorCables = New HashSet(Of FiberOpticCable)(terminator.GetCables)
          For Each closure In closures
            Dim incomingClosureCables = closure.GetIncomingCables()
            Dim outgoingClosureCables = closure.GetOutgoingCables()
            closure.Delete()
            For Each cable In incomingClosureCables
              If Not currentTerminatorCables.Add(cable) Then
                currentTerminatorCables.Remove(cable)
                cable.Delete()
              End If
              terminator.AddIncomingCable(cable)
            Next
            For Each cable In outgoingClosureCables
              If Not currentTerminatorCables.Add(cable) Then
                currentTerminatorCables.Remove(cable)
                cable.Delete()
              End If
              terminator.AddOutgoingCable(cable)
            Next
          Next
        End If
      Catch ex As Exception
        TC.HandleError(ex)
      End Try
    End Sub
  End Class

End Namespace
