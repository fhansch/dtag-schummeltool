Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports TC.Common.Tools
Imports TC.FO.API

Namespace BMVI
  Public Class DeviceDialogPlugIn
    Inherits TC.Base.PlugIn.DialogPlugIn.TelcoItemDialogPlugIn

    Private WithEvents _mniReplaceTerminators As New MenuItem()
    Private WithEvents _mniReplaceClosures As New MenuItem()

    Private Sub DeviceDialogPlugIn_InitMenu(sender As Object, e As MenuEventArgs) Handles Me.InitMenu
      If Me.Dialog.FeatureClass.Name = FiberOpticDevice.TABLE_NAME_TERMINATOR OrElse Me.Dialog.FeatureClass.Name = FiberOpticDevice.TABLE_NAME_CLOSURE Then
        If Me.Dialog.FeatureClass.Name = FiberOpticDevice.TABLE_NAME_TERMINATOR Then
          AddMenuItem(mnuEdit, _mniReplaceTerminators, "Schummeltool: In Muffe umwandeln (Filter)")
        End If
        If Me.Dialog.FeatureClass.Name = FiberOpticDevice.TABLE_NAME_CLOSURE Then
          AddMenuItem(mnuEdit, _mniReplaceClosures, "Schummeltool: In Abschluss umwandeln (Filter)")
        End If
      End If
    End Sub

    Private Sub _mniReplaceTerminators_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniReplaceTerminators.Click
      Dim openDialogs As IReadOnlyCollection(Of Dialog) = {}
      Try
        If Me.Dialog.Record.Available Then
          openDialogs = GetOpenDialogs(Me.Document, {FiberOpticTerminator.TABLE_NAME})
          CloseDialogs(openDialogs)

          Dim terminators = FiberOpticTerminator.Get(Me.Document.Connection, Me.Dialog.Record.Fids())
          Dim replacer As New DeviceReplace.FiberOpticsDeviceReplacer(Me.Document, terminators)
          replacer.Execute(True)
        End If
      Catch ex As Exception
        TC.HandleError(ex)
      Finally
        ShowDialogs(openDialogs)
      End Try
    End Sub

    Private Sub _mniReplaceClosures_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniReplaceClosures.Click
      Dim openDialogs As IReadOnlyCollection(Of Dialog) = {}
      Try
        If Me.Dialog.Record.Available Then
          openDialogs = GetOpenDialogs(Me.Document, {FiberOpticClosure.TABLE_NAME})
          CloseDialogs(openDialogs)

          Dim closures = FiberOpticClosure.Get(Me.Document.Connection, Me.Dialog.Record.Fids())
          Dim replacer As New DeviceReplace.FiberOpticsDeviceReplacer(Me.Document, closures)
          replacer.Execute(True)
        End If
      Catch ex As Exception
        TC.HandleError(ex)
      Finally
        ShowDialogs(openDialogs)
      End Try
    End Sub


    Friend Function GetOpenDialogs(document As Document, dialogNames As IReadOnlyCollection(Of String)) As IReadOnlyCollection(Of Dialog)
      Dim dialogs As New List(Of Dialog)()

      For Each dialogName In dialogNames
        Dim dialog = document.Dialogs.Item(dialogName)
        If dialog?.Visible Then
          dialogs.Add(dialog)
        End If
      Next

      Return dialogs
    End Function

    Friend Sub CloseDialogs(dialogs As IReadOnlyCollection(Of Dialog))
      For Each currentDialog In dialogs
        currentDialog.Close()
      Next
    End Sub

    Friend Sub ShowDialogs(dialogs As IReadOnlyCollection(Of Dialog))
      For Each currentDialog In dialogs
        currentDialog.Show()
      Next
    End Sub
  End Class

End Namespace
