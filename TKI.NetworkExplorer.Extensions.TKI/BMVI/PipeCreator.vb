Imports System.Threading
Imports Autodesk.AutoCAD.ApplicationServices
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports Autodesk.Map.IM.PlaneGeometry
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.Tools
Imports TKI.Tools.Database
Imports TKI.UI
Imports TKI.UI.Configuration

Namespace BMVI
  Friend Class PipeCreator

    Private Property Document As Autodesk.Map.IM.Forms.Document
    Friend Sub New(doc As Autodesk.Map.IM.Forms.Document)
      _Document = doc
    End Sub

    Friend Sub Run(clusterFid As Long)
      Dim vCluster = Cluster.Get(Document.Connection, clusterFid)
      If vCluster Is Nothing Then Return

      Dim clusterName = vCluster.Name
      clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

      Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf CreatePipesJob)
      statusForm.AbortThreadOnCancel = True
      statusForm.CancelButtonVisible = True
      statusForm.CancelButtonEnabled = True
      statusForm.Parameters = {vCluster}
      statusForm.Status = ""
      statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
      statusForm.Text = "Magie wird gewirkt"
      statusForm.ShowDialog()
      '  End If
    End Sub

    Private Sub CreatePipesJob(status As IStatusDisplay, parameters() As Object)
      If parameters.Length <> 1 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
      If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))

      Dim vCluster = DirectCast(parameters(0), Cluster)

      RunPipesJob(status, vCluster, CancellationToken.None).Wait()
    End Sub

    Private Async Function RunPipesJob(status As IStatusDisplay, vCluster As Cluster, token As CancellationToken) As Task
      Using db As New DatabaseWorker(vCluster.Connection)
        Using burst As New RegenerateLabelDisableBurst(db.Connection)
          Using mapBurst = Document.Map.InitiateDrawBurst()
            Using transaction = Await db.BeginTransaction(token).ConfigureAwait(False)
              Await ExecuteInternal(status, db, vCluster, token).ConfigureAwait(False)
              Await transaction.Commit(token)
            End Using
          End Using
        End Using
      End Using
    End Function

    Private Async Function ExecuteInternal(status As IStatusDisplay, db As DatabaseWorker, vCluster As Cluster, token As CancellationToken) As Task
      Using Duct.SuspendDuctInsertionChecks.Set
        Using Duct.SuspendDuctDeletionChecks.Set
          Using Duct.DisableCalculationOfDuctLength.Set
            Using DuctCable.SuspendAssignmentChecks.Set

              Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
              Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)

              Dim cables = Await FiberOpticCable.GetCache(cache).ConfigureAwait(False)
              Dim ducts = Await DuctInfrastructure.Duct.GetCache(cache).ConfigureAwait(False)
              Dim segments = Await Segment.GetCache(cache).ConfigureAwait(False)
              Dim segmentDuctsBySegmentFid = Await cache.SegmentDuctsBySegmentFid.ConfigureAwait(False)
              Dim ductCablesByLineFid = Await cache.DuctCablesByLineFid.ConfigureAwait(False)
              Dim ductCablesToDelete As New HashSet(Of DuctInfrastructure.DuctCable)
              Dim ductTypes = Await DuctInfrastructure.DuctType.GetCache(cache).ConfigureAwait(False)
              Dim ductTypeSnr10 As DuctInfrastructure.DuctType = ductTypes.Values.Where(Function(dt) String.Equals("SNR 10x1", dt.Type, StringComparison.OrdinalIgnoreCase)).FirstOrDefault
              If ductTypeSnr10 Is Nothing Then Return
              Dim ductTypeDn50 As DuctInfrastructure.DuctType = ductTypes.Values.Where(Function(dt) dt.Feature.Attributes("ID_BMVI_DUCT_TYPE").ValueNullableLong.GetValueOrDefault(-1) = 22).FirstOrDefault
              If ductTypeDn50 Is Nothing Then Return

              For Each cable In cables.Values
                For Each ductCable In ductCablesByLineFid(cable.FIDGeom)
                  Dim duct = ducts(ductCable.DuctFID)
                  If duct Is Nothing Then Continue For
                  If Not Await IsExistingProtectivePipe(duct, cache, token).ConfigureAwait(False) Then Continue For
                  'Hier müssen wir ein SNR 10/8 zwischen Rohr und Kabel einfügen!
                  ductCablesToDelete.Add(ductCable)

                  Dim innerDuct = Await db.ExecuteOnDatabase(Function() DuctInfrastructure.Duct.CreateNew(db.Connection), token).ConfigureAwait(False)
                  innerDuct.ModelFID = ductTypeSnr10.FID
                  SetLong(1, innerDuct.Feature, "ID_STATE")
                  Await db.ExecuteOnDatabase(Function() innerDuct.Insert(), token).ConfigureAwait(False)

                  Dim ductDuct = Await db.ExecuteOnDatabase(Function() DuctInfrastructure.DuctDuct.CreateNew(db.Connection), token).ConfigureAwait(False)
                  ductDuct.SetOuterDuct(duct)
                  ductDuct.SetInnerDuct(innerDuct)
                  Await db.ExecuteOnDatabase(Function() ductDuct.Insert(), token).ConfigureAwait(False)

                  Dim newDuctCable = Await db.ExecuteOnDatabase(Function() DuctInfrastructure.DuctCable.CreateNew(db.Connection), token).ConfigureAwait(False)
                  newDuctCable.SetDuct(innerDuct)
                  newDuctCable.SetCable(cable)
                  Await db.ExecuteOnDatabase(Function() newDuctCable.Insert(), token).ConfigureAwait(False)
                Next
              Next

              Dim segmentDuctsToDelete As New HashSet(Of SegmentDuct)
              For Each segment In segments.Values
                Dim segmentDucts = segmentDuctsBySegmentFid(segment.FID)
                Dim singleDucts = New HashSet(Of Duct)
                For Each segmentDuct In segmentDucts
                  Dim duct = ducts(segmentDuct.DuctFID.GetValueOrDefault(-1))
                  If Await IsExistingSingleDuct(duct, cache, token).ConfigureAwait(False) Then
                    singleDucts.Add(duct)
                    segmentDuctsToDelete.Add(segmentDuct)
                  End If
                Next
                If Not singleDucts.Any Then Continue For

                Dim protectivePipe = Await db.ExecuteOnDatabase(Function() DuctInfrastructure.Duct.CreateNew(db.Connection), token).ConfigureAwait(False)
                protectivePipe.ModelFID = ductTypeDn50.FID
                SetLong(1, protectivePipe.Feature, "ID_STATE")
                Await db.ExecuteOnDatabase(Function() protectivePipe.Insert(), token).ConfigureAwait(False)

                Dim newSegmentDuct = Await db.ExecuteOnDatabase(Function() SegmentDuct.CreateNew(db.Connection), token).ConfigureAwait(False)
                newSegmentDuct.SetDuct(protectivePipe)
                newSegmentDuct.SetSegment(segment)
                Await db.ExecuteOnDatabase(Function() newSegmentDuct.Insert(), token).ConfigureAwait(False)

                For Each duct In singleDucts
                  Dim ductDuct = Await db.ExecuteOnDatabase(Function() DuctInfrastructure.DuctDuct.CreateNew(db.Connection), token).ConfigureAwait(False)
                  ductDuct.SetOuterDuct(protectivePipe)
                  ductDuct.SetInnerDuct(duct)
                  Await db.ExecuteOnDatabase(Function() ductDuct.Insert(), token).ConfigureAwait(False)
                Next
              Next

              If ductCablesToDelete.Any Then
                Await db.ExecuteOnDatabase(Function()
                                             ductCablesToDelete.First.FeatureClass.DeleteFeatures(ductCablesToDelete.Select(Function(dc) dc.FID))
                                             Return 1
                                           End Function, token).ConfigureAwait(False)
              End If
              If segmentDuctsToDelete.Any Then
                Await db.ExecuteOnDatabase(Function()
                                             segmentDuctsToDelete.First.FeatureClass.DeleteFeatures(segmentDuctsToDelete.Select(Function(dc) dc.FID))
                                             Return 1
                                           End Function, token).ConfigureAwait(False)
              End If
            End Using
          End Using
        End Using
      End Using
    End Function

    Private Async Function IsExistingProtectivePipe(duct As DuctInfrastructure.Duct, cache As ExportCache, token As CancellationToken) As Task(Of Boolean)
      Dim ductTypes = Await DuctInfrastructure.DuctType.GetCache(cache).ConfigureAwait(False)
      If Not ductTypes.ContainsKey(duct.ModelFID.GetValueOrDefault(-1)) Then Return False
      Dim ductType = ductTypes(duct.ModelFID.Value)
      Dim lrArt = ductType.Feature.Attributes("ID_BMVI_DUCT_TYPE").ValueNullableLong.GetValueOrDefault(-1)
      Dim isProtectivePipe = False
      If lrArt = 1 OrElse (lrArt > 20 AndAlso lrArt < 26) Then
        isProtectivePipe = True
      ElseIf lrArt = 99 Then
        Dim otherTypeString = ductType.Name
        If String.Equals("Düker/ /100/11.4/125/40101285", otherTypeString) Then isProtectivePipe = True
        If String.Equals("KKR/ /125/3.7/117/40833663", otherTypeString) Then isProtectivePipe = True
        If String.Equals("KSR/ /125/3.7/125/40150079", otherTypeString) Then isProtectivePipe = True
        If String.Equals("KKR/ /140/5.0/150", otherTypeString) Then isProtectivePipe = True
      End If

      If Not isProtectivePipe Then Return False
      If duct.Feature.Attributes("ID_STATE").ValueNullableLong.GetValueOrDefault(-1) = 1 Then
        Return True
      End If
      Return False
    End Function

    Private Async Function IsExistingSingleDuct(duct As DuctInfrastructure.Duct, cache As ExportCache, token As CancellationToken) As Task(Of Boolean)
      Dim ductTypes = Await DuctInfrastructure.DuctType.GetCache(cache).ConfigureAwait(False)
      If Not ductTypes.ContainsKey(duct.ModelFID.GetValueOrDefault(-1)) Then Return False
      Dim ductType = ductTypes(duct.ModelFID.Value)
      If Not String.Equals("SNR 10x1", ductType.Type, StringComparison.OrdinalIgnoreCase) Then Return False

      If duct.Feature.Attributes("ID_STATE").ValueNullableLong.GetValueOrDefault(-1) = 1 Then
        Return True
      End If
      Return False
    End Function


  End Class

End Namespace
