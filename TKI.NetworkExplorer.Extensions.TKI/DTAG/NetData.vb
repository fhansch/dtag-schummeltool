Imports System.Threading
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.FO.API
Imports TKI.Tools.Database

Namespace DTAG
  Friend Class NetData
    Friend Property Connection As TBConnection
    Friend Property Closures As IDictionary(Of PlanningNode, FiberOpticClosure)
    Friend Property Terminators As IDictionary(Of PlanningNode, FiberOpticTerminator)
    Friend Property Cables As IList(Of FiberOpticCable)
    Friend Property CableLinking As IList(Of (Cable As FiberOpticCable, Device As FiberOpticDevice, FlowForward As Boolean))
    Friend Property DuctCables As IList(Of DuctCable)
    Friend Property SegmentDucts As IList(Of SurveyPlan.SegmentDuct)
    Friend Property SegmentCables As IList(Of SurveyPlan.SegmentCable)
    Friend Property SegmentsByEdge As IDictionary(Of PlanningEdge, TC.Common.API.SurveyPlan.Segment)
    Friend Property DirectDuctsByEdge As IDictionary(Of PlanningEdge, IList(Of Duct))
    Friend Property DuctModelsByName As IDictionary(Of String, DuctType)
    Friend Property CableModelsByName As IDictionary(Of String, FiberOpticCableModel)
    Friend Property Trays As IList(Of FiberOpticTray)
    Friend Property TrayModel As FiberOpticTrayModel = Nothing
    Friend Property DemandInSplices As List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
    Friend Property PopOutSplices As List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
    Friend Property ClosureInSplitterSplices As List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
    Friend Property ClosureOutSplitterSplices As List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
    Friend Property ClosureInSpareSplices As List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
    Friend Property DeviceOutFeederSplices As List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer))
    Friend Property DeviceSharedFeederSplices As List(Of (Device As FiberOpticDevice, IncomingCable As FiberOpticCable, IncomingNumber As Integer, OutgoingCable As FiberOpticCable, OutgoingNumber As Integer, FiberOpticTray As FiberOpticTray))
    Friend Property Cabinets As IDictionary(Of PlanningNode, SurveyPlan.Cabinet)
    Friend Property Manholes As IDictionary(Of PlanningNode, SurveyPlan.Manhole)
    Friend Property Poles As IDictionary(Of PlanningNode, SurveyPlan.Pole)

    Friend Sub New(connection As TBConnection, existingDuctModels As IEnumerable(Of DuctType), existingCableModels As IEnumerable(Of FiberOpticCableModel), existingTrayModels As IEnumerable(Of FiberOpticTrayModel))
      Me.Connection = connection
      Me.TrayModel = existingTrayModels.Where(Function(t) t.Name = "SE").FirstOrDefault
      Me.Terminators = New Dictionary(Of PlanningNode, FiberOpticTerminator)
      Me.Closures = New Dictionary(Of PlanningNode, FiberOpticClosure)
      Me.Cables = New List(Of FiberOpticCable)
      Me.CableLinking = New List(Of (Cable As FiberOpticCable, Device As FiberOpticDevice, FlowForward As Boolean))
      Me.DuctCables = New List(Of DuctCable)
      Me.SegmentDucts = New List(Of SurveyPlan.SegmentDuct)
      Me.SegmentCables = New List(Of SurveyPlan.SegmentCable)
      Me.SegmentsByEdge = New Dictionary(Of PlanningEdge, TC.Common.API.SurveyPlan.Segment)
      Me.DirectDuctsByEdge = New Dictionary(Of PlanningEdge, IList(Of DuctInfrastructure.Duct))
      Me.DuctModelsByName = existingDuctModels.Where(Function(dt) Not String.IsNullOrEmpty(dt.Name) AndAlso dt.IsActiveModel).ToDictionary(Function(dt) dt.Name)
      Me.CableModelsByName = existingCableModels.Where(Function(cm) Not String.IsNullOrEmpty(cm.Name) AndAlso cm.IsActiveModel).ToDictionary(Function(cm) cm.Name)
      Me.Trays = New List(Of FiberOpticTray)
      Me.DemandInSplices = New List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
      Me.ClosureInSplitterSplices = New List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
      Me.ClosureOutSplitterSplices = New List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
      Me.ClosureInSpareSplices = New List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
      Me.DeviceOutFeederSplices = New List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer))
      Me.DeviceSharedFeederSplices = New List(Of (Device As FiberOpticDevice, IncomingCable As FiberOpticCable, IncomingNumber As Integer, OutgoingCable As FiberOpticCable, OutgoingNumber As Integer, FiberOpticTray As FiberOpticTray))
      Me.PopOutSplices = New List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
      Me.Cabinets = New Dictionary(Of PlanningNode, SurveyPlan.Cabinet)
      Me.Manholes = New Dictionary(Of PlanningNode, SurveyPlan.Manhole)
      Me.Poles = New Dictionary(Of PlanningNode, SurveyPlan.Pole)
    End Sub

    Friend Async Function InsertStructures(db As DatabaseWorker, token As CancellationToken) As Task
      Await db.ExecuteOnDatabase(Function() SurveyPlan.Manhole.InsertBatch(Manholes.Values.Distinct), token)
      Await db.ExecuteOnDatabase(Function() SurveyPlan.Cabinet.InsertBatch(Cabinets.Values.Distinct), token)
      Await db.ExecuteOnDatabase(Function() SurveyPlan.Pole.InsertBatch(Poles.Values.Distinct), token)
    End Function

    Friend Async Function Insert(status As IStatusDisplay, db As DatabaseWorker, token As CancellationToken) As Task

      status.Status = $"{FiberOpticTerminator.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticTerminator.InsertBatch(Terminators.Values.Distinct), token)
      status.Status = $"{FiberOpticClosure.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticClosure.InsertBatch(Closures.Values.Distinct), token)
      status.Status = $"{SurveyPlan.Segment.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() SurveyPlan.Segment.InsertBatch(SegmentsByEdge.Values.Distinct), token)
      status.Status = $"{FiberOpticCable.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticCable.InsertBatch(Cables.Distinct), token)
      status.Status = $"{DuctCable.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() db.Connection.FeatureClasses(DuctInfrastructure.DuctCable.TABLE_NAME).InsertFeatures(DuctCables.Select(Function(f) f.Feature)), token)
      status.Status = $"{SurveyPlan.SegmentDuct.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() db.Connection.FeatureClasses(SurveyPlan.SegmentDuct.TABLE_NAME).InsertFeatures(SegmentDucts.Select(Function(f) f.Feature)), token)
      status.Status = $"{SurveyPlan.SegmentCable.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() db.Connection.FeatureClasses(SurveyPlan.SegmentCable.TABLE_NAME).InsertFeatures(SegmentCables.Select(Function(f) f.Feature)), token)
      status.Status = $"{FiberOpticTray.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() db.Connection.FeatureClasses(FiberOpticTray.TABLE_NAME).InsertFeatures(Trays.Select(Function(f) f.Feature)), token)


      status.Status = $"Kabel in Geräte einführen..."
      For Each cableLink In CableLinking
        If cableLink.FlowForward Then
          Await db.ExecuteOnDatabase(Sub()
                                       cableLink.Device.AddIncomingCable(cableLink.Cable)
                                     End Sub, token)
        Else
          Await db.ExecuteOnDatabase(Sub()
                                       cableLink.Device.AddOutgoingCable(cableLink.Cable)
                                     End Sub, token)
        End If

      Next

      Dim splitters = New List(Of FiberOpticSplitter)
      Dim splitterModel = FiberOpticSplitterModel.GetActiveModels(Connection).Where(Function(sm) sm.Outs.HasValue AndAlso sm.Outs.Value = 32).FirstOrDefault
      Dim splitterInPaths = New Queue(Of FiberOpticSplitterPath)
      Dim splitterOutPaths = New Queue(Of FiberOpticSplitterPath)
      Dim splitterPaths = New List(Of FiberOpticSplitterPath)
      If splitterModel IsNot Nothing Then
        For Each splicesByNVT In ClosureOutSplitterSplices.GroupBy(Function(f) f.Device)
          For i = 1 To Math.Ceiling(splicesByNVT.Count / 32)
            Dim splitter = FiberOpticSplitter.CreateNew(Connection)
            splitter.PointFID = splicesByNVT.Key.FIDGeom
            splitter.ModelFID = splitterModel.FID
            splitter.Name = i.ToString
            splitters.Add(splitter)

            Dim inPath = FiberOpticSplitterPath.CreateNew(Connection)
            inPath.Number = 1
            inPath.PointFID = splitter.FIDGeom
            inPath.ParentFID = splicesByNVT.Key.FIDGeom
            inPath.TypeID = FiberOpticSplitterPathType.IN
            splitterPaths.Add(inPath)
            splitterInPaths.Enqueue(inPath)

            For j = 1 To 32
              Dim splitterpath = FiberOpticSplitterPath.CreateNew(Connection)
              splitterpath.Number = j
              splitterpath.PointFID = splitter.FIDGeom
              splitterpath.ParentFID = splicesByNVT.Key.FIDGeom
              splitterpath.TypeID = FiberOpticSplitterPathType.OUT
              splitterPaths.Add(splitterpath)
              splitterOutPaths.Enqueue(splitterpath)
            Next
          Next
        Next
      End If

      status.Status = $"{FiberOpticSplitter.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticSplitter.InsertBatch(splitters), token)
      status.Status = $"{FiberOpticSplitterPath.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticSplitterPath.InsertBatch(splitterPaths), token)

      Dim splices = New ArrayList
      Dim connectors = New List(Of FiberOpticConnector)

      status.Status = $"Fasern laden..."
      Dim fibersByCable = Fiber.Get(Connection).ToLookup(Function(f) f.LineFID.Value)
      Dim counterByPoint = New Dictionary(Of Long, Integer)

      If splitterModel IsNot Nothing Then
        For Each splicesByNVT In ClosureInSplitterSplices.GroupBy(Function(f) f.Device)
          For Each nvtSplice In splicesByNVT
            Dim fiber = fibersByCable(nvtSplice.Cable.FIDGeom.Value).Where(Function(f) f.Number.HasValue AndAlso f.Number.Value = nvtSplice.Number).FirstOrDefault
            If fiber Is Nothing Then
              Debug.Fail("Faser nicht gefunden!")
              Continue For
            End If
            If Not splitterInPaths.Any Then
              Debug.Fail("Splitterpfad nicht gefunden!")
              Continue For
            End If
            If Not counterByPoint.ContainsKey(nvtSplice.FiberOpticTray.FID) Then
              counterByPoint(nvtSplice.FiberOpticTray.FID) = 1
            Else
              counterByPoint(nvtSplice.FiberOpticTray.FID) += 1
            End If

            Dim splice = FiberOpticSplice.CreateNew(Connection)
            splice.SetTray(nvtSplice.FiberOpticTray)
            splice.SetSplitterPath1(splitterInPaths.Dequeue)
            splice.SetFiber2(fiber)
            splice.PlaceNumber = counterByPoint(nvtSplice.FiberOpticTray.FID)
            splice.PointFID = nvtSplice.Device.FIDGeom
            splice.Active = True

            splices.Add(splice)
          Next
        Next

        For Each splicesByNVT In ClosureOutSplitterSplices.GroupBy(Function(f) f.Device)
          For Each nvtSplice In splicesByNVT
            Dim fiber = fibersByCable(nvtSplice.Cable.FIDGeom.Value).Where(Function(f) f.Number.HasValue AndAlso f.Number.Value = nvtSplice.Number).FirstOrDefault
            If fiber Is Nothing Then
              Debug.Fail("Faser nicht gefunden!")
              Continue For
            End If
            If Not splitterOutPaths.Any Then
              Debug.Fail("Splitterpfad nicht gefunden!")
              Continue For
            End If
            If Not counterByPoint.ContainsKey(nvtSplice.FiberOpticTray.FID) Then
              counterByPoint(nvtSplice.FiberOpticTray.FID) = 1
            Else
              counterByPoint(nvtSplice.FiberOpticTray.FID) += 1
            End If

            Dim splice = FiberOpticSplice.CreateNew(Connection)
            splice.SetTray(nvtSplice.FiberOpticTray)
            splice.SetSplitterPath2(splitterOutPaths.Dequeue)
            splice.SetFiber1(fiber)
            splice.PlaceNumber = counterByPoint(nvtSplice.FiberOpticTray.FID)
            splice.PointFID = nvtSplice.Device.FIDGeom
            splice.Active = True

            splices.Add(splice)
          Next
        Next
      End If

      For Each closureSplice In Me.DeviceSharedFeederSplices
        'Durchspleißen:
        Dim inFiber = fibersByCable(closureSplice.IncomingCable.FIDGeom.Value).Where(Function(f) f.Number.HasValue AndAlso f.Number.Value = closureSplice.IncomingNumber).FirstOrDefault
        If inFiber Is Nothing Then
          Debug.Fail("Faser nicht gefunden!")
          Continue For
        End If
        Dim outFiber = fibersByCable(closureSplice.OutgoingCable.FIDGeom.Value).Where(Function(f) f.Number.HasValue AndAlso f.Number.Value = closureSplice.OutgoingNumber).FirstOrDefault
        If outFiber Is Nothing Then
          Debug.Fail("Faser nicht gefunden!")
          Continue For
        End If
        If Not counterByPoint.ContainsKey(closureSplice.FiberOpticTray.FID) Then
          counterByPoint(closureSplice.FiberOpticTray.FID) = 1
        Else
          counterByPoint(closureSplice.FiberOpticTray.FID) += 1
        End If

        Dim splice = FiberOpticSplice.CreateNew(Connection)
        splice.SetTray(closureSplice.FiberOpticTray)
        splice.SetFiber1(inFiber)
        splice.SetFiber2(outFiber)
        splice.PlaceNumber = counterByPoint(closureSplice.FiberOpticTray.FID)
        splice.PointFID = closureSplice.Device.FIDGeom
        splice.Active = True

        splices.Add(splice)
      Next

      For Each closureSplice In Me.ClosureInSpareSplices
        'Ablegen:
        Dim inFiber = fibersByCable(closureSplice.Cable.FIDGeom.Value).Where(Function(f) f.Number.HasValue AndAlso f.Number.Value = closureSplice.Number).FirstOrDefault
        If inFiber Is Nothing Then
          Debug.Fail("Faser nicht gefunden!")
          Continue For
        End If

        Dim splice = FiberOpticSplice.CreateNew(Connection)
        splice.SetTray(closureSplice.FiberOpticTray)
        splice.SetFiber1(inFiber)
        'splice.PlaceNumber = counterByPoint(closureSplice.FiberOpticTray.FID)
        splice.PointFID = closureSplice.Device.FIDGeom
        splice.Active = True

        splices.Add(splice)
      Next

      'Kupplungen als letztes:
      Dim connectorFiberConnections As New List(Of FiberOpticConnector.FiberToConnectorSpliceStructure)
      For Each terminatorSplice In Me.DemandInSplices.Concat(Me.PopOutSplices)
        If Not counterByPoint.ContainsKey(terminatorSplice.FiberOpticTray.PointFID.Value) Then
          counterByPoint(terminatorSplice.FiberOpticTray.PointFID.Value) = 1
        Else
          counterByPoint(terminatorSplice.FiberOpticTray.PointFID.Value) += 1
        End If
        Dim fiber = fibersByCable(terminatorSplice.Cable.FIDGeom.Value).Where(Function(f) f.Number.HasValue AndAlso f.Number.Value = terminatorSplice.Number).FirstOrDefault
        If fiber Is Nothing Then
          Debug.Fail("Faser nicht gefunden!")
          Continue For
        End If
        If Not counterByPoint.ContainsKey(terminatorSplice.FiberOpticTray.FID) Then
          counterByPoint(terminatorSplice.FiberOpticTray.FID) = 1
        Else
          counterByPoint(terminatorSplice.FiberOpticTray.FID) += 1
        End If

        Dim connector = FiberOpticConnector.CreateNew(Connection)
        connector.Number = counterByPoint(terminatorSplice.FiberOpticTray.PointFID.Value)
        connector.PointFID = terminatorSplice.Device.FIDGeom
        connectors.Add(connector)


        Dim connFiber As FiberOpticConnector.FiberToConnectorSpliceStructure
        connFiber.Connector = connector
        connFiber.ConnectedFeatureFIDAttr = fiber.FIDAttr
        connFiber.PlaceNumber = counterByPoint(terminatorSplice.FiberOpticTray.FID)
        connFiber.TrayFid = terminatorSplice.FiberOpticTray.FID
        connectorFiberConnections.Add(connFiber)
      Next

      status.Status = $"{FiberOpticConnector.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticConnector.InsertBatch(connectors), token)
      status.Status = $"{FiberOpticSplice.TABLE_NAME} einfügen..."
      Await db.ExecuteOnDatabase(Function() FiberOpticSplice.InsertFeatures(splices), token)
      status.Status = $"Fasern und Spleiße topologisch verknüpfen..."
      Await db.ExecuteOnDatabase(Sub() FiberOpticConnector.ConnectToFiberBatch(connectorFiberConnections), token)
      status.Status = $"Transaktion abschließen..."
    End Function
  End Class
End Namespace
