Namespace DTAG
  Public Class DTAGError
    Public Property FID As Long?
    Public Property Text As String
    Public Property TableName As String

    Public Sub New(text As String, tableName As String, fid As Long?)
      Me.TableName = tableName
      Me.Text = text
      Me.FID = fid
    End Sub
  End Class
End Namespace

