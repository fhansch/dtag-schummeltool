Namespace DTAG
  Friend Enum DTAGLayerType
    All = 0
    Backbone = 1
    Feeder = 2
    Distribution = 3
    Drop = 4
  End Enum
End Namespace
