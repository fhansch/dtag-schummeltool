Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Graphic
Imports TC.Common.API
Imports TC.FO.API

Namespace DTAG
  Friend Class PlanningNode
    Public Property Feature As Feature
    Public Property Geometry As Autodesk.Map.IM.Graphic.Point
    Public Property TotalLength As Double
    Public Property Type As NodeType
    Public Property [Next] As New List(Of PlanningEdge)
    Public Property Previous As PlanningEdge
    Public Property Building As Topography.Building
    Public Property Name As String
    Public Property Info As String
    Public Property Cluster As Polygon
    Public Property SpareFibers As Long = 0
    Public Property IncomingCableTypes As Queue(Of FiberOpticCableModel) = New Queue(Of FiberOpticCableModel)
  End Class
End Namespace
