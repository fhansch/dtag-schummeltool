Imports System.Threading
Imports Autodesk.AutoCAD.ApplicationServices
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports Autodesk.Map.IM.PlaneGeometry
Imports TC.Common.API
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.Tools
Imports TKI.Tools.Database
Imports TKI.UI
Imports TKI.UI.Configuration

Namespace DTAG
  Friend Class DTAGAssistent

    Private _errors As IList(Of DTAGError)
    Private Property Document As Autodesk.Map.IM.Forms.Document
    Private Property PositionByOuterDuctFid As New Dictionary(Of Long, Long)
    Friend Sub New(doc As Autodesk.Map.IM.Forms.Document)
      _errors = New List(Of DTAGError)
      _Document = doc
    End Sub

    Private ReadOnly Property DistributionTypes As NodeType()
      Get
        Return ({NodeType.NVTCabinet, NodeType.NVTManhole, NodeType.PoleClosure, NodeType.ManholeClosure, NodeType.CabinetClosure, NodeType.Closure})
      End Get
    End Property

    Friend Sub RunAssistant(clusterFid As Long)
      Dim vCluster = Cluster.Get(Document.Connection, clusterFid)
      If vCluster Is Nothing Then Return

      Dim clusterName = vCluster.Name
      clusterName = If(String.IsNullOrWhiteSpace(clusterName), "Export", clusterName)

      _errors.Clear()
      Dim statusForm = New Desktop.StatusForm(Of Object)(AddressOf CreateDTAGJob)
      statusForm.AbortThreadOnCancel = True
      statusForm.CancelButtonVisible = True
      statusForm.CancelButtonEnabled = True
      statusForm.Parameters = {vCluster}
      statusForm.Status = ""
      statusForm.Type = Desktop.ProgressBarType.ContinuousProgressBar
      statusForm.Text = "Magie wird gewirkt"
      statusForm.ShowDialog()
      '  End If
      If _errors.Any Then
        Dim errorLog = New TC.frmErrorLog(Document)
        errorLog.Visible = False
        errorLog.ClearError()
        For Each dtagError In _errors
          If dtagError.FID.HasValue Then
            errorLog.AddNewError(dtagError.Text, dtagError.TableName, dtagError.FID.Value, False)
          Else
            errorLog.AddNewError(dtagError.Text, dtagError.TableName, 0, False)
          End If
        Next
        errorLog.Visible = True
        errorLog.ShowError()
      End If
    End Sub

    Private Sub CreateDTAGJob(status As IStatusDisplay, parameters() As Object)
      If parameters.Length <> 1 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
      If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))

      Dim vCluster = DirectCast(parameters(0), Cluster)

      _errors = RunDTAGJob(status, vCluster, CancellationToken.None).Result
    End Sub

    Private Async Function RunDTAGJob(status As IStatusDisplay, vCluster As Cluster, token As CancellationToken) As Task(Of IList(Of DTAGError))
      Using db As New DatabaseWorker(vCluster.Connection)
        Using burst As New RegenerateLabelDisableBurst(db.Connection)
          Using mapBurst = Document.Map.InitiateDrawBurst()
            Using transaction = Await db.BeginTransaction(token)
              Dim errors = Await ExecuteInternal(status, db, vCluster, token)

              If errors.Any Then
                Return errors
              Else
                Await transaction.Commit(token)
              End If
            End Using
          End Using
        End Using
      End Using
      Return New List(Of DTAGError)
    End Function

    Private Async Function ExecuteInternal(status As IStatusDisplay, db As DatabaseWorker, vCluster As Cluster, token As CancellationToken) As Task(Of IList(Of DTAGError))
      Dim errors = New List(Of DTAGError)
      Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
      Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)

      'STEP 0: ProcessNetwork
      status.Status = "Lade Quelldaten..."
      Dim lineFeatures = Await db.ExecuteOnDatabase(Function() db.Connection.FeatureClasses("TC_DC_SEGMENT").GetFeatures(True, sms, vCluster.Geometry).ToList, token)
      Dim pointFeatures = Await db.ExecuteOnDatabase(Function() db.Connection.FeatureClasses("TC_DC_LAYER_POINT").GetFeatures(True, sms, vCluster.Geometry).ToList, token)
      Dim existingDuctModels = Await DuctInfrastructure.DuctType.GetCache(cache)
      Dim existingCableModels = Await FiberOpticCableModel.GetCache(cache)

      status.Status = "Erzeuge Knoten und Kanten..."
      Dim existingTrayModels = Await FiberOpticTrayModel.GetCache(cache)
      Dim netData As New NetData(cache.Db.Connection, existingDuctModels.Values, existingCableModels.Values, existingTrayModels.Values)
      Dim nodesAndErrors = Await ProcessNodes(pointFeatures, cache, netData)
      If nodesAndErrors.Errors.Any Then Return nodesAndErrors.Errors 'Abbruch und Fehlerausgabe
      Dim processedNodes As HashSet(Of PlanningNode) = nodesAndErrors.PlanningNodes
      Dim linesAndErrors = ProcessLines(db.Connection.DefaultSpatialTolerance, processedNodes, lineFeatures)
      If linesAndErrors.Errors.Any Then Return linesAndErrors.Errors 'Abbruch und Fehlerausgabe
      Dim processedLines As HashSet(Of PlanningEdge) = linesAndErrors.PlanningEdges

      Dim possibleRootNodes = processedNodes.Where(Function(n) n.Type = NodeType.NVTCabinet OrElse n.Type = NodeType.NVTManhole).ToList
      If possibleRootNodes.Count = 1 Then
        possibleRootNodes(0).Cluster = TryCast(vCluster.Geometry, Polygon)
      End If

      'STEP 1: Strukturen aufbauen
      status.Status = "Strukturen erzeugen..."
      netData = Await CreateStructures(cache, processedNodes, netData, token)
      netData = Await CreateFODevices(cache, processedNodes, netData, token)

      'STEP 2: Gebäude an NVTs anschließen
      Dim buildingNodes = processedNodes.Where(Function(n) n.Type = NodeType.Building).ToList
      Dim counter = 1
      For Each rootNode In possibleRootNodes
        status.Status = $"Erzeuge Verteilnetz {counter.ToString}/{possibleRootNodes.Count.ToString}"
        counter += 1
        Dim linesAndNodes = CreateNetwork(rootNode, processedNodes, processedLines, db.Connection.DefaultSpatialTolerance * 2)

        Dim nvtClosure = netData.Closures(rootNode)
        If rootNode.Cluster Is Nothing Then Continue For 'Ohne Cluster gibts keine Anschlüsse

        For Each buildingNode In buildingNodes.Except(linesAndNodes.RemainingNodes).OrderByDescending(Function(n) n.TotalLength)
          If Not rootNode.Cluster.AnyInteract2DPredicate(db.Connection.DefaultSpatialTolerance)(buildingNode.Geometry) Then Continue For 'Wir wollen nur anschließen was im Cluster liegt, der Rest ist raus!

          Dim requiredFiberCount = CalculateFiberCount(buildingNode, netData)
          Dim dropCableCount = 1

          While requiredFiberCount > 0
            Dim cableType As FiberOpticCableModel = PickCableModel(netData, requiredFiberCount, DTAGLayerType.Drop)
            If cableType Is Nothing Then
              errors.Add(New DTAGError("Kein Kabeltyp für Layer Drop gefunden!", vCluster.FeatureClass.Name, vCluster.FID))
              Exit While
            End If
            Dim usedFibers = If(requiredFiberCount > cableType.FiberCount.GetValueOrDefault(0), cableType.FiberCount.GetValueOrDefault(0), requiredFiberCount)

            Dim errorsInConnection = Await CreateBuildingConnection(db, netData, nvtClosure, buildingNode, cableType, usedFibers, dropCableCount, DTAGLayerType.Drop, token)
            errors.AddRange(errorsInConnection)

            dropCableCount += 1

            requiredFiberCount -= usedFibers
          End While
        Next
      Next

      'STEP 3: Feeder Connection
      Dim popNodes = processedNodes.Where(Function(n) n.Type = NodeType.HVTCabinet).ToList
      If popNodes.Count > 1 Then
        errors.Add(New DTAGError("Mehr als ein HVT im Gebiet! Abbruch!", vCluster.FeatureClass.Name, vCluster.FID))
        Return errors
      End If
      If popNodes.Any Then
        Dim rootnode = popNodes.FirstOrDefault
        Dim linesAndNodes = CreateNetwork(rootnode, processedNodes, processedLines, db.Connection.DefaultSpatialTolerance * 2)
        Dim popTerminator = netData.Terminators(rootnode)
        Dim closureNodes = processedNodes.Where(Function(n) DistributionTypes.Concat({NodeType.MSANCabinet}).Contains(n.Type)).Except(linesAndNodes.RemainingNodes).ToList
        Dim hvtCounter = 1
        For Each deviceNode In closureNodes.OrderByDescending(Function(n) n.TotalLength)
          status.Status = $"Erzeuge Hauptkabelnetz {hvtCounter.ToString}/{closureNodes.Count.ToString}"
          hvtCounter += 1
          Dim requiredFiberCount = CalculateFiberCount(deviceNode, netData)

          Dim endDevice As FiberOpticDevice = Nothing
          If netData.Closures.ContainsKey(deviceNode) Then
            endDevice = netData.Closures(deviceNode)
          ElseIf netData.Terminators.ContainsKey(deviceNode) Then
            endDevice = netData.Terminators(deviceNode)
          End If
          Dim cables = New List(Of FiberOpticCable)
          Dim startDevice As FiberOpticDevice = Nothing

          While requiredFiberCount > 0 OrElse deviceNode.IncomingCableTypes.Any
            Dim cableType = If(deviceNode.IncomingCableTypes.Any, deviceNode.IncomingCableTypes.Dequeue, PickCableModel(netData, requiredFiberCount, DTAGLayerType.Feeder))
            If cableType Is Nothing Then
              errors.Add(New DTAGError("Kein Kabeltyp für Layer Feeder gefunden!", vCluster.FeatureClass.Name, vCluster.FID))
              Exit While
            End If
            Dim usedFibers = If(requiredFiberCount > cableType.FiberCount.GetValueOrDefault(0), cableType.FiberCount.GetValueOrDefault(0), requiredFiberCount)

            Dim result = Await CreatePopConnection(db, netData, deviceNode, cableType, DTAGLayerType.Feeder, token)
            errors.AddRange(result.ErrorsInConnection)
            cables.Add(result.Cable)
            startDevice = result.StartDevice

            requiredFiberCount -= usedFibers
          End While

          If TypeOf endDevice Is FiberOpticClosure Then
            Await CreateClosureSplices(db, netData, startDevice, cables, DirectCast(endDevice, FiberOpticClosure), deviceNode.SpareFibers, token)
          ElseIf TypeOf endDevice Is FiberOpticTerminator Then
            Await CreateMSANSplices(db, netData, startDevice, cables, DirectCast(endDevice, FiberOpticTerminator), deviceNode.SpareFibers, token)
          End If
        Next

        Await CreatePopSplices(db, netData, popTerminator, token)
      End If

      For Each currentEdge In processedLines
        If String.IsNullOrEmpty(currentEdge.Content) Then Continue For
        Dim segment = Await TryCreateSegmentFromEdge(netData, db, currentEdge, token)
        Dim newErrors = Await TryParseDuctsFromEdge(netData, db, currentEdge, segment, token)
        errors.AddRange(newErrors)
      Next

      If errors.Any Then Return errors
      status.Status = "Daten einfügen..."
      Await netData.Insert(status, db, token)

      Return errors
    End Function

    Private Async Function CreatePopSplices(db As DatabaseWorker, netData As NetData, device As FiberOpticDevice, token As CancellationToken) As Task
      Dim trayNumber = 1

      Dim usedSharedFeederFiberGroups = netData.DeviceOutFeederSplices.Where(Function(s) s.Device.FID = device.FID).ToList.GroupBy(Function(sf) sf.Cable)
      Dim currentSpliceNumber = 1
      For Each usedSharedFeederFibers In usedSharedFeederFiberGroups
        Dim stopIndex = usedSharedFeederFibers.Count + currentSpliceNumber - 1
        Dim outCableFiberQueue = New Queue(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer))(usedSharedFeederFibers)
        While currentSpliceNumber < stopIndex
          Dim newTray = Await db.ExecuteOnDatabase(Function() FiberOpticTray.CreateNew(netData.Connection), token)
          newTray.ModelFID = netData.TrayModel?.FID
          newTray.IdClosureSide = FiberOpticClosureSide.UNKNOWN
          newTray.PointFID = device.FIDGeom
          newTray.Number = trayNumber
          trayNumber += 1
          netData.Trays.Add(newTray)

          Dim remainingTraySpace = 12

          For i = currentSpliceNumber To stopIndex
            If remainingTraySpace = 0 Then Exit For
            Dim outCableFiber = outCableFiberQueue.Dequeue
            netData.PopOutSplices.Add((device, outCableFiber.Cable, outCableFiber.Number, newTray))

            remainingTraySpace -= 1
            currentSpliceNumber += 1
          Next
        End While
      Next
    End Function

    Private Async Function CreateMSANSplices(db As DatabaseWorker, netData As NetData, startDevice As FiberOpticDevice, cables As List(Of FiberOpticCable), terminator As FiberOpticTerminator, spareFibers As Long, token As CancellationToken) As Task
      Dim incomingCableQueue = CreateQueue(cables, netData)
      Dim endTrayNumber = 1

      'Fall 4: Abgelegte Muffen-Reserven:
      Dim currentSpliceNumber = 1
      While currentSpliceNumber < spareFibers
        Dim endTray = Await db.ExecuteOnDatabase(Function() FiberOpticTray.CreateNew(netData.Connection), token)
        endTray.ModelFID = netData.TrayModel?.FID
        endTray.IdClosureSide = FiberOpticClosureSide.UNKNOWN
        endTray.PointFID = terminator.FIDGeom
        endTray.Number = endTrayNumber
        endTrayNumber += 1
        netData.Trays.Add(endTray)

        Dim remainingTraySpace = 12

        For i = currentSpliceNumber To spareFibers
          If remainingTraySpace = 0 Then Exit For
          Dim inCableFiber = incomingCableQueue.Dequeue
          netData.DemandInSplices.Add((terminator, inCableFiber.Cable, inCableFiber.Number, endTray))
          netData.DeviceOutFeederSplices.Add((startDevice, inCableFiber.Cable, inCableFiber.Number))

          remainingTraySpace -= 1
          currentSpliceNumber += 1
        Next
      End While
    End Function


    Private Async Function CreateClosureSplices(db As DatabaseWorker, netData As NetData, startDevice As FiberOpticDevice, cables As List(Of FiberOpticCable), endClosure As FiberOpticClosure, spareFibers As Long, token As CancellationToken) As Task
      'Fangen wir hinten an - End-Muffe muss erstmal ausgewertet werden:
      Dim incomingCableQueue = CreateQueue(cables, netData)
      Dim endTrayNumber = 1

      'Fall 1: durchgehende Feeder-Fasern auflegen:
      Dim usedSharedFeederFiberGroups = netData.DeviceOutFeederSplices.Where(Function(s) s.Device.FID = endClosure.FID).ToList.GroupBy(Function(sf) sf.Cable)
      Dim currentSpliceNumber = 1
      For Each usedSharedFeederFibers In usedSharedFeederFiberGroups
        Dim stopIndex = usedSharedFeederFibers.Count + currentSpliceNumber - 1
        Dim outCableFiberQueue = New Queue(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer))(usedSharedFeederFibers)
        While currentSpliceNumber < stopIndex
          Dim endTray = Await db.ExecuteOnDatabase(Function() FiberOpticTray.CreateNew(netData.Connection), token)
          endTray.ModelFID = netData.TrayModel?.FID
          endTray.IdClosureSide = FiberOpticClosureSide.UNKNOWN
          endTray.PointFID = endClosure.FIDGeom
          endTray.Number = endTrayNumber
          endTrayNumber += 1
          netData.Trays.Add(endTray)

          Dim remainingTraySpace = 12

          For i = currentSpliceNumber To stopIndex
            If remainingTraySpace = 0 Then Exit For
            Dim inCableFiber = incomingCableQueue.Dequeue
            Dim outCableFiber = outCableFiberQueue.Dequeue
            netData.DeviceSharedFeederSplices.Add((endClosure, inCableFiber.Cable, inCableFiber.Number, outCableFiber.Cable, outCableFiber.Number, endTray))
            netData.DeviceOutFeederSplices.Add((startDevice, inCableFiber.Cable, inCableFiber.Number))

            remainingTraySpace -= 1
            currentSpliceNumber += 1
          Next
        End While
      Next

      'Fall 2: eingehende Splitter-Fasern auflegen:
      Dim outgoingSplitterSplices = netData.ClosureOutSplitterSplices.Where(Function(s) s.Device.FID = endClosure.FID).ToList
      currentSpliceNumber = 1
      Dim inSplitterFibers = CLng(Math.Ceiling(outgoingSplitterSplices.Count / 32))
      While currentSpliceNumber < inSplitterFibers
        Dim endTray = Await db.ExecuteOnDatabase(Function() FiberOpticTray.CreateNew(netData.Connection), token)
        endTray.ModelFID = netData.TrayModel?.FID
        endTray.IdClosureSide = FiberOpticClosureSide.UNKNOWN
        endTray.PointFID = endClosure.FIDGeom
        endTray.Number = endTrayNumber
        endTrayNumber += 1
        netData.Trays.Add(endTray)

        Dim remainingTraySpace = 12

        For i = currentSpliceNumber To inSplitterFibers
          If remainingTraySpace = 0 Then Exit For
          Dim inCableFiber = incomingCableQueue.Dequeue
          netData.ClosureInSplitterSplices.Add((endClosure, inCableFiber.Cable, inCableFiber.Number, endTray))
          netData.DeviceOutFeederSplices.Add((startDevice, inCableFiber.Cable, inCableFiber.Number))

          remainingTraySpace -= 1
          currentSpliceNumber += 1
        Next
      End While

      'Fall 3: Bestehende Ausgehende Kassetten umnummerieren:
      Dim demandSpliceTraysOrdered = outgoingSplitterSplices.Select(Function(s) s.FiberOpticTray).Distinct.OrderBy(Function(t) t.Number.GetValueOrDefault(-1)).ToList
      For Each tray In demandSpliceTraysOrdered
        tray.Number = endTrayNumber
        endTrayNumber += 1
      Next

      'Fall 4: Abgelegte Muffen-Reserven:
      currentSpliceNumber = 1
      While currentSpliceNumber < spareFibers
        Dim endTray = Await db.ExecuteOnDatabase(Function() FiberOpticTray.CreateNew(netData.Connection), token)
        endTray.ModelFID = netData.TrayModel?.FID
        endTray.IdClosureSide = FiberOpticClosureSide.UNKNOWN
        endTray.PointFID = endClosure.FIDGeom
        endTray.Number = endTrayNumber
        endTrayNumber += 1
        netData.Trays.Add(endTray)

        Dim remainingTraySpace = 12

        For i = currentSpliceNumber To spareFibers
          If remainingTraySpace = 0 Then Exit For
          Dim inCableFiber = incomingCableQueue.Dequeue
          netData.ClosureInSpareSplices.Add((endClosure, inCableFiber.Cable, inCableFiber.Number, endTray))
          netData.DeviceOutFeederSplices.Add((startDevice, inCableFiber.Cable, inCableFiber.Number))

          remainingTraySpace -= 1
          currentSpliceNumber += 1
        Next
      End While
    End Function

    Private Function CreateQueue(cables As List(Of FiberOpticCable), netData As NetData) As Queue(Of (Cable As FiberOpticCable, Number As Integer))
      Dim result = New Queue(Of (Cable As FiberOpticCable, Number As Integer))
      For Each cable In cables
        If cable Is Nothing Then Continue For
        Dim model = netData.CableModelsByName.Values.Where(Function(m) m.FID = cable.ModelFID.Value).FirstOrDefault
        For i = 1 To model.FiberCount.Value
          result.Enqueue((cable, CInt(i)))
        Next
      Next
      Return result
    End Function

    Private Shared Async Function CreateStructures(cache As ExportCache, processedNodes As HashSet(Of PlanningNode), netData As NetData, token As CancellationToken) As Task(Of NetData)
      Dim cabinetModels = Await SurveyPlan.CabinetModel.GetCache(cache)
      Dim cabinetModelsByName = cabinetModels.Values.Where(Function(n) Not String.IsNullOrEmpty(n.Name)).ToDictionary(Function(n) n.Name)
      EnsureCabinetTypeAdded(cache, "NVT-Schrank", cabinetModelsByName)
      EnsureCabinetTypeAdded(cache, "HVT-Schrank", cabinetModelsByName)
      EnsureCabinetTypeAdded(cache, "MSAN", cabinetModelsByName)
      EnsureCabinetTypeAdded(cache, "KVZ", cabinetModelsByName)
      Dim manholeModels = Await SurveyPlan.ManholeModel.GetCache(cache)
      Dim manholeModelsByName = manholeModels.Values.Where(Function(n) Not String.IsNullOrEmpty(n.Name)).ToDictionary(Function(n) n.Name)
      EnsureManholeTypeAdded(cache, "Ziehschacht", manholeModelsByName)
      EnsureManholeTypeAdded(cache, "NVT-Schacht", manholeModelsByName)
      EnsureManholeTypeAdded(cache, "Muffe", manholeModelsByName)
      Dim poleModels = Await SurveyPlan.PoleModel.GetCache(cache)
      Dim poleModelsByName = poleModels.Values.Where(Function(n) Not String.IsNullOrEmpty(n.Name)).ToDictionary(Function(n) n.Name)
      EnsurePoleTypeAdded(cache, "Mast", poleModelsByName)
      EnsurePoleTypeAdded(cache, "Mast-Box", poleModelsByName)

      For Each structNode In processedNodes.Where(Function(n) Not {NodeType.Branch, NodeType.Building}.Contains(n.Type))
        Select Case structNode.Type
          Case NodeType.HVTCabinet, NodeType.NVTCabinet, NodeType.CabinetClosure, NodeType.MSANCabinet
            Dim cabinet = SurveyPlan.Cabinet.CreateNew(cache.Db.Connection)
            cabinet.Geometry = structNode.Geometry
            cabinet.Name = structNode.Name
            cabinet.Info = structNode.Info
            netData.Cabinets(structNode) = cabinet
            Select Case structNode.Type
              Case NodeType.HVTCabinet
                cabinet.ModelFID = cabinetModelsByName("HVT-Schrank").FID
              Case NodeType.NVTCabinet
                cabinet.ModelFID = cabinetModelsByName("NVT-Schrank").FID
              Case NodeType.CabinetClosure
                cabinet.ModelFID = cabinetModelsByName("KVZ").FID
              Case NodeType.MSANCabinet
                cabinet.ModelFID = cabinetModelsByName("MSAN").FID
            End Select
          Case NodeType.Handhole, NodeType.NVTManhole, NodeType.ManholeClosure
            Dim manhole = SurveyPlan.Manhole.CreateNew(cache.Db.Connection)
            manhole.Geometry = structNode.Geometry
            manhole.Name = structNode.Name
            manhole.Info = structNode.Info
            netData.Manholes(structNode) = manhole
            Select Case structNode.Type
              Case NodeType.ManholeClosure
                manhole.ModelFID = manholeModelsByName("Muffe").FID
              Case NodeType.Handhole
                manhole.ModelFID = manholeModelsByName("Ziehschacht").FID
              Case NodeType.NVTManhole
                manhole.ModelFID = manholeModelsByName("NVT-Schacht").FID
            End Select
          Case NodeType.Pole, NodeType.PoleClosure
            Dim pole = SurveyPlan.Pole.CreateNew(cache.Db.Connection)
            pole.Geometry = structNode.Geometry
            pole.Name = structNode.Name
            pole.Info = structNode.Info
            netData.Poles(structNode) = pole
            Select Case structNode.Type
              Case NodeType.Pole
                pole.ModelFID = poleModelsByName("Mast").FID
              Case NodeType.PoleClosure
                pole.ModelFID = poleModelsByName("Mast-Box").FID
            End Select
        End Select
      Next
      Await netData.InsertStructures(cache.Db, token)
      Return netData
    End Function

    Private Shared Sub EnsureCabinetTypeAdded(cache As ExportCache, typeName As String, cabinetModelsByName As Dictionary(Of String, SurveyPlan.CabinetModel))
      If Not cabinetModelsByName.ContainsKey(typeName) Then
        cabinetModelsByName(typeName) = SurveyPlan.CabinetModel.CreateNew(cache.Db.Connection)
        cabinetModelsByName(typeName).Name = typeName
        cabinetModelsByName(typeName).Insert()
      End If
    End Sub

    Private Shared Sub EnsureManholeTypeAdded(cache As ExportCache, typeName As String, cabinetModelsByName As Dictionary(Of String, SurveyPlan.ManholeModel))
      If Not cabinetModelsByName.ContainsKey(typeName) Then
        cabinetModelsByName(typeName) = SurveyPlan.ManholeModel.CreateNew(cache.Db.Connection)
        cabinetModelsByName(typeName).Name = typeName
        cabinetModelsByName(typeName).Insert()
      End If
    End Sub

    Private Shared Sub EnsurePoleTypeAdded(cache As ExportCache, typeName As String, cabinetModelsByName As Dictionary(Of String, SurveyPlan.PoleModel))
      If Not cabinetModelsByName.ContainsKey(typeName) Then
        cabinetModelsByName(typeName) = SurveyPlan.PoleModel.CreateNew(cache.Db.Connection)
        cabinetModelsByName(typeName).Name = typeName
        cabinetModelsByName(typeName).Insert()
      End If
    End Sub

    Private Shared Sub EnsureClosureTypeAdded(cache As ExportCache, typeName As String, cabinetModelsByName As Dictionary(Of String, FiberOpticClosureModel))
      If Not cabinetModelsByName.ContainsKey(typeName) Then
        cabinetModelsByName(typeName) = FiberOpticClosureModel.CreateNew(cache.Db.Connection)
        cabinetModelsByName(typeName).Name = typeName
        cabinetModelsByName(typeName).Insert()
      End If
    End Sub

    Private Shared Sub EnsureTerminatorTypeAdded(cache As ExportCache, typeName As String, cabinetModelsByName As Dictionary(Of String, FiberOpticTerminatorModel))
      If Not cabinetModelsByName.ContainsKey(typeName) Then
        cabinetModelsByName(typeName) = FiberOpticTerminatorModel.CreateNew(cache.Db.Connection)
        cabinetModelsByName(typeName).Name = typeName
        cabinetModelsByName(typeName).Insert()
      End If
    End Sub

    Private Shared Async Function CreateFODevices(cache As ExportCache, processedNodes As HashSet(Of PlanningNode), netData As NetData, token As CancellationToken) As Task(Of NetData)
      Dim closureModels = Await FiberOpticClosureModel.GetCache(cache)
      Dim closureModelsByName = closureModels.Values.Where(Function(n) Not String.IsNullOrEmpty(n.Name)).ToDictionary(Function(n) n.Name)
      EnsureClosureTypeAdded(cache, "NVT-Schrank", closureModelsByName)
      EnsureClosureTypeAdded(cache, "NVT-Schacht", closureModelsByName)
      EnsureClosureTypeAdded(cache, "Mast-Box", closureModelsByName)
      EnsureClosureTypeAdded(cache, "Muffe", closureModelsByName)
      EnsureClosureTypeAdded(cache, "KVZ", closureModelsByName)
      EnsureClosureTypeAdded(cache, "Muffe erdverlegt", closureModelsByName)
      Dim terminatorModels = Await FiberOpticTerminatorModel.GetCache(cache)
      Dim terminatorModelsByName = terminatorModels.Values.Where(Function(n) Not String.IsNullOrEmpty(n.Name)).ToDictionary(Function(n) n.Name)
      EnsureTerminatorTypeAdded(cache, "HVT-Schrank", terminatorModelsByName)
      EnsureTerminatorTypeAdded(cache, "MSAN", terminatorModelsByName)
      EnsureTerminatorTypeAdded(cache, "Gf-AP", terminatorModelsByName)

      For Each deviceNode In processedNodes
        Select Case deviceNode.Type
          Case NodeType.NVTCabinet, NodeType.NVTManhole, NodeType.PoleClosure, NodeType.ManholeClosure, NodeType.CabinetClosure, NodeType.Closure
            Dim nvtClosure = FiberOpticClosure.CreateNew(cache.Db.Connection)
            nvtClosure.Name = deviceNode.Name
            nvtClosure.Info = deviceNode.Info
            nvtClosure.Geometry = deviceNode.Geometry
            If netData.Cabinets.ContainsKey(deviceNode) Then
              nvtClosure.StructuralPointFID = netData.Cabinets(deviceNode).FIDGeom
            ElseIf netData.Manholes.ContainsKey(deviceNode) Then
              nvtClosure.StructuralPointFID = netData.Manholes(deviceNode).FIDGeom
            ElseIf netData.Poles.ContainsKey(deviceNode) Then
              nvtClosure.StructuralPointFID = netData.Poles(deviceNode).FIDGeom
            End If
            netData.Closures.Add(deviceNode, nvtClosure)
            Select Case deviceNode.Type
              Case NodeType.NVTCabinet
                nvtClosure.ModelFID = closureModelsByName("NVT-Schrank").FID
              Case NodeType.NVTManhole
                nvtClosure.ModelFID = closureModelsByName("NVT-Schacht").FID
              Case NodeType.PoleClosure
                nvtClosure.ModelFID = closureModelsByName("Mast-Box").FID
              Case NodeType.ManholeClosure
                nvtClosure.ModelFID = closureModelsByName("Muffe").FID
              Case NodeType.CabinetClosure
                nvtClosure.ModelFID = closureModelsByName("KVZ").FID
              Case NodeType.Closure
                nvtClosure.ModelFID = closureModelsByName("Muffe erdverlegt").FID
            End Select
          Case NodeType.HVTCabinet, NodeType.Building, NodeType.MSANCabinet
            Dim popTerminator = FiberOpticTerminator.CreateNew(cache.Db.Connection)
            popTerminator.Name = deviceNode.Name
            popTerminator.Info = deviceNode.Info
            popTerminator.Geometry = deviceNode.Geometry
            If netData.Cabinets.ContainsKey(deviceNode) Then
              popTerminator.StructuralPointFID = netData.Cabinets(deviceNode).FIDGeom
            ElseIf netData.Manholes.ContainsKey(deviceNode) Then
              popTerminator.StructuralPointFID = netData.Manholes(deviceNode).FIDGeom
            ElseIf netData.Poles.ContainsKey(deviceNode) Then
              popTerminator.StructuralPointFID = netData.Poles(deviceNode).FIDGeom
            End If
            popTerminator.BuildingFID = deviceNode.Building?.FID
            netData.Terminators.Add(deviceNode, popTerminator)
            Select Case deviceNode.Type
              Case NodeType.HVTCabinet
                popTerminator.ModelFID = terminatorModelsByName("HVT-Schrank").FID
              Case NodeType.Building
                popTerminator.ModelFID = terminatorModelsByName("Gf-AP").FID
              Case NodeType.MSANCabinet
                popTerminator.ModelFID = terminatorModelsByName("MSAN").FID
            End Select
        End Select
      Next

      Return netData
    End Function

#Region "Helper functions"
    Private Function PickCableModel(netData As NetData, requiredFiberCount As Long, layerType As DTAGLayerType) As FiberOpticCableModel
      Dim lastModelFound As FiberOpticCableModel = Nothing
      For Each cableModel In netData.CableModelsByName.Values.OrderBy(Function(cm) cm.FiberCount.GetValueOrDefault(0))
        If Not cableModel.IsActiveModel Then Continue For
        If cableModel.Feature.Attributes.Contains("ID_LAYER_TYPE") Then
          Dim idLayerType = cableModel.Feature.Attributes("ID_LAYER_TYPE").ValueNullableLong
          If Not idLayerType.HasValue OrElse idLayerType <> layerType Then Continue For
        End If
        If cableModel.FiberCount.GetValueOrDefault(0) > requiredFiberCount Then Return cableModel
        lastModelFound = cableModel 'Weitersuchen, das ist zu klein
      Next
      Return lastModelFound 'netData.CableModelsByName.Values.OrderBy(Function(cm) cm.FiberCount.GetValueOrDefault(0)).LastOrDefault
    End Function

    Private Function CalculateFiberCount(node As PlanningNode, netData As NetData) As Long
      If node.Building IsNot Nothing Then
        Return node.Building.NumberOfHouseholds.GetValueOrDefault(0) * 4 +
       node.Building.NumberOfBusinesses.GetValueOrDefault(0) * 4 +
       node.Building.Feature.Attributes("NUMBER_SCHOOLS").ValueNullableLong.GetValueOrDefault(0) * 4 +
       node.Building.Feature.Attributes("NUMBER_PUBLICS").ValueNullableLong.GetValueOrDefault(0) * 4 +
       node.Building.Feature.Attributes("NUMBER_HOSPITALS").ValueNullableLong.GetValueOrDefault(0) * 4 + 2
      Else 'Closure
        'TODO: Berechnung einfügen!
        Dim device As FiberOpticDevice = Nothing
        If netData.Closures.ContainsKey(node) Then
          device = netData.Closures(node)
        ElseIf netData.Terminators.ContainsKey(node) Then
          device = netData.Terminators(node)
        End If
        If device Is Nothing Then Return 0

        Dim outgoingSplitterSplices = netData.ClosureOutSplitterSplices.Where(Function(s) s.Device.FID = device.FID).Count
        Dim inSplitterFibers = CLng(Math.Ceiling(outgoingSplitterSplices / 32))
        Return node.SpareFibers + inSplitterFibers + netData.DeviceOutFeederSplices.Where(Function(s) s.Device.FID = device.FID).Count
      End If
    End Function

    Private Shared Function ProcessLines(tolerance As Double, processedNodes As HashSet(Of PlanningNode), lineFeatures As List(Of Feature)) As (PlanningEdges As HashSet(Of PlanningEdge), Errors As IList(Of DTAGError))
      Dim processedLines = New HashSet(Of PlanningEdge)
      Dim remainingLineFeatures = New HashSet(Of Feature)(lineFeatures)
      Dim errors = New List(Of DTAGError)

      While remainingLineFeatures.Any
        Dim nextLine = remainingLineFeatures.First
        remainingLineFeatures.Remove(nextLine)
        Dim lineGeom = TryCast(nextLine.Geometry, Autodesk.Map.IM.Graphic.LineString)
        If lineGeom Is Nothing Then Continue While

        Dim segments = New List(Of PlanningEdge)
        segments.Add(New PlanningEdge() With {.Feature = nextLine, .Geometry = lineGeom})
        Try
          Dim pred = lineGeom.Bounds.AnyInteract2DPredicate(tolerance)
          For Each feat In remainingLineFeatures
            Dim line = TryCast(feat.Geometry, LineString)
            If line Is Nothing Then Continue For
            Try
              If pred(line) Then
                Dim changes = True
                While changes
                  changes = False
                  For i = 0 To segments.Count - 1
                    Dim interactions = segments(i).Geometry.ToLine2D.FindInteractions(line.ToLine2D, Autodesk.Map.IM.PlaneGeometry.Tolerance.CreateFromSDOTolerance(tolerance))
                    If Not interactions.Any Then Continue For
                    Dim firstInteract = interactions.First
                    Dim interactionResult = DivideLine(firstInteract, segments(i).Geometry, line, tolerance)
                    If interactionResult.Success Then
                      segments(i).Geometry = interactionResult.FirstLine
                      segments.Insert(i + 1, segments(i).Clone)
                      segments(i + 1).Geometry = interactionResult.SecondLine
                      changes = True
                      Exit For
                    End If
                  Next
                End While
              End If
            Catch ex As Exception
              errors.Add(New DTAGError("Fehler bei Linien-Geometrieerzeugung", feat.FeatureClass.Name, feat.FID))
            End Try
          Next

          For Each line In processedLines
            If pred(line.Geometry) Then
              Dim changes = True
              While changes
                changes = False
                For i = 0 To segments.Count - 1
                  Dim interactions = segments(i).Geometry.ToLine2D.FindInteractions(line.Geometry.ToLine2D, Autodesk.Map.IM.PlaneGeometry.Tolerance.CreateFromSDOTolerance(tolerance))
                  If Not interactions.Any Then Continue For
                  Dim firstInteract = interactions.First
                  Dim interactionResult = DivideLine(firstInteract, segments(i).Geometry, line.Geometry, tolerance)
                  If interactionResult.Success Then
                    segments(i).Geometry = interactionResult.FirstLine
                    segments.Insert(i + 1, segments(i).Clone)
                    segments(i + 1).Geometry = interactionResult.SecondLine
                    changes = True
                    Exit For
                  End If
                Next
              End While
            End If
          Next

          For Each node In processedNodes
            If pred(node.Geometry) Then
              Dim changes = True
              While changes
                changes = False
                For i = 0 To segments.Count - 1
                  Dim interactionResult = DivideLine(node.Geometry, segments(i).Geometry, tolerance)
                  If interactionResult.Success Then
                    segments(i).Geometry = interactionResult.FirstLine
                    segments.Insert(i + 1, segments(i).Clone)
                    segments(i + 1).Geometry = interactionResult.SecondLine
                    changes = True
                    Exit For
                  End If
                Next
              End While
            End If
          Next

          For Each seg In segments
            processedLines.Add(seg)
          Next
        Catch ex As Exception
          errors.Add(New DTAGError("Fehler bei Linien-Geometrieerzeugung", nextLine.FeatureClass.Name, nextLine.FID))
        End Try

      End While

      Return (processedLines, errors)
    End Function

    Private Shared Async Function ProcessNodes(pointFeatures As List(Of Feature), cache As ExportCache, netData As NetData) As Task(Of (PlanningNodes As HashSet(Of PlanningNode), Errors As IList(Of DTAGError)))
      Dim processedNodes = New HashSet(Of PlanningNode)
      Dim clusters = Await Cluster.GetCache(cache)
      Dim buildings = Await Topography.Building.GetCache(cache)
      Dim errors = New List(Of DTAGError)

      For Each feat In pointFeatures
        Dim name As String = String.Empty
        If feat.Attributes.Contains("NAME") Then name = feat.Attributes("NAME").ValueString
        Dim info As String = String.Empty
        If feat.Attributes.Contains("INFO") Then info = feat.Attributes("INFO").ValueString
        Dim incomingCableTypes = New Queue(Of FiberOpticCableModel)
        If feat.Attributes.Contains("CABLE_ASSIGNMENT") Then
          Dim currentString = feat.Attributes("CABLE_ASSIGNMENT").ValueString
          While Not String.IsNullOrEmpty(currentString)
            Select Case currentString(0)
              Case ";"c
                currentString = currentString.Substring(1)
                Continue While
              Case Else
                Dim maxLength = currentString.Length
                Dim index = currentString.IndexOfAny({";"c})
                If index <> -1 AndAlso index < maxLength Then
                  maxLength = index
                End If
                Dim potentialCableModel = currentString.Substring(0, maxLength).Trim
                If netData.CableModelsByName.ContainsKey(potentialCableModel) Then
                  incomingCableTypes.Enqueue(netData.CableModelsByName(potentialCableModel))
                Else
                  errors.Add(New DTAGError("Der Kabeltyp '" & currentString & "' konnte nicht gefunden werden.", feat.FeatureClass.Name, feat.FID))
                End If
                currentString = currentString.Substring(maxLength)
            End Select
          End While
        End If
        Dim polygon As Polygon = Nothing
        If feat.Attributes.Contains("FID_CLUSTER") Then
          Dim cluster As Cluster = Nothing
          If clusters.TryGetValue(feat.Attributes("FID_CLUSTER").ValueNullableLong.GetValueOrDefault(-1), cluster) Then
            polygon = TryCast(cluster.Geometry, Polygon)
          End If
        End If
        Dim type = NodeType.Branch
        Dim structureType = feat.Attributes("ID_STRUCTURE_TYPE").ValueNullableLong
        If structureType.HasValue Then
          Select Case structureType.Value
            Case 1 'NVT-Schrank
              type = NodeType.NVTCabinet
            Case 2 'POP-Schrank
              type = NodeType.HVTCabinet
            Case 3 'Schacht
              type = NodeType.Handhole
            Case 4 'NVT-Schacht
              type = NodeType.NVTManhole
            Case 5 'Mast
              type = NodeType.Pole
            Case 6 'Muffe
              type = NodeType.ManholeClosure
            Case 7 'MSAN
              type = NodeType.MSANCabinet
            Case 8 'KVZ
              type = NodeType.CabinetClosure
            Case 9 'Muffe erdverlegt
              type = NodeType.Closure
            Case 10 'Mast-Box
              type = NodeType.PoleClosure
          End Select
        End If

        Dim spareFibers = 0L
        If feat.Attributes.Contains("SPARE_FIBERS") Then
          spareFibers = feat.Attributes("SPARE_FIBERS").ValueNullableLong.GetValueOrDefault(0)
        End If

        processedNodes.Add(New PlanningNode() With {.Feature = feat, .Geometry = DirectCast(feat.Geometry, Point), .Type = type, .Name = name, .Info = info, .Cluster = polygon, .SpareFibers = spareFibers, .IncomingCableTypes = incomingCableTypes})
      Next

      For Each building In buildings.Values
        Dim geom = TryCast(building.Geometry, Autodesk.Map.IM.Graphic.Point)
        If geom Is Nothing Then Continue For
        processedNodes.Add(New PlanningNode() With {.Feature = building.Feature, .Geometry = geom, .Type = NodeType.Building, .Building = building, .Name = building.Name, .Info = building.Info, .IncomingCableTypes = New Queue(Of FiberOpticCableModel)})
      Next

      Return (processedNodes, errors)
    End Function

    Private Async Function CreateBuildingConnection(db As DatabaseWorker,
                                         netData As NetData,
                                         nvt As FiberOpticClosure,
                                         buildingNode As PlanningNode,
                                         cableModel As FiberOpticCableModel,
                                         usedFibers As Long,
                                         cableNumber As Integer,
                                         layerType As DTAGLayerType,
                                         token As CancellationToken) As Task(Of IList(Of DTAGError))
      Dim errors = New List(Of DTAGError)
      Dim cable = Await db.ExecuteOnDatabase(Function() FiberOpticCable.CreateNew(db.Connection), token)
      cable.Name = cableModel?.Name
      If cableModel IsNot Nothing Then
        cable.ModelFID = cableModel.FID
      End If
      netData.Cables.Add(cable)

      Dim tracingResult = Await TracePathToNextNode(netData, cable, buildingNode, layerType, {}, db, token)
      Dim currentNode = tracingResult.CurrentNode
      errors.AddRange(tracingResult.Errors)

      Dim terminator = netData.Terminators(buildingNode)
      netData.CableLinking.Add((cable, terminator, True))
      netData.CableLinking.Add((cable, nvt, False))

      Dim demandSplices = New List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))
      Dim nvtSplices = New List(Of (Device As FiberOpticDevice, Cable As FiberOpticCable, Number As Integer, FiberOpticTray As FiberOpticTray))

      Dim currentSpliceNumber = 1
      While currentSpliceNumber < usedFibers
        Dim demandTray = Await db.ExecuteOnDatabase(Function() FiberOpticTray.CreateNew(netData.Connection), token)
        demandTray.ModelFID = netData.TrayModel?.FID
        demandTray.IdClosureSide = FiberOpticClosureSide.UNKNOWN
        demandTray.PointFID = terminator.FIDGeom
        demandTray.Number = netData.Trays.Where(Function(t) t.PointFID.GetValueOrDefault(-1) = terminator.FIDGeom.Value).Count + 1

        Dim nvtTray = Await db.ExecuteOnDatabase(Function() FiberOpticTray.CreateNew(netData.Connection), token)
        nvtTray.ModelFID = netData.TrayModel?.FID
        nvtTray.IdClosureSide = FiberOpticClosureSide.UNKNOWN
        nvtTray.PointFID = nvt.FIDGeom
        nvtTray.Number = netData.Trays.Where(Function(t) t.PointFID.GetValueOrDefault(-1) = nvt.FIDGeom.Value).Count + 1

        Dim remainingTraySpace = 12

        For i = currentSpliceNumber To CInt(usedFibers)
          If remainingTraySpace = 0 Then Exit For
          remainingTraySpace -= 1
          currentSpliceNumber += 1

          demandSplices.Add((terminator, cable, i, demandTray))
          If (i = 1 AndAlso cableNumber = 1) OrElse i Mod 4 = 3 Then
            nvtSplices.Add((nvt, cable, i, nvtTray))
          End If
        Next
        netData.DemandInSplices.AddRange(demandSplices)
        netData.ClosureOutSplitterSplices.AddRange(nvtSplices)

        netData.Trays.Add(demandTray)
        netData.Trays.Add(nvtTray)
      End While

      Return errors
    End Function

    Private Async Function CreatePopConnection(db As DatabaseWorker,
                                         netData As NetData,
                                         deviceNode As PlanningNode,
                                         cableModel As FiberOpticCableModel,
                                         layerType As DTAGLayerType,
                                         token As CancellationToken) As Task(Of (ErrorsInConnection As IList(Of DTAGError), StartDevice As FiberOpticDevice, Cable As FiberOpticCable))
      Dim errors = New List(Of DTAGError)
      Dim cable = Await db.ExecuteOnDatabase(Function() FiberOpticCable.CreateNew(db.Connection), token)
      cable.Name = cableModel?.Name
      If cableModel IsNot Nothing Then
        cable.ModelFID = cableModel.FID
      End If
      netData.Cables.Add(cable)

      Dim tracingResult = Await TracePathToNextNode(netData, cable, deviceNode, layerType, DistributionTypes, db, token)
      Dim startNode = tracingResult.CurrentNode
      errors.AddRange(tracingResult.Errors)

      Dim endDevice As FiberOpticDevice = Nothing
      If netData.Terminators.ContainsKey(deviceNode) Then
        endDevice = netData.Terminators(deviceNode)
      ElseIf netData.Closures.ContainsKey(deviceNode) Then
        endDevice = netData.Closures(deviceNode)
      End If
      If endDevice IsNot Nothing Then netData.CableLinking.Add((cable, endDevice, True))

      'Unterscheidung POP / Muffe:
      Dim startDevice As FiberOpticDevice = Nothing
      If netData.Terminators.ContainsKey(startNode) Then
        startDevice = netData.Terminators(startNode)
      ElseIf netData.Closures.ContainsKey(startNode) Then
        startDevice = netData.Closures(startNode)
      End If
      If startDevice IsNot Nothing Then netData.CableLinking.Add((cable, startDevice, False))

      Return (errors, startDevice, cable)
    End Function


    Private Async Function TracePathToNextNode(netData As NetData, cable As FiberOpticCable, startNode As PlanningNode, layerType As DTAGLayerType, stopNodeTypes As IEnumerable(Of NodeType), db As DatabaseWorker, token As CancellationToken) As Task(Of (CurrentNode As PlanningNode, Errors As IList(Of DTAGError)))
      Dim microDuctsForCable = New List(Of DuctInfrastructure.Duct)
      Dim currentMicroDuct As DuctInfrastructure.Duct = Nothing
      Dim errors As New List(Of DTAGError)

      Dim currentnode = startNode
      While currentnode.Previous IsNot Nothing AndAlso Not IsStopNode(currentnode, startNode, stopNodeTypes)
        Dim currentEdge = currentnode.Previous
        Dim segment = Await TryCreateSegmentFromEdge(netData, db, currentEdge, token)

        If String.IsNullOrEmpty(currentEdge.Content) Then
          Dim segmentCable = Await db.ExecuteOnDatabase(Function() SurveyPlan.SegmentCable.CreateNew(db.Connection), token)
          segmentCable.SetCable(cable)
          segmentCable.SetSegment(segment)
          netData.SegmentCables.Add(segmentCable)
        Else
          If currentEdge.Next.Next.Count = 1 AndAlso currentEdge.Next.Type = NodeType.Branch AndAlso currentEdge.Content.Equals(currentEdge.Next.Next.First.Content) Then
            If Not netData.DirectDuctsByEdge.ContainsKey(currentEdge) Then
              netData.DirectDuctsByEdge(currentEdge) = netData.DirectDuctsByEdge(currentEdge.Next.Next.First)
              For Each duct In netData.DirectDuctsByEdge(currentEdge)
                Dim segmentDuct = Await db.ExecuteOnDatabase(Function() SurveyPlan.SegmentDuct.CreateNew(db.Connection), token)
                segmentDuct.SetDuct(duct)
                segmentDuct.SetSegment(segment)
                netData.SegmentDucts.Add(segmentDuct)
              Next
            End If
          Else
            Dim newErrors = Await TryParseDuctsFromEdge(netData, db, currentEdge, segment, token)
            errors.AddRange(newErrors)

            If Not currentEdge.MicroDucts(layerType).Any AndAlso Not currentEdge.MicroDucts(DTAGLayerType.All).Any Then
              Dim segmentCable = Await db.ExecuteOnDatabase(Function() SurveyPlan.SegmentCable.CreateNew(db.Connection), token)
              segmentCable.SetCable(cable)
              segmentCable.SetSegment(segment)
              netData.SegmentCables.Add(segmentCable)
            Else
              Dim microDuct = If(currentEdge.MicroDucts(layerType).Any, currentEdge.MicroDucts(layerType).Dequeue, currentEdge.MicroDucts(DTAGLayerType.All).Dequeue)
              While microDuct.HasInnerDucts AndAlso (currentEdge.MicroDucts(layerType).Any OrElse currentEdge.MicroDucts(DTAGLayerType.All).Any)
                microDuct = If(currentEdge.MicroDucts(layerType).Any, currentEdge.MicroDucts(layerType).Dequeue, currentEdge.MicroDucts(DTAGLayerType.All).Dequeue)
              End While
              If Not microDuctsForCable.Contains(microDuct) Then
                microDuctsForCable.Add(microDuct)
                If currentMicroDuct Is Nothing Then
                  If startNode.Building IsNot Nothing Then
                    Await db.ExecuteOnDatabase(Function() microDuct.AddDuctInsertion(startNode.Geometry, startNode.Building), token)
                  Else
                    Dim startStruct As SurveyPlan.StructuralNode = Nothing
                    If netData.Cabinets.ContainsKey(startNode) Then
                      startStruct = netData.Cabinets(startNode)
                    ElseIf netData.Manholes.ContainsKey(startNode) Then
                      startStruct = netData.Manholes(startNode)
                    ElseIf netData.Poles.ContainsKey(startNode) Then
                      startStruct = netData.Poles(startNode)
                    End If
                    If startStruct IsNot Nothing Then
                      Await db.ExecuteOnDatabase(Function() microDuct.AddDuctInsertion(startNode.Geometry, startStruct.FIDGeom), token)
                    End If
                  End If
                Else
                  Try
                    Await db.ExecuteOnDatabase(Function() microDuct.AddFitting(currentMicroDuct, currentEdge.Geometry.EndPoint), token)
                  Catch ex As Exception
                    errors.Add(New DTAGError($"Fehler beim Erzeugen eines Fittings. Aufgetreten in Segment: {currentEdge.Feature.FID.ToString}", currentEdge.Feature.FeatureClass.Name, currentEdge.Feature.FID))
                  End Try
                End If
                currentMicroDuct = microDuct
              End If
            End If

          End If

        End If

        currentnode = currentnode.Previous.Previous
      End While

      If microDuctsForCable.Any Then
        Dim ductCable = Await db.ExecuteOnDatabase(Function() DuctInfrastructure.DuctCable.CreateNew(db.Connection), token)
        ductCable.SetDuct(microDuctsForCable.First)
        ductCable.SetCable(cable)
        netData.DuctCables.Add(ductCable)

      End If

      Dim struct As SurveyPlan.StructuralNode = Nothing
      If netData.Cabinets.ContainsKey(currentnode) Then
        struct = netData.Cabinets(currentnode)
      ElseIf netData.Manholes.ContainsKey(currentnode) Then
        struct = netData.Manholes(currentnode)
      ElseIf netData.Poles.ContainsKey(currentnode) Then
        struct = netData.Poles(currentnode)
      End If
      If currentMicroDuct IsNot Nothing Then
        Await db.ExecuteOnDatabase(Function() currentMicroDuct.AddDuctInsertion(currentnode.Geometry, If(struct IsNot Nothing, struct.FIDGeom, Nothing)), token)
      End If

      Return (currentnode, errors)
    End Function

    Private Async Function TryParseDuctsFromEdge(netData As NetData, db As DatabaseWorker, currentEdge As PlanningEdge, segment As SurveyPlan.Segment, token As CancellationToken) As Task(Of IList(Of DTAGError))
      Dim errors = New List(Of DTAGError)
      If Not netData.DirectDuctsByEdge.ContainsKey(currentEdge) Then
        Dim ductsAndMicroDucts = Await ParseContent(db, currentEdge.Content, netData.DuctModelsByName, token)
        For Each dtagError In ductsAndMicroDucts.Errors
          dtagError.TableName = currentEdge.Feature.FeatureClass.Name
          dtagError.FID = currentEdge.Feature.FID
          errors.Add(dtagError)
        Next
        For Each duct In ductsAndMicroDucts.OuterDucts
          Dim segmentDuct = Await db.ExecuteOnDatabase(Function() SurveyPlan.SegmentDuct.CreateNew(db.Connection), token)
          segmentDuct.SetDuct(duct)
          segmentDuct.SetSegment(segment)
          netData.SegmentDucts.Add(segmentDuct)
        Next
        netData.DirectDuctsByEdge(currentEdge) = ductsAndMicroDucts.OuterDucts.ToList
        For Each kvp In ductsAndMicroDucts.MicroDucts
          currentEdge.MicroDucts(kvp.Key) = New Queue(Of DuctInfrastructure.Duct)(kvp.Value)
        Next
      End If
      Return errors
    End Function

    Private Shared Async Function TryCreateSegmentFromEdge(netData As NetData, db As DatabaseWorker, currentEdge As PlanningEdge, token As CancellationToken) As Task(Of SurveyPlan.Segment)
      If Not netData.SegmentsByEdge.ContainsKey(currentEdge) Then
        Dim newsegment = Await db.ExecuteOnDatabase(Function() SurveyPlan.Segment.CreateNew(db.Connection), token)
        newsegment.ModelFID = currentEdge.SegmentModelFid
        netData.SegmentsByEdge(currentEdge) = newsegment
        newsegment.Geometry = currentEdge.Geometry
        newsegment.Info = currentEdge.Info
      End If

      Return netData.SegmentsByEdge(currentEdge)
    End Function

    Private Function IsStopNode(currentnode As PlanningNode, startNode As PlanningNode, stopNodeTypes As IEnumerable(Of NodeType)) As Boolean
      If Not stopNodeTypes.Any Then Return False
      If currentnode.Equals(startNode) Then Return False
      If stopNodeTypes.Contains(currentnode.Type) Then Return True

      Return False
    End Function

    Private Async Function ParseContent(db As DatabaseWorker, content As String, existingDuctModelsByName As IDictionary(Of String, DuctInfrastructure.DuctType), token As CancellationToken) As Task(Of (OuterDucts As IList(Of DuctInfrastructure.Duct), MicroDucts As IDictionary(Of Long, IList(Of DuctInfrastructure.Duct)), Errors As IList(Of DTAGError)))
      Dim errors = New List(Of DTAGError)
      Dim result = New List(Of DuctInfrastructure.Duct)
      Dim currentStack As New Stack(Of DuctInfrastructure.Duct)
      Dim currentDuct As DuctInfrastructure.Duct = Nothing
      Dim microDucts = New Dictionary(Of Long, IList(Of DuctInfrastructure.Duct))
      microDucts(DTAGLayerType.All) = New List(Of DuctInfrastructure.Duct)
      microDucts(DTAGLayerType.Backbone) = New List(Of DuctInfrastructure.Duct)
      microDucts(DTAGLayerType.Feeder) = New List(Of DuctInfrastructure.Duct)
      microDucts(DTAGLayerType.Distribution) = New List(Of DuctInfrastructure.Duct)
      microDucts(DTAGLayerType.Drop) = New List(Of DuctInfrastructure.Duct)

      Dim currentString = content
      While Not String.IsNullOrEmpty(currentString)
        Select Case currentString(0)
          Case ";"c
            currentString = currentString.Substring(1)
            Continue While
          Case "("c
            currentStack.Push(currentDuct)
            currentString = currentString.Substring(1)
            Continue While
          Case ")"c
            If Not currentStack.Any Then Return (result, microDucts, errors)
            currentStack.Pop()
            currentString = currentString.Substring(1)
            Continue While
          Case Else
            Dim maxLength = currentString.Length
            Dim index = currentString.IndexOfAny({";"c, "("c, ")"c})
            If index <> -1 AndAlso index < maxLength Then
              maxLength = index
            End If
            Dim potentialDuctType = currentString.Substring(0, maxLength)
            If existingDuctModelsByName.ContainsKey(potentialDuctType) Then
              Dim duct = Await db.ExecuteOnDatabase(Function() DuctInfrastructure.Duct.CreateNew(db.Connection), token)
              duct.ModelFID = existingDuctModelsByName(potentialDuctType).FID
              Await db.ExecuteOnDatabase(Function() duct.Insert(), token)
              currentDuct = duct
              Dim idLayerType As Long? = 0
              If existingDuctModelsByName(potentialDuctType).Feature.Attributes.Contains("ID_LAYER_TYPE") Then
                idLayerType = existingDuctModelsByName(potentialDuctType).Feature.Attributes("ID_LAYER_TYPE").ValueNullableLong.GetValueOrDefault(0)
              End If
              If duct.HasInnerDucts Then
                If idLayerType.HasValue Then
                  If microDucts(idLayerType.Value).Contains(duct) Then
                    microDucts(idLayerType.Value).Remove(duct)
                  End If
                  For Each subduct In Await db.ExecuteOnDatabase(Function() duct.GetInnerDucts, token)
                    microDucts(idLayerType.Value).Add(subduct)
                  Next
                End If
              Else
                If idLayerType.HasValue Then
                  microDucts(idLayerType.Value).Add(duct)
                End If
              End If
              If currentStack.Any Then
                Dim outerDuct = currentStack.Peek

                If Not PositionByOuterDuctFid.ContainsKey(outerDuct.FID) Then
                  PositionByOuterDuctFid(outerDuct.FID) = 0
                End If
                PositionByOuterDuctFid(outerDuct.FID) += 1

                Dim ductDuct = Await db.ExecuteOnDatabase(Function() DuctInfrastructure.DuctDuct.CreateNew(db.Connection), token)
                ductDuct.SetOuterDuct(outerDuct)
                ductDuct.SetInnerDuct(duct)
                ductDuct.PositionNumber = PositionByOuterDuctFid(outerDuct.FID)
                ductDuct.Insert()
              Else
                result.Add(duct)
              End If
            Else
              errors.Add(New DTAGError("Der Rohrtyp '" & currentString & "' konnte nicht gefunden werden. Das Rohr kann nicht angelegt werden", String.Empty, Nothing))
            End If
            currentString = currentString.Substring(maxLength)
        End Select

      End While

      Return (result, microDucts, errors)
    End Function

    Private Function CreateNetwork(rootNode As PlanningNode, processedNodes As HashSet(Of PlanningNode), processedLines As HashSet(Of PlanningEdge), tolerance As Double) As (RemainingNodes As HashSet(Of PlanningNode), RemainingLines As HashSet(Of PlanningEdge))
      For Each node In processedNodes
        node.Previous = Nothing
        node.Next = New List(Of PlanningEdge)
      Next
      For Each edge In processedLines
        edge.Previous = Nothing
        edge.Next = Nothing
      Next
      Return CreateNetworkInternal(rootNode, 0.0, processedNodes, processedLines, tolerance)
    End Function
    Private Function CreateNetworkInternal(rootNode As PlanningNode, length As Double, processedNodes As HashSet(Of PlanningNode), processedLines As HashSet(Of PlanningEdge), tolerance As Double) As (RemainingNodes As HashSet(Of PlanningNode), RemainingLines As HashSet(Of PlanningEdge))
      Dim currentNodes = New HashSet(Of PlanningNode)(processedNodes)
      Dim currentLines = New HashSet(Of PlanningEdge)(processedLines)

      currentNodes.Remove(rootNode)
      rootNode.TotalLength = length

      Dim linesToProcess = New HashSet(Of PlanningEdge)
      Dim distanceToRoot = rootNode.Geometry.Distance2DFunction()
      For Each line In currentLines
        If distanceToRoot(line.Geometry.StartPoint) < tolerance Then
          linesToProcess.Add(line)
        End If
        If distanceToRoot(line.Geometry.EndPoint) < tolerance Then
          line.Geometry.Reverse()
          linesToProcess.Add(line)
        End If
      Next

      For Each line In linesToProcess
        currentLines.Remove(line)
      Next
      For Each line In linesToProcess
        Dim endNode = New PlanningNode() With {.Geometry = line.Geometry.EndPoint, .Type = NodeType.Branch}
        For Each node In currentNodes
          If endNode.Geometry.Distance2D(node.Geometry) < tolerance Then
            endNode = node
            currentNodes.Remove(node)
            Exit For
          End If
        Next
        line.Previous = rootNode
        rootNode.Next.Add(line)
        line.Next = endNode
        endNode.Previous = line
        line.TotalLength = length + line.Geometry.Length
        Dim newNodesAndLines = CreateNetworkInternal(endNode, line.TotalLength, currentNodes, currentLines, tolerance)
        currentNodes = newNodesAndLines.RemainingNodes
        currentLines = newNodesAndLines.RemainingLines
      Next
      Return (currentNodes, currentLines)
    End Function


#End Region

  End Class

End Namespace
