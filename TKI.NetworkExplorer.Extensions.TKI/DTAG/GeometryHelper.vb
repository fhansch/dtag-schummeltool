Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.PlaneGeometry

Namespace DTAG
  Friend Module GeometryHelper
    Friend Function DivideLine(firstInteract As LineSegmentInteraction, line As LineString, interactLine As LineString, tolerance As Double) As (Success As Boolean, FirstLine As LineString, SecondLine As LineString)
      If firstInteract.Location.DistanceTo(line.StartPoint.ToPoint2D) <= tolerance OrElse firstInteract.Location.DistanceTo(line.EndPoint.ToPoint2D) <= tolerance Then
        Return (False, Nothing, Nothing)
      End If
      Dim firstLinePoints = New List(Of LinePoint)
      Dim secondLinePoints = New List(Of LinePoint)
      Dim index = 0
      For i = 1 To line.Count - 1
        Dim startPoint = New LinePoint(line(i - 1).X, line(i - 1).Y)
        Dim endPoint = New LinePoint(line(i).X, line(i).Y)
        Dim tempLine = New LineString({startPoint, endPoint})
        firstLinePoints.Add(startPoint)

        Dim interactions = tempLine.ToLine2D.FindInteractions(interactLine.ToLine2D, Autodesk.Map.IM.PlaneGeometry.Tolerance.CreateFromSDOTolerance(tolerance))
        If Not interactions.Any Then
          Continue For
        End If
        Dim intersection = New LinePoint(interactions.First.Location.X, interactions.First.Location.Y)
        If intersection.Distance2D(startPoint) > tolerance Then
          firstLinePoints.Add(intersection)
        End If
        If intersection.Distance2D(endPoint) > tolerance Then
          secondLinePoints.Add(intersection)
        End If
        index = i
        Exit For
      Next

      For i = index To line.Count - 1
        secondLinePoints.Add(New LinePoint(line(i).X, line(i).Y))
      Next
      If Not firstLinePoints.Count > 1 OrElse Not secondLinePoints.Count > 1 Then Return (False, Nothing, Nothing)
      Return (True, New LineString(firstLinePoints), New LineString(secondLinePoints))
    End Function

    Friend Function DivideLine(interaction As Point, line As LineString, tolerance As Double) As (Success As Boolean, FirstLine As LineString, SecondLine As LineString)
      If interaction.ToPoint2D.DistanceTo(line.StartPoint.ToPoint2D) <= tolerance OrElse interaction.ToPoint2D.DistanceTo(line.EndPoint.ToPoint2D) <= tolerance Then
        Return (False, Nothing, Nothing)
      End If
      Dim firstLinePoints = New List(Of LinePoint)
      Dim secondLinePoints = New List(Of LinePoint)
      Dim index = -1
      For i = 1 To line.Count - 1
        Dim startPoint = New LinePoint(line(i - 1).X, line(i - 1).Y)
        Dim endPoint = New LinePoint(line(i).X, line(i).Y)
        Dim tempLine = New LineString({startPoint, endPoint})
        firstLinePoints.Add(startPoint)

        If Not tempLine.Distance2DFunction(interaction) < tolerance Then
          Continue For
        End If
        Dim intersection = New LinePoint(interaction.X, interaction.Y)
        If intersection.Distance2D(startPoint) > tolerance Then
          firstLinePoints.Add(intersection)
        End If
        If intersection.Distance2D(endPoint) > tolerance Then
          secondLinePoints.Add(intersection)
        End If
        index = i
        Exit For
      Next

      If index = -1 Then Return (False, Nothing, Nothing)

      For i = index To line.Count - 1
        secondLinePoints.Add(New LinePoint(line(i).X, line(i).Y))
      Next
      If Not firstLinePoints.Count > 1 OrElse Not secondLinePoints.Count > 1 Then Return (False, Nothing, Nothing)
      Return (True, New LineString(firstLinePoints), New LineString(secondLinePoints))
    End Function

  End Module
End Namespace

