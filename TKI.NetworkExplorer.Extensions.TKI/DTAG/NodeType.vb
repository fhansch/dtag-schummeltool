Namespace DTAG
  Friend Enum NodeType
    Building
    Branch
    Handhole
    HVTCabinet
    NVTManhole
    NVTCabinet
    MSANCabinet
    Closure
    Pole
    ManholeClosure
    CabinetClosure
    PoleClosure
  End Enum
End Namespace
