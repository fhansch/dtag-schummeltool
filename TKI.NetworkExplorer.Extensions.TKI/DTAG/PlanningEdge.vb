Imports Autodesk.Map.IM.Data
Imports TC.Common.API

Namespace DTAG
  Friend Class PlanningEdge
    Implements ICloneable

    Public Property Feature As Feature
    Public Property Geometry As Autodesk.Map.IM.Graphic.LineString
    Public Property TotalLength As Double
    Public Property MicroDucts As New Dictionary(Of Long, Queue(Of DuctInfrastructure.Duct))
    Public Property [Next] As PlanningNode
    Public Property Previous As PlanningNode
    Public ReadOnly Property Content As String
      Get
        If Feature Is Nothing Then Return String.Empty
        If Not Feature.Attributes.Contains("TRENCH_ASSIGNMENT") Then Return String.Empty
        Return Feature.Attributes("TRENCH_ASSIGNMENT").ValueString.Replace(" ", "")
      End Get
    End Property
    Public ReadOnly Property SegmentModelFid As Long?
      Get
        If Feature Is Nothing Then Return Nothing
        If Not Feature.Attributes.Contains("TRENCH_TYPE") Then Return Nothing
        Return Feature.Attributes("TRENCH_TYPE").ValueNullableLong
      End Get
    End Property
    Public ReadOnly Property Info As String
      Get
        If Feature Is Nothing Then Return String.Empty
        If Not Feature.Attributes.Contains("INFO") Then Return String.Empty
        Return Feature.Attributes("INFO").ValueString
      End Get
    End Property

    Public Function Clone() As PlanningEdge
      Return DirectCast(CloneInternal(), PlanningEdge)
    End Function
    Private Function CloneInternal() As Object Implements ICloneable.Clone
      Return New PlanningEdge With {.Feature = Feature,
                                      .Geometry = Geometry,
                                      .MicroDucts = MicroDucts,
                                      .[Next] = [Next],
                                      .Previous = Previous,
                                      .TotalLength = TotalLength}
    End Function
  End Class

End Namespace
