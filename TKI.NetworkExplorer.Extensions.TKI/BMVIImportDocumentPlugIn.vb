Imports System.Threading
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Forms.Events
Imports TKI.NetworkExplorer
Imports TKI.Tools
Imports TKI.Tools.Database
Imports TKI.UI.Configuration

Public Class BMVIImportDocumentPlugIn
  Inherits DocumentPlugIn

  Private WithEvents _mniBMVIImport As New MenuItem()
  Private _bmviDirectory As DirectoryEntry

  Private Sub DocumentPlugIn_InitMenus(sender As Object, e As MenusEventArgs) Handles Me.InitMenus
    Dim documentMenu = e.Menus.Item(DocumentContextMenuType.Document)
    documentMenu.MenuItems.Add(_mniBMVIImport, "Schummeltool: BMVI-Import")
  End Sub

  Private Sub CreateImportJob(status As IStatusDisplay, parameters() As String)
    Dim token = CancellationToken.None
    Using db As New DatabaseWorker(Document.Connection)
      Using burst As New RegenerateLabelDisableBurst(db.Connection)
        Using mapBurst = Document.Map.InitiateDrawBurst()
          Using transaction = db.BeginTransaction(token).Result
            Dim import = New BMVI.Import(db, token)
            import.PerformImport(parameters(0)).Wait()
            Task.WaitAll(transaction.Commit(token))
          End Using
        End Using
      End Using
    End Using

  End Sub
  Private Sub _mniBMVIImport_Click(sender As Object, e As MenuItemClickEventArgs) Handles _mniBMVIImport.Click
    Try
      _bmviDirectory = New DirectoryEntry("Verzeichnis", "Verzeichnis", True)

      Dim dialogControls As New List(Of IConfigurationEntry)({_bmviDirectory})
      If TKI.UI.ShowDialog("BMVI-Import", Branding.EngineeringTkiWindowTheme, dialogControls) Then
        If String.IsNullOrWhiteSpace(_bmviDirectory.Value) Then Throw New Exception("My.Resources.NoKmlDirectorySelected")

        Dim statusForm = New Desktop.StatusForm(Of String)(AddressOf CreateImportJob)
        statusForm.Parameters = {_bmviDirectory.Value}
        statusForm.AbortThreadOnCancel = True
        statusForm.CancelButtonVisible = True
        statusForm.CancelButtonEnabled = True
        statusForm.Status = "Import BMVI Data"
        statusForm.Type = Desktop.ProgressBarType.ProgressBar
        statusForm.Text = ""
        statusForm.Minimum = 0
        statusForm.Maximum = 100
        statusForm.ShowDialog()
      End If
    Catch ex As Exception
      Dim message = ex.Message
      Dim currentEx = ex.InnerException
      While currentEx IsNot Nothing
        message += Environment.NewLine + currentEx.StackTrace
        currentEx = currentEx.InnerException
      End While
      Debug.Fail(message)
      MsgBox(message)
    End Try
  End Sub
End Class
