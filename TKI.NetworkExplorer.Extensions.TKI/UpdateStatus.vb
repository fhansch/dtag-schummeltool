Imports System.Threading
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.NetworkExplorer.Tools.Caching
Imports TKI.Tools
Imports TKI.Tools.Database

Friend Module UpdateStatus
  Friend Sub UpdateStatusJob(status As IStatusDisplay, parameters() As Object)
    If parameters.Length <> 2 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
    If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))
    If TypeOf parameters(1) IsNot Document Then Throw New ArgumentException($"The parameter 2 has the wrong type. Expected: {NameOf(Document)}", NameOf(parameters))

    Dim vCluster = DirectCast(parameters(0), Cluster)
    Dim vDocument = DirectCast(parameters(1), Document)

    Task.WaitAll(RunUpdateStatusJob(status, vCluster, vDocument, CancellationToken.None))
  End Sub

  Private Async Function RunUpdateStatusJob(status As IStatusDisplay, vCluster As Cluster, document As Document, token As CancellationToken) As Task
    Using db As New DatabaseWorker(vCluster.Connection)
      Using burst As New RegenerateLabelDisableBurst(db.Connection)
        Using mapBurst = document.Map.InitiateDrawBurst()
          Using transaction = Await db.BeginTransaction(token)
            Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
            Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)

            status.Status = "Segmente ermitteln..."
            Dim segments = Await Segment.GetCache(cache)
            status.Status = "Status von Segmenten setzen..."
            Await SetStatus(vCluster, db, segments, token)

            status.Status = "Status von Rohren setzen..."
            Dim ducts = Await Duct.GetCache(cache)
            Await SetStatus(vCluster, db, ducts, token)

            status.Status = "Status von Kabeln setzen..."
            Dim cables = Await FiberOpticCable.GetCache(cache)
            Await SetStatus(vCluster, db, cables, token)

            status.Status = "Status von Schränken setzen..."
            Dim cabinets = Await Cabinet.GetCache(cache)
            Await SetStatus(vCluster, db, cabinets, token)

            status.Status = "Status von Schächten setzen..."
            Dim manholes = Await Manhole.GetCache(cache)
            Await SetStatus(vCluster, db, manholes, token)

            status.Status = "Status von Masten setzen..."
            Dim poles = Await Pole.GetCache(cache)
            Await SetStatus(vCluster, db, poles, token)

            status.Status = "Status von LWL-Abschlüssen setzen..."
            Dim terminators = Await FiberOpticTerminator.GetCache(cache)
            Await SetStatus(vCluster, db, terminators, token)

            status.Status = "Status von LWL-Muffen setzen..."
            Dim closures = Await FiberOpticClosure.GetCache(cache)
            Await SetStatus(vCluster, db, closures, token)

            status.Status = "Status von Rohr-Abschlüssen setzen..."
            Dim ductInsertions = Await DuctInsertion.GetCache(cache)
            Await SetStatus(vCluster, db, ductInsertions, token)

            status.Status = "Status von Rohr-Fittingen setzen..."
            Dim ductFittings = Await DuctInfrastructure.DuctFitting.GetCache(cache)
            Await SetStatus(vCluster, db, ductFittings, token)

            status.Status = "Status von Rohr-Knoten setzen..."
            Dim ductTaps = Await DuctTap.GetCache(cache)
            Await SetStatus(vCluster, db, ductTaps, token)

            Await transaction.Commit(token)
          End Using
        End Using
      End Using
    End Using

  End Function

  Private Async Function SetStatus(Of T As UtilityFeatureItem)(vCluster As Cluster, db As DatabaseWorker, utilCache As UtilityBaseCache(Of T), token As CancellationToken) As Task
    Dim updatedFeatures = New HashSet(Of Feature)
    For Each feature In utilCache.Values
      Dim statusFid = feature.GeomFeature.Attributes(Segment.GEOM_ATTR_FID_STATUS).ValueNullableLong
      If vCluster.StatusFID.GetValueOrDefault(-1) = statusFid.GetValueOrDefault(-1) Then Continue For
      feature.GeomFeature.Attributes(Segment.GEOM_ATTR_FID_STATUS).Value = vCluster.StatusFID
      updatedFeatures.Add(feature.GeomFeature)
    Next
    If updatedFeatures.Any Then Await db.ExecuteOnDatabase(Function() updatedFeatures.First.FeatureClass.UpdateFeatures(updatedFeatures, False), token)
  End Function
End Module
