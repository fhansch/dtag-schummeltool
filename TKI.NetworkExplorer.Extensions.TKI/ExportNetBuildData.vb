Imports System.Text
Imports System.Threading
Imports Autodesk.Map.IM.CoordinateSystem.API
Imports Autodesk.Map.IM.CoordinateSystem.Factory
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Graphic
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.Common.API.Topography
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.NetworkExplorer.Caching.Tracing
Imports TKI.Tools
Imports TKI.Tools.Database

Friend Module ExportNetBuildData
  Friend Sub ExportNetBuildDataJob(status As IStatusDisplay, parameters() As Object)
    If parameters.Length <> 2 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
    If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))
    If TypeOf parameters(1) IsNot Document Then Throw New ArgumentException($"The parameter 2 has the wrong type. Expected: {NameOf(Document)}", NameOf(parameters))

    Dim vCluster = DirectCast(parameters(0), Cluster)
    Dim vDocument = DirectCast(parameters(1), Document)

    Task.WaitAll(RunExportNetBuildDataJob(status, vCluster, vDocument, CancellationToken.None))
  End Sub

  Private Async Function RunExportNetBuildDataJob(status As IStatusDisplay, vCluster As Cluster, document As Document, token As CancellationToken) As Task
    Try
      Using db As New DatabaseWorker(vCluster.Connection)
        Using transformation = CreateTransformation(vCluster.Connection)
          Dim buildingFc = db.Connection.FeatureClasses(Building.TABLE_NAME)
          Dim lineFc = db.Connection.FeatureClasses(Duct.GEOM_TABLE_NAME)
          Dim pointFc = db.Connection.FeatureClasses(DuctInsertion.GEOM_TABLE_NAME)

          Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
          Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)
          Dim tracer = cache.CreateTracer()

          Dim buildingsByFid = Await Building.GetCache(cache)
          Dim cabinets = Await Cabinet.GetCache(cache)
          Dim closures = Await FiberOpticClosure.GetCache(cache)
          Dim ductInsertions = Await DuctInsertion.GetCache(cache)
          Dim ductInsertionsByBuilding = ductInsertions.Values.Distinct.Where(Function(di) di.BuildingFID.HasValue).ToLookup(Function(di) di.BuildingFID.Value)
          Dim terminators = Await FiberOpticTerminator.GetCache(cache)
          Dim terminatorsByBuilding = terminators.Values.Distinct.Where(Function(t) t.BuildingFID.HasValue).ToLookup(Function(t) t.BuildingFID.Value)

          For Each building In buildingsByFid
            'for each building in area
            '	find duct termination
            Dim buildingDuctInsertions = ductInsertionsByBuilding(building.Value.FID)
            If Not buildingDuctInsertions.Any Then
              'Bestenfalls ein HP Anschluss. Was machen wir mit dem? TBD
              DebugOutput(building.Value, "Keine Rohreinführung")
              Continue For
            End If

            If buildingDuctInsertions.Count > 2 Then
              'Hier gibt es mehr als 1 Rohreinführung. Was machen wir damit?
              DebugOutput(building.Value, "Mehr als eine Rohreinführung")
              Continue For
            End If

            Dim buildingInsertion As DuctInsertion = buildingDuctInsertions.First
            Dim result = tracer.Trace(buildingInsertion.FIDGeom.Value, Tracing.TopoTracingDirection.Both)
            If Not result.Routes.Any Then
              DebugOutput(building.Value, "Keine Rohrroute gefunden?!")
              Continue For
            End If

            If result.Routes.Count > 1 Then
              DebugOutput(building.Value, "Mehr als eine Rohrroute gefunden?!")
              Continue For
            End If

            Dim ductRoute = result.Routes.First

            Dim oppositeInsertion As DuctInsertion = Nothing
            If Not ductInsertions.TryGetValue(ductRoute.Last, oppositeInsertion) Then
              DebugOutput(building.Value, "Gegenüberliegendes Ende keine Rohreinführung?!")
              Continue For
            End If

            'TODO: Den StructuralNode der Rohreinführung auch aus dem Cache holen
            Dim pointFt = oppositeInsertion.GetPoint
            If pointFt Is Nothing OrElse Not TypeOf pointFt Is Cabinet Then
              DebugOutput(building.Value, "Gegenüberliegendes Ende hat keine Beziehung zu einem Schrank?!")
              Continue For
            End If

            Dim cabinet = CType(pointFt, Cabinet)
            'TODO: Das Device auch aus dem Cache holen
            Dim devices = cabinet.GetDevices()
            If devices.Count <> 1 Then
              DebugOutput(building.Value, "Gegenüberliegendes Ende hat ungleich 1 Device?!")
              Continue For
            End If

            Dim tracingResult = Await tracer.ConvertToNodesAndLines(ductRoute, TopoTracer.LineMode.Segments)

            Dim routeLength = CalculateRouteLength(tracingResult)

            Dim terminatorAtBuilding As FiberOpticTerminator = Nothing
            If terminatorsByBuilding.Contains(building.Value.FID) Then
              terminatorAtBuilding = terminatorsByBuilding(building.Value.FID).First
            End If

            DebugOutput(building.Value, buildingInsertion, terminatorAtBuilding, devices.Single, routeLength, transformation)

            '	trace duct route
            '	start = Distribution point
            '	end = Building
            '	
            '	output attributes
            '		path length
            '		FO terminator exists?
            '		HP/HP+ (is marker available at duct termination)
            '		
          Next
        End Using
      End Using

    Catch ex As Exception
      Throw New Exception("Error exporting NetBuild data", ex)
    End Try
  End Function

  Private Function CalculateRouteLength(routeStartItem As ITracingItem) As Double
    Dim length As Double = 0.0R
    Dim current = routeStartItem

    Do While current IsNot Nothing
      If TypeOf current Is TracingLine Then
        length += CType(current, TracingLine).Segment.Length.GetValueOrDefault()
      End If
      current = current.Next
    Loop

    Return length
  End Function

  Private Function GetFullAddress(building As Building) As String
    If building Is Nothing Then
      Return $"{vbTab}{vbTab}{vbTab}"
    End If
    Dim street = building.GetStreet()
    Dim city = street?.GetCity()
    Dim postcode = building.GetPostcode()
    Return $"{postcode?.Postcode}{vbTab}{city?.Name}{vbTab}{street?.Name}{vbTab}{building.Housenumber}{building.Suffix}"
  End Function

  Private Sub DebugOutput(building As Building, message As String)
    Debug.WriteLine($"{building.FID}{vbTab}{building.Name}{vbTab}{GetFullAddress(building)}{vbTab}{message}")
  End Sub

  Private Sub DebugOutput(building As Building, insertionAtBuilding As DuctInsertion, terminatorAtBuilding As FiberOpticTerminator, targetDevice As TelcoDevice, routeLength As Double, transformation As ITransformation)
    If building Is Nothing Then Throw New ArgumentNullException(NameOf(building))
    If insertionAtBuilding Is Nothing Then Throw New ArgumentNullException(NameOf(insertionAtBuilding))
    If transformation Is Nothing Then Throw New ArgumentNullException(NameOf(transformation))

    Dim buildingLL84 = transformation.Transform(CType(building.Geometry, Point))
    Dim deviceLL84 = transformation.Transform(CType(targetDevice.Geometry, Point))
    Dim insertionLL84 = transformation.Transform(CType(insertionAtBuilding.Geometry, Point))
    Dim terminatorLL84 As Point = Nothing
    Dim buildingCable As FiberOpticCable = Nothing
    If terminatorAtBuilding IsNot Nothing Then
      terminatorLL84 = transformation.Transform(CType(terminatorAtBuilding.Geometry, Point))
      buildingCable = terminatorAtBuilding.GetIncomingCables().FirstOrDefault
    End If



    Dim outputString As New StringBuilder()
    outputString.Append($"{insertionAtBuilding.StatusText}{vbTab}")
    outputString.Append($"{building.Key}{vbTab}")
    Dim buildingUnits As Long = building.NumberOfHouseholds.GetValueOrDefault(0)
    outputString.Append($"{If(buildingUnits > 1, "MFH", "EFH")}{vbTab}")
    outputString.Append($"{buildingUnits}{vbTab}")
    outputString.Append($"{insertionAtBuilding.GetDuct().ColorString}{vbTab}")
    outputString.Append($"{routeLength}{vbTab}")
    outputString.Append($"{targetDevice.Name}{vbTab}")
    outputString.Append($"{targetDevice.GeomFeature.Attributes("C_PROJECT_ID").ValueString}-{targetDevice.Name}{vbTab}")
    outputString.Append($"{targetDevice.GetStructuralPoint()?.GetBuilding?.Name}{vbTab}")
    outputString.Append($"{GetFullAddress(targetDevice.GetStructuralPoint()?.GetBuilding)}{vbTab}")
    outputString.Append($"{deviceLL84.Y}{vbTab}")
    outputString.Append($"{deviceLL84.X}{vbTab}")

    outputString.Append($"{terminatorAtBuilding IsNot Nothing}{vbTab}")
    outputString.Append($"{buildingCable?.GetFibersCount()}{vbTab}")
    outputString.Append($"{buildingCable?.GetFibersCount(True)}{vbTab}")
    outputString.Append($"{buildingLL84.Y}{vbTab}")
    outputString.Append($"{buildingLL84.X}{vbTab}")
    outputString.Append($"{insertionLL84.Y}{vbTab}")
    outputString.Append($"{insertionLL84.X}{vbTab}")
    outputString.Append($"{(terminatorLL84?.Y).GetValueOrDefault()}{vbTab}")
    outputString.Append($"{(terminatorLL84?.X).GetValueOrDefault()}{vbTab}")

    DebugOutput(building, outputString.ToString())
  End Sub

  Private Function CreateTransformation(connection As TBConnection) As ITransformation
    Try
      Dim csFactory = New CoordinateSystemFactory()
      Dim targetCoordinateSystem As ICoordinateSystem = Nothing
      Dim sourceCoordinateSystem As ICoordinateSystem = Nothing
      Using coordinateSystems = csFactory.CoordinateSystems
        sourceCoordinateSystem = coordinateSystems.CreateFromWKT(connection.DefaultCoordinateSystem.Wkt)
        Const tcs = "LL84"
        For Each c As ICoordinateSystem In coordinateSystems
          If tcs.Equals(c.Name, StringComparison.OrdinalIgnoreCase) Then
            targetCoordinateSystem = c
          End If
        Next

        If targetCoordinateSystem Is Nothing Then
          Return Nothing
        End If

        If Not targetCoordinateSystem.Equals(sourceCoordinateSystem) Then
          Using transformations = csFactory.Transformations
            Return transformations.Create(sourceCoordinateSystem, targetCoordinateSystem)
          End Using
        End If
      End Using
    Catch ignore As Exception
    End Try
    Return Nothing
  End Function

End Module
