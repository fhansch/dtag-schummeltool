Imports System.Threading
Imports Autodesk.Map.IM.Data
Imports Autodesk.Map.IM.Data.Util
Imports Autodesk.Map.IM.Forms
Imports Autodesk.Map.IM.Graphic
Imports Autodesk.Map.IM.Modules.Common.API
Imports TC.Common.API
Imports TC.Common.API.DuctInfrastructure
Imports TC.Common.API.SurveyPlan
Imports TC.FO.API
Imports TKI.NetworkExplorer.Caching
Imports TKI.Tools
Imports TKI.Tools.Database

Friend Module DeleteCluster
  Friend Sub DeleteDTAGJob(status As IStatusDisplay, parameters() As Object)
    If parameters.Length <> 2 Then Throw New ArgumentException("The number of parameters has to be three.", NameOf(parameters))
    If TypeOf parameters(0) IsNot Cluster Then Throw New ArgumentException($"The parameter 1 has the wrong type. Expected: {NameOf(Cluster)}", NameOf(parameters))
    If TypeOf parameters(1) IsNot Document Then Throw New ArgumentException($"The parameter 2 has the wrong type. Expected: {NameOf(Document)}", NameOf(parameters))

    Dim vCluster = DirectCast(parameters(0), Cluster)
    Dim vDocument = DirectCast(parameters(1), Document)

    Task.WaitAll(RunDeleteDTAGJob(status, vCluster, vDocument, CancellationToken.None))
  End Sub

  Private Async Function RunDeleteDTAGJob(status As IStatusDisplay, vCluster As Cluster, document As Document, token As CancellationToken) As Task
    Using db As New DatabaseWorker(vCluster.Connection)
      Using burst As New RegenerateLabelDisableBurst(db.Connection)
        Using mapBurst = document.Map.InitiateDrawBurst()
          Using transaction = Await db.BeginTransaction(token)
            Dim sms As New Spatial_Mask.Struct With {.AnyInteract = True}
            Dim cache = New ExportCache(TryCast(vCluster.Geometry, Polygon), sms, db, token)

            status.Status = "Daten ermitteln..."
            Dim deletefeatureQueue = New Queue(Of IEnumerable(Of FeatureItem))
            deletefeatureQueue.Enqueue((Await FiberOpticSplice.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await Fiber.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await FiberOpticCable.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await DuctTap.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await DuctInfrastructure.DuctFitting.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await DuctInsertion.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await DuctCable.GetCache(cache)).Values)
            Dim allDucts = (Await Duct.GetCache(cache)).Values.ToHashSet
            Dim ductDuctsByInnerDuctFid = Await cache.DuctDuctsByInnerDuctFid
            Dim innerducts = New HashSet(Of Duct)
            For Each duct In allDucts
              If ductDuctsByInnerDuctFid.Contains(duct.FID) Then innerducts.Add(duct)
            Next
            deletefeatureQueue.Enqueue(innerducts)
            deletefeatureQueue.Enqueue(allDucts.Except(innerducts))
            deletefeatureQueue.Enqueue((Await DuctDuct.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await SegmentCable.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await SegmentDuct.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await Segment.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await FiberOpticSplitter.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await FiberOpticSplitterPath.GetCache(cache)).Values)
            'deletefeatureQueue.Enqueue((Await FiberOpticPatch.GetCache(cache)).Values)
            'deletefeatureQueue.Enqueue((Await FiberOpticPatchFiber.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await FiberOpticTray.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await FiberOpticConnector.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await FiberOpticClosure.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await FiberOpticTerminator.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await Cabinet.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await Manhole.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await Pole.GetCache(cache)).Values)
            deletefeatureQueue.Enqueue((Await SwitchingPoint.GetCache(cache)).Values)


            While deletefeatureQueue.Any
              Dim nextFeatures = deletefeatureQueue.Dequeue
              If Not nextFeatures.Any Then Continue While
              status.Status = $"Lösche {nextFeatures.First.FeatureClass.Caption}..."
              If TypeOf nextFeatures.First Is UtilityFeatureItem Then
                Await DeleteUtilityFeatures(nextFeatures.OfType(Of UtilityFeatureItem), cache.Db, token)
              Else
                Await DeleteFeatures(nextFeatures, cache.Db, token)
              End If
            End While

            Await transaction.Commit(token)
          End Using
        End Using
      End Using
    End Using

  End Function

  Friend Async Function DeleteUtilityFeatures(features As IEnumerable(Of UtilityFeatureItem), db As DatabaseWorker, token As CancellationToken) As Task
    If Not features.Any Then Return
    Dim fc = features.First.FeatureClass
    Dim unprocessedFeatures = New HashSet(Of UtilityFeatureItem)(features)
    While unprocessedFeatures.Any
      Dim nextBatch = unprocessedFeatures.Take(If(unprocessedFeatures.Count > 500, 500, unprocessedFeatures.Count)).ToList
      For Each item In nextBatch
        unprocessedFeatures.Remove(item)
      Next
      Await db.ExecuteOnDatabase(Function() fc.DeleteFeatures(nextBatch.Select(Function(f) f.FID)), token)
    End While
  End Function

  Friend Async Function DeleteFeatures(features As IEnumerable(Of FeatureItem), db As DatabaseWorker, token As CancellationToken) As Task
    If Not features.Any Then Return
    Dim fc = features.First.FeatureClass
    Dim unprocessedFeatures = New HashSet(Of FeatureItem)(features)
    While unprocessedFeatures.Any
      Dim nextBatch = unprocessedFeatures.Take(If(unprocessedFeatures.Count > 500, 500, unprocessedFeatures.Count)).ToList
      For Each item In nextBatch
        unprocessedFeatures.Remove(item)
      Next
      Await db.ExecuteOnDatabase(Function() fc.DeleteFeatures(nextBatch.Select(Function(f) f.FID)), token)
    End While
  End Function
End Module
